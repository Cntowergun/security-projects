﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SecureFileEncryptor
{
    public class FileEncryption_APIClass
    {
        public async Task QuicklyEncryptDirectory(string Directory, string Password)
        {
            DirectoryInfo DirectoryEncrypt = new DirectoryInfo(Directory);
            var i = 0;
            foreach (var fileInfo in DirectoryEncrypt.GetFiles())
            {
                i++;
            }
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(
                    new Uri(
                        "https://raw.githubusercontent.com/EpicGamesGun/File-Encryption-API/master/File-Encryption-API-Responder/File-Encryption-API-Responder/bin/Debug/File-Encryption-API-Responder.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Responder.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var c = i;
            i = 0;
            foreach (var item in DirectoryEncrypt.GetFiles())
            {
                BackgroundWorker Respoder = new BackgroundWorker();
                Respoder.DoWork += async (sender, args) =>
                {
                    string[] DataToWrite = { item.FullName, Password, "QuickDirectoryEncrypt" };
                    File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\FileEncrypt-APIRespond.txt",
                        DataToWrite);
                    Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Responder.exe").WaitForExit();
                    i++;
                };
                Respoder.RunWorkerAsync();
                await Task.Delay(500);
            }

            while (i < c)
            {
                await Task.Delay(10);
            }
        }

        public async Task QuicklyDecryptDirectory(string Directory, string Password)
        {
            DirectoryInfo DirectoryEncrypt = new DirectoryInfo(Directory);
            var i = 0;
            foreach (var fileInfo in DirectoryEncrypt.GetFiles())
            {
                i++;
            }
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(
                    new Uri(
                        "https://raw.githubusercontent.com/EpicGamesGun/File-Encryption-API/master/File-Encryption-API-Responder/File-Encryption-API-Responder/bin/Debug/File-Encryption-API-Responder.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Responder.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var c = i;
            i = 0;
            foreach (var item in DirectoryEncrypt.GetFiles())
            {
                BackgroundWorker Respoder = new BackgroundWorker();
                Respoder.DoWork += async (sender, args) =>
                {
                    string[] DataToWrite = { item.FullName, Password, "QuickDirectoryDecrypt" };
                    File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\FileEncrypt-APIRespond.txt",
                        DataToWrite);
                    Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Responder.exe").WaitForExit();
                    i++;
                };
                Respoder.RunWorkerAsync();
                await Task.Delay(500);
            }

            while (i < c)
            {
                await Task.Delay(10);
            }
        }

        public async Task QuicklyDecryptFile(string FilePath, string Password)
        {
            string[] DataToWrite = { FilePath, Password, "QuickFileDecrypt" };
            File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\FileEncrypt-APIRespond.txt", DataToWrite);
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(
                    new Uri(
                        "https://raw.githubusercontent.com/EpicGamesGun/File-Encryption-API/master/File-Encryption-API-Responder/File-Encryption-API-Responder/bin/Debug/File-Encryption-API-Responder.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Responder.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }

            await Task.Factory.StartNew(() =>
            {
                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Responder.exe").WaitForExit();
            });
        }

        public async Task QuicklyEncryptFile(string FilePath, string Password)
        {
            string[] DataToWrite = { FilePath, Password, "QuickFileEncrypt" };
            File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\FileEncrypt-APIRespond.txt", DataToWrite);
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(
                    new Uri(
                        "https://raw.githubusercontent.com/EpicGamesGun/File-Encryption-API/master/File-Encryption-API-Responder/File-Encryption-API-Responder/bin/Debug/File-Encryption-API-Responder.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Responder.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }

            await Task.Factory.StartNew(() =>
            {
                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Responder.exe").WaitForExit();
            });
        }
    }
}
