﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Media;
using System.Net;
using System.Net.Mail;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Security.Cryptography;
using System.Speech.Synthesis;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GitHUB_API;
using GitHub_Large_Uploader_API;
using HTML_Download_API;
using Jackson_Download_Manager_API;
using MaterialDesignThemes.Wpf;
using WinRAR_API;
using WPFFolderBrowser;
using Application = System.Windows.Application;
using CheckBox = System.Windows.Controls.CheckBox;
using Clipboard = System.Windows.Clipboard;
using MessageBox = System.Windows.MessageBox;
using OpenFileDialog = Microsoft.Win32.OpenFileDialog;
using Path = System.IO.Path;
using ProgressBar = System.Windows.Controls.ProgressBar;
using SaveFileDialog = Microsoft.Win32.SaveFileDialog;
using TextBox = System.Windows.Controls.TextBox;

namespace SecureFileEncryptor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Loaded += MainWindow_Loaded;
            Loaded += MainWindow_Loaded1;
            Loaded += MainWindow_Loaded2;
            Loaded += MainWindow_Loaded3;
            Loaded += async (sender, args) =>
            {
                AdvancedDecryptUniqueEncryptionCheckBox.IsEnabled = true;
                AdvancedEncryptUniqueEncryptionCheckBox.IsEnabled = true;
                CreateSecureVaultUniqueEncryptionCheckBox.IsEnabled = true;
                OpenSecureVaultUniqueEncryptionCheckBox.IsEnabled = true;
                Directory.CreateDirectory(GitHubLoginDirectory);
                if (File.Exists(GitHubLoginDirectory + "\\CurrentAccount.txt"))
                {
                    try
                    {
                        GitHubLoggedInLabel.Content = File.ReadAllText(GitHubLoginDirectory + "\\CurrentAccount.txt").Split('$')[1].Trim();
                        GitHubLoggedInAccount = GitHubLoggedInLabel.Content.ToString();
                    }
                    catch
                    {

                    }
                }

                await RefreshGitHubLoginListBox();
            };
        }

        

        private string GitHubLoggedInAccount = "";

        private async Task RefreshGitHubLoginListBox()
        {
            GitHubAccountsListBox.Items.Clear();
            if (File.Exists(GitHubLoginDirectory + "\\SavedAccount.txt"))
            {
                foreach (var readLine in File.ReadLines(GitHubLoginDirectory + "\\SavedAccount.txt"))
                {
                    GitHubAccountsListBox.Items.Add(readLine.Split('$')[0].Trim());
                }
            }
        }
        private async void MainWindow_Loaded3(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(@"C:\Program Files (x86)\GitHub CLI\gh.exe"))
            {
                File.WriteAllBytes(Environment.GetEnvironmentVariable("TEMP") + "\\CLI.msi",Properties.Resources.GitHubCLI);
                await Task.Factory.StartNew(() =>
                {
                    Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\CLI.msi","/passive").WaitForExit();
                });
            }
        }

        string DataBaseFile = Environment.GetEnvironmentVariable("TEMP") + "\\TempDataBase.txt";


        private async Task<string> GetCoronavirusCases()
        {
            string CheckCoronavirusCasesFile =
                Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt";
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://ncov2019.live/"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var ReadLine = 0;
            string CoronavirusCases = "";
            foreach (var readLine in File.ReadLines(CheckCoronavirusCasesFile))
            {
                if (readLine.Contains("Total Confirmed"))
                {
                    CoronavirusCases = File.ReadLines(CheckCoronavirusCasesFile)
                        .ElementAtOrDefault(ReadLine - 3);
                }

                ReadLine++;
            }

            CoronavirusCases = CoronavirusCases.Replace(",", "");
            CoronavirusCases.Replace(" ", "");

            return CoronavirusCases;
        }

        private async Task<string> GetRecoveredCoronavirusCases()
        {
            string CheckCoronavirusCasesFile =
                Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt";
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://ncov2019.live/"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var ReadLine = 0;
            string CoronavirusCases = "";
            foreach (var readLine in File.ReadLines(CheckCoronavirusCasesFile))
            {
                if (readLine.Contains("Total Recovered"))
                {
                    CoronavirusCases = File.ReadLines(CheckCoronavirusCasesFile)
                        .ElementAtOrDefault(ReadLine - 3);
                }

                ReadLine++;
            }

            CoronavirusCases = CoronavirusCases.Replace(",", "");
            CoronavirusCases.Replace(" ", "");

            return CoronavirusCases;
        }

        private async Task<string> GetActiveCoronavirusCases()
        {
            string CheckCoronavirusCasesFile =
                Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt";
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://ncov2019.live/"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var ReadLine = 0;
            string CoronavirusCases = "";
            foreach (var readLine in File.ReadLines(CheckCoronavirusCasesFile))
            {
                if (readLine.Contains("Total Active"))
                {
                    CoronavirusCases = File.ReadLines(CheckCoronavirusCasesFile)
                        .ElementAtOrDefault(ReadLine - 3);
                }

                ReadLine++;
            }

            CoronavirusCases = CoronavirusCases.Replace(",", "");
            CoronavirusCases.Replace(" ", "");

            return CoronavirusCases;
        }

        private async Task<string> GetCriticalCoronavirusCases()
        {
            string CheckCoronavirusCasesFile =
                Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt";
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://ncov2019.live/"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var ReadLine = 0;
            string CoronavirusCases = "";
            foreach (var readLine in File.ReadLines(CheckCoronavirusCasesFile))
            {
                if (readLine.Contains("Total Critical"))
                {
                    CoronavirusCases = File.ReadLines(CheckCoronavirusCasesFile)
                        .ElementAtOrDefault(ReadLine - 3);
                }

                ReadLine++;
            }

            CoronavirusCases = CoronavirusCases.Replace(",", "");
            CoronavirusCases.Replace(" ", "");

            return CoronavirusCases;
        }

        private async Task<string> GetDeathCoronavirusCases()
        {
            string CheckCoronavirusCasesFile =
                Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt";
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://ncov2019.live/"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var ReadLine = 0;
            string CoronavirusCases = "";
            foreach (var readLine in File.ReadLines(CheckCoronavirusCasesFile))
            {
                if (readLine.Contains("Total Deceased"))
                {
                    CoronavirusCases = File.ReadLines(CheckCoronavirusCasesFile)
                        .ElementAtOrDefault(ReadLine - 3);
                }

                ReadLine++;
            }

            CoronavirusCases = CoronavirusCases.Replace(",", "");
            CoronavirusCases.Replace(" ", "");

            return CoronavirusCases;
        }

        private async Task<string> GetVaccineCoronavirusCases()
        {
            string CheckCoronavirusCasesFile =
                Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt";
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://ncov2019.live/"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\CoronavirusCheckerThingHuiChut.txt");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            var ReadLine = 0;
            string CoronavirusCases = "";
            foreach (var readLine in File.ReadLines(CheckCoronavirusCasesFile))
            {
                if (readLine.Contains("Total Vaccines In Development"))
                {
                    CoronavirusCases = File.ReadLines(CheckCoronavirusCasesFile)
                        .ElementAtOrDefault(ReadLine - 3);
                }

                ReadLine++;
            }

            CoronavirusCases = CoronavirusCases.Split('<')[0].Trim();
            CoronavirusCases.Replace(" ", "");
            return CoronavirusCases;
        }
        private async void MainWindow_Loaded1(object sender, RoutedEventArgs e)
        {
            try
            {
                CoronavirusLabel.Content = "Coronavirus cases confirmed:" + await GetCoronavirusCases() +
                                           " | Coronavirus Deaths:" + await GetDeathCoronavirusCases() +
                                           " | Critical coronavirus cases:" + await GetCriticalCoronavirusCases() +
                                           "\nRecovered coronavirus cases:" + await GetRecoveredCoronavirusCases() +
                                           " | Active coronavirus cases:" + await GetActiveCoronavirusCases() +
                                           " | Vaccines in development:" + await GetVaccineCoronavirusCases();
            }
            catch
            {
                CoronavirusLabel.Content = "Could not fetch COVID-19 info.";
            }
        }

        private async void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            await SetupNewRun();
            EncryptFileGrid.Visibility = Visibility.Visible;
            DecryptFileGrid.Visibility = Visibility.Hidden;
            PartitioningToolsGrid.Visibility = Visibility.Hidden;
            VeracryptToolsGrid.Visibility = Visibility.Hidden;
            Base64AndHashingGrid.Visibility = Visibility.Hidden;
            WindowsToolsGrid.Visibility = Visibility.Hidden;
            TextEncryptionGrid.Visibility = Visibility.Hidden;
            OpenSecureVaultGrid.Visibility = Visibility.Hidden;
            CreateSecureFileVaultGrid.Visibility = Visibility.Hidden;
            UsefulToolsGrid.Visibility = Visibility.Hidden;
            ButtonClose.Visibility = Visibility.Hidden;
            AdvancedEncryptFileGrid.Visibility = Visibility.Hidden;
            AdvancedDecryptFileGrid.Visibility = Visibility.Hidden;
            PasswordGenerationGrid.Visibility = Visibility.Hidden;
            QuickEncryptionGrid.Visibility = Visibility.Hidden;
            QuickModeDecryptGrid.Visibility = Visibility.Hidden;
            COVID19Grid.Visibility = Visibility.Hidden;
            GitHubLargeUploaderGrid.Visibility = Visibility.Hidden;
            SettingsPageOneGrid.Visibility = Visibility.Hidden;
            UploaderDownloaderGrid.Visibility = Visibility.Hidden;
            ProRegistrationGrid.Visibility = Visibility.Hidden;
            GitHubLoginGrid.Visibility = Visibility.Hidden;
            TTSGrid.Visibility = Visibility.Hidden;
            Console.WriteLine(getAvailableDriveLetters());
        }

        private void ListViewItem_Selected(object sender, RoutedEventArgs e)
        {

        }

        private async void ListViewItem_Selected_1(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(EncryptFileGrid);
        }

        private async Task PromptProRegistration(string Message)
        {
            await ShowUniversalSnackbarButton(Message, "Register Now", TimeSpan.FromSeconds(3), async () =>
            {
                await ChangeGrid(ProRegistrationGrid);
            });
        }

        private async Task PromptProRegistration()
        {
            await ShowUniversalSnackbarButton("Register to PRO to unlock this feature.", "Register Now", TimeSpan.FromSeconds(3), async () =>
            {
                await ChangeGrid(ProRegistrationGrid);
            });
        }
        private async Task ChangeGrid(Grid grid)
        {
            void HideAllGrids()
            {
                TTSGrid.Visibility = Visibility.Hidden;
                SettingsPageOneGrid.Visibility = Visibility.Hidden;
                GitHubLoginGrid.Visibility = Visibility.Hidden;
                ProRegistrationGrid.Visibility = Visibility.Hidden;
                GitHubLargeUploaderGrid.Visibility = Visibility.Hidden;
                UploaderDownloaderGrid.Visibility = Visibility.Hidden;
                COVID19Grid.Visibility = Visibility.Hidden;
                QuickModeDecryptGrid.Visibility = Visibility.Hidden;
                QuickEncryptionGrid.Visibility = Visibility.Hidden;
                PasswordGenerationGrid.Visibility = Visibility.Hidden;
                AdvancedDecryptFileGrid.Visibility = Visibility.Hidden;
                AdvancedEncryptFileGrid.Visibility = Visibility.Hidden;
                UsefulToolsGrid.Visibility = Visibility.Hidden;
                CreateSecureFileVaultGrid.Visibility = Visibility.Hidden;
                OpenSecureVaultGrid.Visibility = Visibility.Hidden;
                TextEncryptionGrid.Visibility = Visibility.Hidden;
                Base64AndHashingGrid.Visibility = Visibility.Hidden;
                EncryptFileGrid.Visibility = Visibility.Hidden;
                DecryptFileGrid.Visibility = Visibility.Hidden;
                PartitioningToolsGrid.Visibility = Visibility.Hidden;
                VeracryptToolsGrid.Visibility = Visibility.Hidden;
                WindowsToolsGrid.Visibility = Visibility.Hidden;
            }

            void SetVisibleGrid(Grid bruh)
            {
                bruh.Visibility = Visibility.Visible;
            }

            if(grid == TTSGrid)
            {
                HideAllGrids();
                if (ProVersion)
                {
                    SetVisibleGrid(TTSGrid);
                }
                else
                {
                    await PromptProRegistration("Register for PRO to access TTS features");
                }
            }

            if (grid == SettingsPageOneGrid)
            {
                HideAllGrids();
                SetVisibleGrid(SettingsPageOneGrid);
            }
            if (grid == ProRegistrationGrid)
            {
                HideAllGrids();
                SetVisibleGrid(ProRegistrationGrid);
            }

            if (grid == GitHubLoginGrid)
            {
                HideAllGrids();
                if (ProVersion == true)
                {
                    SetVisibleGrid(GitHubLoginGrid);
                }
                else
                {
                    await PromptProRegistration("Register for PRO to login to GITHUB!");
                }
            }
            if (grid == GitHubLargeUploaderGrid)
            {
                if (grid == GitHubLargeUploaderGrid && ProVersion == true)
                {
                    HideAllGrids();
                    GitHubLargeUploaderGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    await PromptProRegistration("GitHub Uploader is for PRO members only");
                } 
            }

            if (grid == UploaderDownloaderGrid)
            {
                if (grid == UploaderDownloaderGrid && ProVersion == true)
                {
                    HideAllGrids();
                    UploaderDownloaderGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    await PromptProRegistration("Downloader is for PRO members only");
                } 
            }

            if (grid == COVID19Grid)
            {
                if (ProVersion == false)
                {
                    await ShowUniversalSnackbar3Seconds("COVID-19 info is free for all users.");
                }
                HideAllGrids();
                COVID19Grid.Visibility = Visibility.Visible;
            }

            if (grid == QuickEncryptionGrid)
            {
                HideAllGrids();
                QuickEncryptionGrid.Visibility = Visibility.Visible;
            }

            if (grid == QuickModeDecryptGrid)
            {
                HideAllGrids();
                grid.Visibility = Visibility.Visible;
            }

            if (grid == WindowsToolsGrid)
            {
                if (grid == WindowsToolsGrid && ProVersion == true)
                {
                    HideAllGrids();
                    WindowsToolsGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    await PromptProRegistration();
                } 
            }

            if (grid == PasswordGenerationGrid)
            {
                if (grid == PasswordGenerationGrid && ProVersion == true)
                {
                    HideAllGrids();
                    PasswordGenerationGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    await PromptProRegistration("Password generator for PRO members only");
                } 
            }

            if (grid == UsefulToolsGrid)
            {
                if (grid == UsefulToolsGrid && ProVersion == true)
                {
                    HideAllGrids();
                    UsefulToolsGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    await PromptProRegistration("Useful Tools for PRO members only");
                } 
            }

            if (grid == CreateSecureFileVaultGrid)
            {
                if (grid == CreateSecureFileVaultGrid && ProVersion == true)
                {
                    HideAllGrids();
                    CreateSecureFileVaultGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    await PromptProRegistration("Super secure file vault for PRO members only");
                    await Task.Delay(3000);
                    await ShowUniversalSnackbarButton("Free version is available", "DOWNLOAD", TimeSpan.FromSeconds(3),
                        async () =>
                        {
                            await Download(
                                "https://gitlab.com/Cntowergun/security-projects/-/raw/master/Secure-File-Vault/Installer/Setup%20Files/Secure%20File%20Vault.exe",
                                Environment.GetEnvironmentVariable("TEMP") + "\\FreeVersionVault.exe");
                            await Task.Factory.StartNew(() =>
                            {
                                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\FreeVersionVault.exe",
                                    "/passive").WaitForExit();
                            });
                        });
                } 
            }

            if (grid == TextEncryptionGrid)
            {
                HideAllGrids();
                TextEncryptionGrid.Visibility = Visibility.Visible;
            }

            if (grid == OpenSecureVaultGrid)
            {
                if (grid == OpenSecureVaultGrid && ProVersion == true)
                {
                    HideAllGrids();
                    OpenSecureVaultGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    await PromptProRegistration("Super secure file vault for PRO members only");
                    await Task.Delay(3000);
                    await ShowUniversalSnackbarButton("Free version is available", "DOWNLOAD", TimeSpan.FromSeconds(3),
                        async () =>
                        {
                            await Download(
                                "https://gitlab.com/Cntowergun/security-projects/-/raw/master/Secure-File-Vault/Installer/Setup%20Files/Secure%20File%20Vault.exe",
                                Environment.GetEnvironmentVariable("TEMP") + "\\FreeVersionVault.exe");
                            await Task.Factory.StartNew(() =>
                            {
                                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\FreeVersionVault.exe",
                                    "/passive").WaitForExit();
                            });
                        });
                } 
            }

            if (grid == EncryptFileGrid)
            {
                HideAllGrids();
                EncryptFileGrid.Visibility = Visibility.Visible;
            }

            if (grid == PartitioningToolsGrid)
            {
                if (grid == PartitioningToolsGrid && ProVersion == true)
                {
                    HideAllGrids();
                    PartitioningToolsGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    await PromptProRegistration("Buy PRO version to unlock partitioning tools");
                } 
            }

            if (grid == DecryptFileGrid)
            {
                HideAllGrids();
                DecryptFileGrid.Visibility = Visibility.Visible;
            }

            if (grid == VeracryptToolsGrid)
            {
                if (grid == VeracryptToolsGrid && ProVersion == true)
                {
                    HideAllGrids();
                    VeracryptToolsGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    await PromptProRegistration("Buy PRO version to unlock Veracrypt tools");
                } 
            }

            if (grid == Base64AndHashingGrid)
            {
                if (grid == Base64AndHashingGrid && ProVersion == true)
                {
                    HideAllGrids();
                    Base64AndHashingGrid.Visibility = Visibility.Visible;
                }
                else
                {
                    await PromptProRegistration("Buy PRO version to unlock Base64 and Hashing Tools");
                } 
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dew = new OpenFileDialog();
            dew.ShowDialog();
            if (File.Exists(dew.FileName))
            {
                EncryptFileBrowseTextBox.Text = dew.FileName;
            }
        }

        private string BackupDirectory = Environment.GetEnvironmentVariable("APPDATA") + "\\SecureTools";

        private string MainSoftwareDirectory =
            Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\SecureTools";

        private string MainDirectory = Environment.GetEnvironmentVariable("TEMP") + "\\SecureTools";

        private async Task ShowSnackBarEncryptFiles(TimeSpan time, string Message)
        {
            var Queuer = new SnackbarMessageQueue(time);
            SuccessfullyEncryptSnackbar.MessageQueue = Queuer;
            Queuer.Enqueue(Message);
        }

        private async void EncryptFileButton_Click(object sender, RoutedEventArgs e)
        {
            if (EncryptDirectoryModeCheckbox.IsChecked == true)
            {
                DirectoryEncryptFilesGrid.Visibility = Visibility.Hidden;
                EncryptDirectoryModeCheckbox.IsEnabled = false;
                DirectoryInfo d = new DirectoryInfo(BrowseDirectoryTextBoxEncrypt.Text);
                var i = 0;
                foreach (var fileInfo in d.GetFiles())
                {
                    i++;
                }

                EncryptStatusProgressbar.Maximum = i;
                var c = 0;
                EncryptFileButton.Content = "(0/" + i + ")";
                foreach (var fileInfo in d.GetFiles())
                {
                    EncryptFileBrowseTextBox.Text = BrowseDirectoryTextBoxEncrypt.Text + "\\" + fileInfo.Name;
                    await EncryptFile();
                    c++;
                    EncryptFileButton.Content = "(" + c + "/" + i + ")";
                    EncryptStatusProgressbar.Value = c;
                    await ShowSnackBarEncryptFiles(TimeSpan.FromSeconds(3),
                        "Files Encrypted: " + "(" + c + " / " + i + ")");
                }

                EncryptFileButton.Content = "Encrypt File";
                EncryptFileButton.IsEnabled = true;
                EncryptFileBrowseTextBox.Text = "";
                EncryptStatusProgressbar.Value = 0;
                EncryptPIM.Password = "";
                CreatePassword.Password = "";
                ConfirmPassword.Password = "";
                await PlaySound(Properties.Resources.SuccessfullyEncrypted);
                DirectoryEncryptFilesGrid.Visibility = Visibility.Visible;
                EncryptDirectoryModeCheckbox.IsEnabled = true;
                Process.Start(BrowseDirectoryTextBoxEncrypt.Text);
                BrowseDirectoryTextBoxEncrypt.Text = "";
            }
            else
            {
                await EncryptFile();
            }
        }

        private async Task SetupNewRun()
        {
            if (!Directory.Exists(MainSoftwareDirectory))
            {
                Visibility = Visibility.Hidden;
                Directory.CreateDirectory(MainSoftwareDirectory);
                await Download(
                    "https://gitlab.com/Cntowergun/security-projects/-/raw/master/SecureFileEncryptor/packages/WinRAR.zip",
                    MainSoftwareDirectory + "\\WinRARDownload.zip");
                await Download(
                    "https://gitlab.com/Cntowergun/security-projects/-/raw/master/SecureFileEncryptor/packages/VeraCrypt.zip",
                    MainSoftwareDirectory + "\\VeraCryptDownload.zip");
                ZipFile.ExtractToDirectory(MainSoftwareDirectory + "\\WinRARDownload.zip",
                    MainSoftwareDirectory + "\\WinRAR");
                ZipFile.ExtractToDirectory(MainSoftwareDirectory + "\\VeraCryptDownload.zip",
                    MainSoftwareDirectory + "\\VeraCrypt");
            }
            else
            {
                Topmost = true;
                await Task.Delay(500);
                Topmost = false;
            }
            Visibility = Visibility.Visible;
        }
        private async Task EncryptFile()
        {
            Topmost = true;
            var SuccessLockQueue = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(8000));
            SuccessfullyEncryptSnackbar.MessageQueue = SuccessLockQueue;
            try
            {
                if (EncryptBackupCheckbox.IsChecked == true)
                {
                    if (!Directory.Exists(BackupDirectory))
                    {
                        Directory.CreateDirectory(BackupDirectory);
                    }

                    File.Copy(EncryptFileBrowseTextBox.Text,
                        BackupDirectory + "\\" + Path.GetFileName(EncryptFileBrowseTextBox.Text));
                }

                if (CreatePassword.Password == ConfirmPassword.Password)
                {
                    EncryptFileButton.IsEnabled = false;
                    EncryptFileButton.Content = "Encrypting..";
                    if (!Directory.Exists(MainSoftwareDirectory))
                    {
                        Directory.CreateDirectory(MainSoftwareDirectory);
                        await DownloadEncryptProgress(
                            "https://gitlab.com/Cntowergun/security-projects/-/raw/master/SecureFileEncryptor/packages/WinRAR.zip",
                            MainSoftwareDirectory + "\\WinRARDownload.zip");
                        await DownloadEncryptProgress(
                            "https://gitlab.com/Cntowergun/security-projects/-/raw/master/SecureFileEncryptor/packages/VeraCrypt.zip",
                            MainSoftwareDirectory + "\\VeraCryptDownload.zip");
                        ZipFile.ExtractToDirectory(MainSoftwareDirectory + "\\WinRARDownload.zip",
                            MainSoftwareDirectory + "\\WinRAR");
                        ZipFile.ExtractToDirectory(MainSoftwareDirectory + "\\VeraCryptDownload.zip",
                            MainSoftwareDirectory + "\\VeraCrypt");
                    }

                    string DefaultDriveLetter = GetRandomDriveLetter();
                    if (!Directory.Exists(MainDirectory))
                    {
                        Directory.CreateDirectory(MainDirectory);
                    }
                    else
                    {
                        DeleteDirectory(MainDirectory);
                        Directory.CreateDirectory(MainDirectory);
                    }

                    FileInfo size = new FileInfo(EncryptFileBrowseTextBox.Text);

                    string VaultSize()
                    {
                        string Return = "";
                        if (Int32.Parse((size.Length / 1024d / 1024d).ToString("0")) > 0)
                        {
                            Return = (size.Length / 1024d / 1024d).ToString("0");
                        }
                        else
                        {
                            Return = "100";
                        }

                        return Return;
                    }

                    if (File.Exists("C:\\Jerjer.vhd"))
                    {
                        await DismountVHD("C:\\Jerjer.vhd");
                        File.Delete("C:\\Jerjer.vhd");
                    }

                    Console.WriteLine("Creating virtual disk");
                    await CreateVHD((Int32.Parse(VaultSize()) + 10).ToString(), "C:\\Jerjer.vhd", DefaultDriveLetter);
                    Console.WriteLine("Waiting for mount");
                    while (!Directory.Exists(DefaultDriveLetter + ":\\"))
                    {
                        await Task.Delay(10);
                    }

                    Console.WriteLine("Moving File");
                    File.Move(EncryptFileBrowseTextBox.Text,
                        DefaultDriveLetter + ":\\" + Path.GetFileNameWithoutExtension(EncryptFileBrowseTextBox.Text));
                    await DismountVHD("C:\\Jerjer.vhd");

                    string PIM()
                    {
                        string Return = "";
                        if (EncryptPIM.Password == "")
                        {
                            Return = "500";
                        }
                        else
                        {
                            Return = EncryptPIM.Password;
                        }

                        return Return;
                    }

                    await CreateVeracryptVolume(MainDirectory + "\\MainEncrypt", UniqueHashing(CreatePassword.Password),
                        (Int32.Parse(VaultSize()) + 20).ToString(), PIM());
                    await MountVeracrypt(MainDirectory + "\\MainEncrypt", UniqueHashing(CreatePassword.Password), PIM(),
                        DefaultDriveLetter);
                    if (File.Exists(DefaultDriveLetter + ":\\Jerjer.vhd"))
                    {
                        File.Delete(DefaultDriveLetter + ":\\Jerjer.vhd");
                    }

                    File.Move("C:\\Jerjer.vhd", DefaultDriveLetter + ":\\Jerjer.vhd");
                    await DismountVeracrypt(DefaultDriveLetter);
                    if (File.Exists("C:\\MainEncrypt"))
                    {
                        File.Delete("C:\\MainEncrypt");
                    }

                    File.Move(MainDirectory + "\\MainEncrypt", "C:\\MainEncrypt");
                    await Rar(EncryptFileBrowseTextBox.Text, "C:\\MainEncrypt", UniqueHashing(CreatePassword.Password));
                    BackgroundWorker EncryptionWorker = new BackgroundWorker();
                    EncryptionWorker.DoWork += (sender, args) =>
                    {
                        FileEncryption.FileEncrypt(EncryptFileBrowseTextBox.Text, UniqueHashing(UniqueHashing(CreatePassword.Password)));
                        File.Delete(EncryptFileBrowseTextBox.Text);
                        File.Move(EncryptFileBrowseTextBox.Text + ".aes", EncryptFileBrowseTextBox.Text);
                    };
                    EncryptionWorker.RunWorkerAsync();
                    while (EncryptionWorker.IsBusy)
                    {
                        await Task.Delay(10);
                    }
                    if (EncryptDirectoryModeCheckbox.IsChecked == false)
                    {
                        EncryptFileButton.Content = "Encrypt File";
                        EncryptFileButton.IsEnabled = true;
                        EncryptFileBrowseTextBox.Text = "";
                        EncryptPIM.Password = "";
                        CreatePassword.Password = "";
                        ConfirmPassword.Password = "";
                        await PlaySound(Properties.Resources.SuccessfullyEncrypted);
                    }

                    SuccessLockQueue.Enqueue("File successfully encrypted");
                }
                else
                {
                    MessageBox.Show("Passwords must match!");
                }
            }
            catch (IOException dew)
            {
                Console.Write(dew);
                SuccessLockQueue.Enqueue("Access denied to encrypt file or file not found");
            }
            catch (Exception hui)
            {
                Console.WriteLine(hui);
                SuccessLockQueue.Enqueue("Error encrypting file");
            }

            EncryptFileButton.IsEnabled = true;
            EncryptFileButton.Content = "Encrypt File";
            Topmost = false;

        }

        private string UniqueHashing(string inputstring)
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(
                    new Uri(
                        "https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/Unique-Hasher/Unique-Hasher/bin/Debug/Unique-Hasher.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe");
                while (client.IsBusy)
                {
                    Task.Delay(10);
                }
            }

            File.WriteAllText(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Hashing.txt",
                inputstring);

            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe").WaitForExit();

            return File.ReadLines(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                                  "\\HashedString.txt").ElementAtOrDefault(0);
            File.Delete(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\HashedString.txt");
        }

        private async Task CreateVHD(string size, string path, string letter)
        {
            string[] Script =
            {
                "create vdisk file=\"" + path + "\" maximum=" + size +
                " type=expandable",
                "select vdisk file=\"" + path + "\"", "attach vdisk", "detail vdisk",
                "convert mbr", "create partition primary", "format fs=fat label=\"install\" quick",
                "assign letter=" + letter
            };
            File.WriteAllLines("C:" + "\\DiskPartScript.txt", Script);
            await RunCommandHidden("cd \"" + "C:\\" + "\"" + "\ndiskpart /s \"C:\\DiskPartScript.txt\"");
        }

        private async Task CreateVHDNTFS(string size, string path, string letter)
        {
            string[] Script =
            {
                "create vdisk file=\"" + path + "\" maximum=" + size +
                " type=expandable",
                "select vdisk file=\"" + path + "\"", "attach vdisk", "detail vdisk",
                "convert mbr", "create partition primary", "format fs=NTFS label=\"install\" quick",
                "assign letter=" + letter
            };
            File.WriteAllLines("C:" + "\\DiskPartScript.txt", Script);
            await RunCommandHidden("cd \"" + "C:\\" + "\"" + "\ndiskpart /s \"C:\\DiskPartScript.txt\"");
        }

        private async Task DismountVHD(string path)
        {

            string[] UnMountVdisk = { "select vdisk file=" + path, "detach vdisk" };
            File.WriteAllLines("C:" + "\\UnMountScript.txt", UnMountVdisk);
            await RunCommandHidden("diskpart /s \"" + "C:" + "\\UnMountScript.txt\"");
        }

        private async Task Download(string link, string filename)
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri(link), filename);
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
        }

        private async Task DownloadEncryptProgress(string link, string filename)
        {
            using (var client = new WebClient())
            {
                client.DownloadProgressChanged += Client_DownloadProgressChanged;
                client.DownloadFileAsync(new Uri(link), filename);
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
        }

        private async Task DownloadDecryptProgress(string link, string filename)
        {
            using (var client = new WebClient())
            {
                client.DownloadProgressChanged += Client_DownloadProgressChanged1;
                client.DownloadFileAsync(new Uri(link), filename);
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
        }

        private void Client_DownloadProgressChanged1(object sender, DownloadProgressChangedEventArgs e)
        {
            DecryptStatusProgressBar.Value = e.ProgressPercentage;
        }

        private void Client_DownloadProgressChanged(object sender, DownloadProgressChangedEventArgs e)
        {
            EncryptStatusProgressbar.Value = e.ProgressPercentage;
        }

        public async Task Unrar(string Archive, string Output, string Password)
        {
            await RunCommandHidden("cd \"" + MainSoftwareDirectory + "\\WinRAR\"" + "\nunrar x -p" + Password + " \"" +
                                   Archive + "\"" + " " + "\"" + Output + "\"");
        }

        public async Task Rar(string Archive, string Files, string Password)
        {
            await RunCommandHidden("cd \"" + MainSoftwareDirectory + "\\WinRAR\"" + "\nrar a -p" + Password + " \"" +
                                   Archive + "\"" + " " + "\"" + Files + "\"");
        }

        private async Task MountVeracrypt(string Path, string Password, string PIM, string Letter)
        {
            await RunCommandHidden("\"" + MainSoftwareDirectory + "\\VeraCrypt\\VeraCrypt.exe\"" + " /q /v \"" + Path +
                                   "\" /l " + Letter + " /a /p " + Password + " /pim " + PIM + "");
        }

        private async Task DismountVeracrypt(string Letter)
        {
            await RunCommandHidden("\"" + MainSoftwareDirectory + "\\VeraCrypt\\VeraCrypt.exe\"" + " /q /d " + Letter +
                                   " /s");
        }

        private async Task CreateVeracryptVolume(string Path, string Password, string Size, string PIM)
        {
            await RunCommandHidden("\"" + MainSoftwareDirectory + "\\VeraCrypt\\VeraCrypt Format.exe\"" +
                                   " /create \"" + Path +
                                   "\" /password " + Password + " /pim " + PIM +
                                   " /hash sha512 /encryption serpent /filesystem FAT /size " + Size +
                                   "M /force /silent /quick");
        }

        private async Task CreateVeracryptVolumeKB(string Path, string Password, string Size, string PIM)
        {
            await RunCommandHidden("\"" + MainSoftwareDirectory + "\\VeraCrypt\\VeraCrypt Format.exe\"" +
                                   " /create \"" + Path +
                                   "\" /password " + Password + " /pim " + PIM +
                                   " /hash sha512 /encryption serpent /filesystem FAT /size " + Size +
                                   "K /force /silent /quick");
        }

        private bool Exit = false;

        public async Task RunCommandHidden(string Command)
        {
            Random dew = new Random();
            int hui = dew.Next(11111111, 999999999);
            string[] CommandChut = { Command };
            File.WriteAllLines(
                System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat",
                CommandChut);
            Process C = new Process();
            C.StartInfo.FileName = System.Environment.GetEnvironmentVariable("USERPROFILE") +
                                   "\\Documents\\RunCommand" + hui + ".bat";
            C.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            C.EnableRaisingEvents = true;
            C.Exited += C_Exited;
            C.Start();
            while (Exit == false)
            {
                await Task.Delay(10);
            }

            Exit = false;
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui +
                        ".bat");
        }

        public async Task RunCommandShown(string Command)
        {
            Random dew = new Random();
            int hui = dew.Next(0000, 9999);
            string[] CommandChut = { Command };
            File.WriteAllLines(
                System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat",
                CommandChut);
            Process C = new Process();
            C.StartInfo.FileName = System.Environment.GetEnvironmentVariable("USERPROFILE") +
                                   "\\Documents\\RunCommand" + hui + ".bat";
            C.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            C.EnableRaisingEvents = true;
            C.Exited += C_Exited;
            C.Start();
            while (Exit == false)
            {
                await Task.Delay(10);
            }

            Exit = false;
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui +
                        ".bat");
        }

        public async Task RunCommandMaximized(string Command)
        {
            Random dew = new Random();
            int hui = dew.Next(0000, 9999);
            string[] CommandChut = { Command };
            File.WriteAllLines(
                System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat",
                CommandChut);
            Process C = new Process();
            C.StartInfo.FileName = System.Environment.GetEnvironmentVariable("USERPROFILE") +
                                   "\\Documents\\RunCommand" + hui + ".bat";
            C.StartInfo.WindowStyle = ProcessWindowStyle.Maximized;
            C.EnableRaisingEvents = true;
            C.Exited += C_Exited;
            C.Start();
            while (Exit == false)
            {
                await Task.Delay(10);
            }

            Exit = false;
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui +
                        ".bat");
        }

        private void C_Exited(object sender, EventArgs e)
        {
            Exit = true;
        }

        private async void ListViewItem_Selected_2(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(DecryptFileGrid);
        }

        private void BrowseButtonDecrypt_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dew = new OpenFileDialog();
            dew.ShowDialog();
            if (File.Exists(dew.FileName))
            {
                DecryptBrowseFile.Text = dew.FileName;
            }
        }

        private async Task ShowSnackbarUnlock(TimeSpan time, string Message)
        {
            var q = new SnackbarMessageQueue(time);
            SuccessUnlockSnackBar.MessageQueue = q;
            q.Enqueue(Message);
        }

        private async void UnlockFileButton_Click(object sender, RoutedEventArgs e)
        {
            if (DirectoryModeUnlockCheckbox.IsChecked == true)
            {
                UnlockDirectoryGrid.Visibility = Visibility.Hidden;
                DirectoryModeUnlockCheckbox.IsEnabled = false;
                DirectoryInfo d = new DirectoryInfo(BrowseDirectoryUnlock.Text);
                var i = 0;
                foreach (var fileInfo in d.GetFiles())
                {
                    i++;
                }

                DecryptStatusProgressBar.Maximum = i;
                var c = 0;
                UnlockFileButton.Content = "(" + c + "/" + i + ")";

                foreach (var fileInfo in d.GetFiles())
                {
                    DecryptBrowseFile.Text = BrowseDirectoryUnlock.Text + "\\" + fileInfo.Name;
                    await UnlockFile();
                    c++;
                    UnlockFileButton.Content = "(" + c + "/" + i + ")";
                    await ShowSnackbarUnlock(TimeSpan.FromSeconds(3), "Files Unlocked: " + "(" + c + "/" + i + ")");
                    DecryptStatusProgressBar.Value = c;
                }

                UnlockFileButton.Content = "Unlock File";
                DecryptStatusProgressBar.Value = 0;
                DecryptFilePassword.Password = "";
                DecryptPIM.Text = "";
                DecryptBrowseFile.Text = "";
                await PlaySound(Properties.Resources.SuccessfullyDecrypted);
                UnlockDirectoryGrid.Visibility = Visibility.Visible;
                DirectoryModeUnlockCheckbox.IsEnabled = true;
                Process.Start(BrowseDirectoryUnlock.Text);
                BrowseDirectoryUnlock.Text = "";
            }
            else
            {
                await UnlockFile();
            }
        }

        private async Task UnlockFile()
        {
            if (DecryptBackupFileCheckbox.IsChecked == true)
            {
                if (!Directory.Exists(BackupDirectory))
                {
                    Directory.CreateDirectory(BackupDirectory);
                }

                try
                {
                    File.Copy(DecryptBrowseFile.Text,
                        BackupDirectory + "\\" + Path.GetFileName(DecryptBrowseFile.Text));
                }
                catch (Exception exception)
                {

                }
            }

            UnlockFileButton.Content = "Unlocking...";
            UnlockFileButton.IsEnabled = false;
            if (!Directory.Exists(MainSoftwareDirectory))
            {
                Directory.CreateDirectory(MainSoftwareDirectory);
                await DownloadDecryptProgress(
                    "https://gitlab.com/Cntowergun/security-projects/-/raw/master/SecureFileEncryptor/packages/WinRAR.zip",
                    MainSoftwareDirectory + "\\WinRARDownload.zip");
                await DownloadDecryptProgress(
                    "https://gitlab.com/Cntowergun/security-projects/-/raw/master/SecureFileEncryptor/packages/VeraCrypt.zip",
                    MainSoftwareDirectory + "\\VeraCryptDownload.zip");
                ZipFile.ExtractToDirectory(MainSoftwareDirectory + "\\WinRARDownload.zip",
                    MainSoftwareDirectory + "\\WinRAR");
                ZipFile.ExtractToDirectory(MainSoftwareDirectory + "\\VeraCryptDownload.zip",
                    MainSoftwareDirectory + "\\VeraCrypt");
            }

            string DefaultDriveLetter = GetRandomDriveLetter();
            if (!Directory.Exists(MainDirectory))
            {
                Directory.CreateDirectory(MainDirectory);
            }
            else
            {
                DeleteDirectory(MainDirectory);
                Directory.CreateDirectory(MainDirectory);
            }
            BackgroundWorker DecryptionWorker = new BackgroundWorker();
            DecryptionWorker.DoWork += (sender, args) =>
            {
                FileEncryption.FileDecrypt(DecryptBrowseFile.Text, "C:\\TempFile", UniqueHashing(UniqueHashing(DecryptFilePassword.Password)));
                File.Delete(DecryptBrowseFile.Text);
                File.Move("C:\\TempFile", DecryptBrowseFile.Text);
            };
            DecryptionWorker.RunWorkerAsync();
            while (DecryptionWorker.IsBusy)
            {
                await Task.Delay(10);
            }
            File.Move(DecryptBrowseFile.Text, MainDirectory + "\\MainDecryption.rar");
            await Unrar(MainDirectory + "\\MainDecryption.rar", MainDirectory,
                UniqueHashing(DecryptFilePassword.Password));

            string PIM()
            {
                string Return = "";
                if (DecryptPIM.Text == "")
                {
                    Return = "500";
                }
                else
                {
                    Return = DecryptPIM.Text;
                }

                return Return;
            }

            await MountVeracrypt(MainDirectory + "\\MainEncrypt", UniqueHashing(DecryptFilePassword.Password), PIM(),
                DefaultDriveLetter);
            File.Copy(DefaultDriveLetter + ":\\Jerjer.vhd", MainDirectory + "\\Jerjer.vhd");
            await DismountVeracrypt(DefaultDriveLetter);
            await MountVHD(MainDirectory + "\\Jerjer.vhd", DefaultDriveLetter);
            DirectoryInfo d = new DirectoryInfo(DefaultDriveLetter + ":\\");
            foreach (var fileInfo in d.GetFiles())
            {
                File.Move(DefaultDriveLetter + ":\\" + fileInfo.Name, DecryptBrowseFile.Text);
            }

            await DismountVHD(MainDirectory + "\\Jerjer.vhd");
            UnlockFileButton.Content = "Unlock File";
            UnlockFileButton.IsEnabled = true;

            var MessageQueue = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(8000));
            SuccessUnlockSnackBar.MessageQueue = MessageQueue;
            if (DirectoryModeUnlockCheckbox.IsChecked == false)
            {
                DecryptFilePassword.Password = "";
                DecryptPIM.Text = "";
                DecryptBrowseFile.Text = "";
                await PlaySound(Properties.Resources.SuccessfullyDecrypted);
            }

            MessageQueue.Enqueue("File successfully unlocked");
        }

        public char[] getAvailableDriveLetters()
        {
            List<char> availableDriveLetters = new List<char>()
            {
                'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u',
                'v', 'w', 'x', 'y', 'z'
            };

            DriveInfo[] drives = DriveInfo.GetDrives();

            for (int i = 0; i < drives.Length; i++)
            {
                availableDriveLetters.Remove((drives[i].Name).ToLower()[0]);
            }

            return availableDriveLetters.ToArray();
        }

        private string GetRandomDriveLetter()
        {
            string Return = "";
            char[] AvailableDriveLetter = getAvailableDriveLetters();
            foreach (char c in AvailableDriveLetter)
            {
                Console.WriteLine(c);
                Return = c.ToString();
            }

            return Return;
        }

        public static void DeleteDirectory(string path)
        {
            foreach (string directory in Directory.GetDirectories(path))
            {
                DeleteDirectory(directory);
            }

            try
            {
                Directory.Delete(path, true);
            }
            catch (IOException)
            {
                Directory.Delete(path, true);
            }
            catch (UnauthorizedAccessException)
            {
                Directory.Delete(path, true);
            }
        }

        private async Task MountVHD(string path, string letter)
        {
            string[] MountDiskScript =
            {
                "select vdisk file=" + path, "attach vdisk", "select partition 1",
                "assign letter=" + letter
            };
            File.WriteAllLines("C:\\MountDiskScript.txt", MountDiskScript);
            await RunCommandHidden("diskpart /s \"" + "C:" + "\\MountDiskScript.txt\"");
        }

        private void ListViewItem_Selected_3(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(BackupDirectory))
            {
                Process.Start(BackupDirectory);
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dew = new SaveFileDialog();
            dew.ShowDialog();
            try
            {
                BrowseCreateVeracrypt.Text = dew.FileName;
            }
            catch (Exception exception)
            {

            }
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (CreateNTFS.IsChecked == true)
            {
                CreateFAT.IsChecked = false;
            }
        }

        private void CreateFAT_Checked(object sender, RoutedEventArgs e)
        {
            if (CreateFAT.IsChecked == true)
            {
                CreateNTFS.IsChecked = false;
            }
        }

        private async void ListViewItem_Selected_4(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(VeracryptToolsGrid);
        }

        private async void CreateVeracryptButton_Click(object sender, RoutedEventArgs e)
        {
            await CreateVeracryptVolumeSECURETOOLS();
        }

        private async Task CreateVeracryptVolumeSECURETOOLS()
        {
            string VaultFormat()
            {
                string Return = "";
                if (CreateFAT.IsChecked == true)
                {
                    Return = "FAT";
                }
                else if (CreateNTFS.IsChecked == true)
                {
                    Return = "NTFS";
                }

                return Return;
            }

            string Algorithm()
            {
                string Return = "";
                Return = AlgorithmComboBox.Text;
                return Return;
            }

            string Path()
            {
                return BrowseCreateVeracrypt.Text;
            }

            string PIM()
            {
                string Return = "";
                if (CreateVeracryptPIMTextbox.Text == "")
                {
                    Return = "500";
                }
                else
                {
                    Return = CreateVeracryptPIMTextbox.Text;
                }

                return Return;
            }

            string SizeUnit()
            {
                string Return = "";
                if (SizeUnitCreateVeracrypt.Text == "MB")
                {
                    Return = "M";
                }
                else if (SizeUnitCreateVeracrypt.Text == "KB")
                {
                    Return = "K";
                }
                else if (SizeUnitCreateVeracrypt.Text == "GB")
                {
                    Return = "G";
                }
                else
                {
                    Return = "M";
                }

                return Return;
            }

            string Password = CreateVeracryptPassword.Password;
            CreateVeracryptButton.Content = "Creating volume...";
            CreateVeracryptButton.IsEnabled = false;
            if (CreateVeracryptPassword.Password == CreateVeracryptPasswordConfirm.Password)
            {
                await RunCommandHidden("\"" + MainSoftwareDirectory + "\\VeraCrypt\\VeraCrypt Format.exe\"" +
                                       " /create \"" + Path() +
                                       "\" /password " + Password + " /pim " + PIM() + " /hash sha512 /encryption " +
                                       Algorithm() + " /filesystem " + VaultFormat() + " /size " +
                                       CreateVeracryptSize.Text + SizeUnit() + " /force /silent /quick");

            }

            CreateFAT.IsChecked = false;
            CreateNTFS.IsChecked = false;
            CreateVeracryptPassword.Password = "";
            CreateVeracryptPasswordConfirm.Password = "";
            string VeraCryptPath = BrowseCreateVeracrypt.Text;
            BrowseCreateVeracrypt.Text = "";
            AlgorithmComboBox.Text = "";
            SizeUnitCreateVeracrypt.Text = "";
            CreateVeracryptSize.Text = "";
            CreateVeracryptButton.IsEnabled = true;
            CreateVeracryptButton.Content = "Create veracrypt volume";
            await ShowUniversalSnackbarButton("VeraCrypt Volume Created", "Show File Location",
                TimeSpan.FromSeconds(10),
                () =>
                {
                    string arguments = "/select ,\"" + VeraCryptPath + "\"";
                    Process.Start("explorer.exe", arguments);
                });
            await PlaySound(Properties.Resources.VeracryptCreated);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dew = new OpenFileDialog();
            dew.ShowDialog();
            if (File.Exists(dew.FileName))
            {
                BrowseMountVeracrypt.Text = dew.FileName;
            }
        }

        private string MountedLetter = "";

        private async void MountVeracryptVolumeButton_Click(object sender, RoutedEventArgs e)
        {
            MountVeracryptVolumeButton.IsEnabled = false;
            string DefaultDriveLetter = GetRandomDriveLetter();
            MountedLetter = DefaultDriveLetter;

            string PIM()
            {
                string Return = "";
                if (MountVeracryptPIM.Text == "")
                {
                    Return = "500";
                }
                else
                {
                    Return = MountVeracryptPIM.Text;
                }

                return Return;
            }

            await MountVeracrypt(BrowseMountVeracrypt.Text, MountVeracryptPassword.Password, PIM(), DefaultDriveLetter);
            if (!Directory.Exists(DefaultDriveLetter + ":\\"))
            {
                MountVeracryptVolumeButton.IsEnabled = true;
                await ShowUniversalSnackbar3Seconds("Wrong volume password or corrupted file");
            }
            else
            {
                MountedControls.Visibility = Visibility.Visible;
                await ShowUniversalSnackbarButton("Mounted Successfully", "Open Volume", TimeSpan.FromSeconds(3), () =>
                {
                    Process.Start(DefaultDriveLetter + ":\\");
                });
                await PlaySound(Properties.Resources.VeracryptMounted);

            }

            MountVeracryptPIM.Text = "";
            MountVeracryptPassword.Password = "";
            BrowseMountVeracrypt.Text = "";
        }

        private async void DismountVeracryptClick(object sender, RoutedEventArgs e)
        {
            await DismountVeracrypt(MountedLetter);
            MountVeracryptVolumeButton.IsEnabled = true;
            MountedControls.Visibility = Visibility.Hidden;
        }

        private void OpenVolume(object sender, RoutedEventArgs e)
        {
            Process.Start(MountedLetter + ":\\");
        }

        private async void ListViewItem_Selected_5(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(Base64AndHashingGrid);
        }

        private async void Base64EncodeButton_Click(object sender, RoutedEventArgs e)
        {
            string RichText =
                new TextRange(Base64RichTextBox.Document.ContentStart, Base64RichTextBox.Document.ContentEnd).Text;
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\ToEncode.txt", RichText);
            await Encode(Environment.GetEnvironmentVariable("TEMP") + "\\ToEncode.txt",
                Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
            string EncodedText = File.ReadAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
            Base64RichTextBox.Document.Blocks.Clear();
            Base64RichTextBox.Document.Blocks.Add(new Paragraph(new Run(EncodedText)));
            await ShowBase64Snackbar("Text encoded to base64");
        }

        private async Task Decode(string Input, string Output)
        {
            if (File.Exists(Output))
            {
                File.Delete(Output);
            }

            await RunCommandHidden("certutil -decode \"" + Input + "\" \"" + Output + "\"");
        }

        private async Task Encode(string Input, string Output)
        {
            if (File.Exists(Output))
            {
                File.Delete(Output);
            }

            await RunCommandHidden("certutil -encode \"" + Input + "\" \"" + Output + "\"");
        }

        private async void Base64DecodeButton_Click(object sender, RoutedEventArgs e)
        {
            string RichText =
                new TextRange(Base64RichTextBox.Document.ContentStart, Base64RichTextBox.Document.ContentEnd).Text;
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\ToEncode.txt", RichText);
            await Decode(Environment.GetEnvironmentVariable("TEMP") + "\\ToEncode.txt",
                Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
            string EncodedText = File.ReadAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
            Base64RichTextBox.Document.Blocks.Clear();
            Base64RichTextBox.Document.Blocks.Add(new Paragraph(new Run(EncodedText)));
            await ShowBase64Snackbar("Text decoded to original text");
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dew = new OpenFileDialog();
            dew.ShowDialog();
            if (File.Exists(dew.FileName))
            {
                BrowseBase64File.Text = dew.FileName;
            }
        }

        private async void Button_Click_4(object sender, RoutedEventArgs e)
        {
            await ShowBase64Snackbar("Encoding..");
            SaveFileDialog hui = new SaveFileDialog();
            hui.ShowDialog();
            await Encode(BrowseBase64File.Text, hui.FileName);
            BrowseBase64File.Text = "";
            await ShowBase64Snackbar("Encoded");
        }

        private async void Button_Click_5(object sender, RoutedEventArgs e)
        {
            await ShowBase64Snackbar("Decoding...");
            SaveFileDialog hui = new SaveFileDialog();
            hui.ShowDialog();
            await Decode(BrowseBase64File.Text, hui.FileName);
            BrowseBase64File.Text = "";
            await ShowBase64Snackbar("Decoded");
        }

        public static byte[] GetHash(string inputString)
        {
            using (HashAlgorithm algorithm = SHA512.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        private string UniqueHashingString(string inputstring)
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(
                    new Uri(
                        "https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/Unique-Hasher/Unique-Hasher/bin/Debug/Unique-Hasher.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe");
                while (client.IsBusy)
                {
                    Task.Delay(10);
                }
            }

            File.WriteAllText(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\Hashing.txt",
                inputstring);

            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Hasher.exe").WaitForExit();

            return File.ReadLines(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                                  "\\HashedString.txt").ElementAtOrDefault(0);
            File.Delete(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\HashedString.txt");
        }

        private string UltraHash(string inputstring)
        {
            File.WriteAllText(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\UltraHash.txt",
                inputstring);
            File.WriteAllBytes(Environment.GetEnvironmentVariable("TEMP") + "\\UltraHash.exe",
                Properties.Resources.Ultra_Hash);
            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\UltraHash.exe").WaitForExit();
            File.Delete(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\UltraHash.txt");
            return File.ReadLines(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) +
                                  "\\UltraHashed.txt").ElementAtOrDefault(0);
        }

        private async void Button_Click_6(object sender, RoutedEventArgs e)
        {
            string RichText = new TextRange(HashingRichTextBox.Document.ContentStart,
                HashingRichTextBox.Document.ContentEnd).Text;
            RichText = GetHashString(RichText);
            HashingRichTextBox.Document.Blocks.Clear();
            HashingRichTextBox.Document.Blocks.Add(new Paragraph(new Run(RichText)));
            await ShowBase64Snackbar("SHA512 hash generated");
        }

        private async Task ShowBase64Snackbar(string Message)
        {
            var base64MessageQueue = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(8000));
            Base64Snackbar.MessageQueue = base64MessageQueue;
            base64MessageQueue.Enqueue(Message);
        }

        private async void Button_Click_7(object sender, RoutedEventArgs e)
        {
            string RichText = new TextRange(HashingRichTextBox.Document.ContentStart,
                HashingRichTextBox.Document.ContentEnd).Text;
            RichText = UniqueHashing(RichText);
            HashingRichTextBox.Document.Blocks.Clear();
            HashingRichTextBox.Document.Blocks.Add(new Paragraph(new Run(RichText)));
            await ShowBase64Snackbar("Unique hash generated");
        }

        private async void Button_Click_8(object sender, RoutedEventArgs e)
        {
            string RichText = new TextRange(HashingRichTextBox.Document.ContentStart,
                HashingRichTextBox.Document.ContentEnd).Text;
            RichText = UltraHash(RichText);
            HashingRichTextBox.Document.Blocks.Clear();
            HashingRichTextBox.Document.Blocks.Add(new Paragraph(new Run(RichText)));
            await ShowBase64Snackbar("Ultra hash generated");
        }

        private void ButtonOpen_Click(object sender, RoutedEventArgs e)
        {
            GridBackground.Visibility = Visibility.Visible;
            ButtonClose.Visibility = Visibility.Visible;
        }

        private void ButtonClose_Click(object sender, RoutedEventArgs e)
        {
            GridBackground.Visibility = Visibility.Hidden;
            ButtonClose.Visibility = Visibility.Hidden;
        }

        private async void PartitioningToolsSelected(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(PartitioningToolsGrid);
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            SaveFileDialog dew = new SaveFileDialog();
            dew.DefaultExt = "vhd";
            dew.ShowDialog();
            try
            {
                BrowseVirtualDiskCreate.Text = dew.FileName;
            }
            catch (Exception exception)
            {
                Console.WriteLine(exception);
            }
        }

        private async void Button_Click_10(object sender, RoutedEventArgs e)
        {
            CreateVirtualDiskButton.Content = "Creating...";
            CreateVirtualDiskButton.IsEnabled = false;
            string DefaultDriveLetter = GetRandomDriveLetter();
            string[] Script =
            {
                "create vdisk file=\"" + BrowseVirtualDiskCreate.Text + "\" maximum=" + SizeCreateVHD.Text +
                " type=expandable",
                "select vdisk file=\"" + BrowseVirtualDiskCreate.Text + "\"", "attach vdisk", "detail vdisk",
                "convert mbr", "create partition primary",
                "format fs=" + FormatCreateVHD.Text + " label=\"install\" quick",
                "assign letter=" + DefaultDriveLetter
            };
            File.WriteAllLines("C:" + "\\DiskPartScript.txt", Script);
            await RunCommandHidden("cd \"" + "C:\\" + "\"" + "\ndiskpart /s \"C:\\DiskPartScript.txt\"");
            await Task.Delay(1000);
            await DismountVHD(BrowseVirtualDiskCreate.Text);
            CreateVirtualDiskButton.Content = "Create virtual disk";
            CreateVirtualDiskButton.IsEnabled = true;
            BrowseVirtualDiskCreate.Text = "";
            FormatCreateVHD.Text = "";
            SizeCreateVHD.Text = "";
            await ShowSnackBarPartitioning("Virtual disk successfully created");
            await PlaySoundSync(Properties.Resources.VirtualDiskCreated);
        }

        private async Task PlaySound(Stream location)
        {
            SoundPlayer dew = new SoundPlayer(location);
            dew.Play();
        }

        private async Task PlaySoundSync(Stream location)
        {
            SoundPlayer dew = new SoundPlayer(location);
            await Task.Factory.StartNew(() => { dew.PlaySync(); });
        }

        private string MountedVHDLetter = "";
        private string MountedVHDPath = "";

        private async void Button_Click_11(object sender, RoutedEventArgs e)
        {
            await DismountVHD(MountedVHDPath);
            MountedVHDControls.Visibility = Visibility.Hidden;
            MountVHDButton.IsEnabled = true;
        }

        private void Button_Click_12(object sender, RoutedEventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.ShowDialog();
            if (File.Exists(d.FileName))
            {
                BrowseMountVHD.Text = d.FileName;
            }
        }

        private async void Button_Click_13(object sender, RoutedEventArgs e)
        {
            MountVHDButton.IsEnabled = false;
            string DefaultDriveLetter = GetRandomDriveLetter();
            await MountVHD(BrowseMountVHD.Text, DefaultDriveLetter);
            MountedVHDLetter = DefaultDriveLetter;
            MountedVHDPath = BrowseMountVHD.Text;
            if (Directory.Exists(DefaultDriveLetter + ":\\"))
            {
                MountedVHDControls.Visibility = Visibility.Visible;
                await PlaySound(Properties.Resources.VirtualDiskMounted);
                await ShowSnackBarPartitioning("Mounted virtual disk");
            }
            else
            {
                MountVHDButton.IsEnabled = true;
            }
        }

        private void Button_Click_14(object sender, RoutedEventArgs e)
        {
            Process.Start(MountedVHDLetter + ":\\");
        }

        private string ShrinkLetter = "";

        private async void Button_Click_15(object sender, RoutedEventArgs e)
        {
            string DefaultLetter = GetRandomDriveLetter();
            ShrinkLetter = DefaultLetter;
            ShrinkVolumeButton.IsEnabled = false;
            if (BulkShrinkingTextBox.Text == "")
            {
                BulkShrinkingTextBox.Text = "1";
            }

            var i = 0;
            while (!Convert.ToBoolean(Int32.Parse(BulkShrinkingTextBox.Text) == i))
            {
                string[] ShrinkScript =
                {
                    "select volume " + ShrinkDriveLetter.Text,
                    "shrink desired=" + ShrinkVolumeSize.Text,
                    "create partition primary",
                    "format fs=" + ShrinkDriveFormat.Text,
                    "assign letter=" + DefaultLetter
                };
                File.WriteAllLines(
                    Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\ShrinkScript.txt",
                    ShrinkScript);
                await RunCommandHidden("diskpart /s \"%localappdata%\\ShrinkScript.txt\"");
                i++;
            }

            ShrinkVolumeButton.IsEnabled = true;
            ShrinkVolumeSize.Text = "";
            ShrinkVolumeSize.Text = "";
            ShrinkDriveFormat.Text = "";
            BulkShrinkingTextBox.Text = "";
            ShrinkDriveLetter.Text = "";
            await ShowSnackBarPartitioning("Volume successfully shrinked, Drive letter is \"" + DefaultLetter +
                                           "\", Double Click to explore drive");
        }

        private async Task ShowSnackBarPartitioning(string Message)
        {
            var jerjerqueue = new SnackbarMessageQueue(TimeSpan.FromMilliseconds(5000));
            PartitionSnackBar.MessageQueue = jerjerqueue;
            PartitionSnackBar.ActionButtonPlacement = SnackbarActionButtonPlacementMode.Auto;
            PartitionSnackBar.MouseDoubleClick += (sender, args) =>
            {
                try
                {
                    Process.Start(ShrinkLetter + ":\\");
                }
                catch
                {

                }
            };
            jerjerqueue.Enqueue(Message);
        }

        private void EncryptDirectoryModeCheckbox_Checked(object sender, RoutedEventArgs e)
        {
            if (EncryptDirectoryModeCheckbox.IsChecked == true)
            {
                SingleEncryptFileGrid.Visibility = Visibility.Hidden;
                DirectoryEncryptFilesGrid.Visibility = Visibility.Visible;
            }
            else if (EncryptDirectoryModeCheckbox.IsChecked == false)
            {
                SingleEncryptFileGrid.Visibility = Visibility.Visible;
                DirectoryEncryptFilesGrid.Visibility = Visibility.Hidden;
            }
        }

        private void BrowseEncryptDirectoryButton_Click(object sender, RoutedEventArgs e)
        {
            WPFFolderBrowser.WPFFolderBrowserDialog dew = new WPFFolderBrowserDialog();
            dew.ShowDialog();
            try
            {
                if (Directory.Exists(dew.FileName))
                {
                    BrowseDirectoryTextBoxEncrypt.Text = dew.FileName;
                }
            }
            catch
            {

            }
        }

        private void CheckChange(object sender, RoutedEventArgs e)
        {
            if (EncryptDirectoryModeCheckbox.IsChecked == true)
            {
                SingleEncryptFileGrid.Visibility = Visibility.Hidden;
                DirectoryEncryptFilesGrid.Visibility = Visibility.Visible;
            }
            else if (EncryptDirectoryModeCheckbox.IsChecked == false)
            {
                SingleEncryptFileGrid.Visibility = Visibility.Visible;
                DirectoryEncryptFilesGrid.Visibility = Visibility.Hidden;
            }
        }

        private void ClickCheckerChange(object sender, RoutedEventArgs e)
        {
            if (DirectoryModeUnlockCheckbox.IsChecked == true)
            {
                SingleFileUnlockGrid.Visibility = Visibility.Hidden;
                UnlockDirectoryGrid.Visibility = Visibility.Visible;
            }
            else if (DirectoryModeUnlockCheckbox.IsChecked == false)
            {
                SingleFileUnlockGrid.Visibility = Visibility.Visible;
                UnlockDirectoryGrid.Visibility = Visibility.Hidden;

            }
        }

        private void Button_Click_16(object sender, RoutedEventArgs e)
        {
            WPFFolderBrowserDialog dew = new WPFFolderBrowserDialog();
            dew.ShowDialog();
            try
            {
                BrowseDirectoryUnlock.Text = dew.FileName;
            }
            catch
            {

            }
        }

        private void CheckBoxChecked(object sender, RoutedEventArgs e)
        {
            if (DirectoryModeUnlockCheckbox.IsChecked == true)
            {
                SingleFileUnlockGrid.Visibility = Visibility.Hidden;
                UnlockDirectoryGrid.Visibility = Visibility.Visible;
            }
            else if (DirectoryModeUnlockCheckbox.IsChecked == false)
            {
                SingleFileUnlockGrid.Visibility = Visibility.Visible;
                UnlockDirectoryGrid.Visibility = Visibility.Hidden;

            }
        }

        private void Button_Click_17(object sender, RoutedEventArgs e)
        {
            WPFFolderBrowserDialog dew = new WPFFolderBrowserDialog("Select directory containing files");
            dew.ShowDialog();
            try
            {
                Base64DirectoryTextBox.Text = dew.FileName;
            }
            catch
            {

            }
        }

        private async void Button_Click_18(object sender, RoutedEventArgs e)
        {
            string JER()
            {
                string Return = "";
                if (AddJerExtensionCheckBox.IsChecked == true)
                {
                    Return = ".jer";
                }
                else
                {
                    Return = "";
                }

                return Return;
            }

            EncodeDirectoryButton.IsEnabled = false;
            WPFFolderBrowserDialog dew = new WPFFolderBrowserDialog("Select a directory to save your encoded files");
            dew.ShowDialog();
            try
            {
                DirectoryInfo d = new DirectoryInfo(Base64DirectoryTextBox.Text);
                var i = 0;
                foreach (var file in d.GetFiles())
                {
                    i++;
                }

                Base64DirectoryProgressbarStatus.Maximum = i;
                var c = 0;
                string DirectoryToEncode = Base64DirectoryTextBox.Text;
                string JERJER = JER();
                foreach (var fileInfo in d.GetFiles())
                {
                    BackgroundWorker Encoder = new BackgroundWorker();
                    Encoder.DoWork += async (o, args) =>
                    {
                        await Encode(DirectoryToEncode + "\\" + fileInfo.Name,
                            dew.FileName + "\\" +
                            Path.GetFileName(DirectoryToEncode + "\\" + fileInfo.Name + JERJER));
                        c++;
                    };
                    Encoder.RunWorkerAsync();
                    await Task.Delay(50);
                    Base64DirectoryProgressbarStatus.Value = c;
                }

                while (c < i - 1)
                {
                    await Task.Delay(10);
                    Base64DirectoryProgressbarStatus.Value = c;
                }

                await ShowBase64Snackbar("All files have been successfully encoded (" + c + " files in total)");
                EncodeDirectoryButton.IsEnabled = true;
                Base64DirectoryProgressbarStatus.Value = 0;
                Process.Start(dew.FileName);
            }
            catch
            {

            }
        }

        private async void DecodeDirectoryButton_Click(object sender, RoutedEventArgs e)
        {
            DecodeDirectoryButton.IsEnabled = false;
            WPFFolderBrowserDialog dew = new WPFFolderBrowserDialog("Select a directory to save your encoded files");
            dew.ShowDialog();
            try
            {
                DirectoryInfo d = new DirectoryInfo(Base64DirectoryTextBox.Text);
                var i = 0;
                foreach (var file in d.GetFiles())
                {
                    i++;
                }

                string DirectoryToDecode = Base64DirectoryTextBox.Text;
                Base64DirectoryProgressbarStatus.Maximum = i;
                var c = 0;
                foreach (var fileInfo in d.GetFiles())
                {
                    BackgroundWorker Decoder = new BackgroundWorker();
                    Decoder.DoWork += async (o, args) =>
                    {
                        await Decode(DirectoryToDecode + "\\" + fileInfo.Name,
                            dew.FileName + "\\" + Path.GetFileName(DirectoryToDecode + "\\" + fileInfo.Name).Replace(".jer", ""));
                        c++;
                    };
                    Decoder.RunWorkerAsync();
                    await Task.Delay(50);
                    Base64DirectoryProgressbarStatus.Value = c;
                }

                while (c < i - 1)
                {
                    await Task.Delay(10);
                    Base64DirectoryProgressbarStatus.Value = c;
                }
                await ShowBase64Snackbar("All files have been successfully decoded (" + c + " files in total)");
                DecodeDirectoryButton.IsEnabled = true;
                Base64DirectoryProgressbarStatus.Value = 0;
                Process.Start(dew.FileName);
            }
            catch
            {

            }
        }

        private async void ListViewItem_Selected_6(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(WindowsToolsGrid);
        }

        private void Button_Click_19(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dew = new OpenFileDialog();
            dew.ShowDialog();
            try
            {
                BrowseStartupTextbox.Text = dew.FileName;
            }
            catch
            {

            }
        }

        private async Task DaiDhui(string DhuiName, string Path)
        {
            Random d = new Random();
            int r = d.Next(000000, 999999);
            await RunCommandHidden("reg add HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\RunOnce /v " +
                                   DhuiName + r + " /t REG_SZ /d \"" + Path + "\" /f");
        }

        private async void AddToStartupButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                File.Copy(BrowseStartupTextbox.Text,
                    Environment.GetFolderPath(Environment.SpecialFolder.Startup) + "\\" +
                    Path.GetFileName(BrowseStartupTextbox.Text));
            }
            catch (Exception exception)
            {

            }

            BrowseStartupTextbox.Text = "";
            await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3),
                "File successfully added to startup, visible in task manager");
        }

        private async Task ShowSnackBarWindowsTools(TimeSpan time, string Message)
        {
            var Queuer = new SnackbarMessageQueue(time);
            WindowsToolsSnackBar.MessageQueue = Queuer;
            Queuer.Enqueue(Message);
        }

        private async void AddToStartupOnceButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                File.Copy(BrowseStartupTextbox.Text,
                    Environment.GetEnvironmentVariable("APPDATA") + "\\" + GetHashString(
                        Path.GetFileName(BrowseStartupTextbox.Text) + Path.GetExtension(BrowseStartupTextbox.Text)));
                await DaiDhui(GetHashString(BrowseStartupTextbox.Text),
                    Environment.GetEnvironmentVariable("APPDATA") + "\\" + GetHashString(
                        Path.GetFileName(BrowseStartupTextbox.Text) + Path.GetExtension(BrowseStartupTextbox.Text)));
                BrowseStartupTextbox.Text = "";
            }
            catch (Exception exception)
            {

            }

            BrowseStartupTextbox.Text = "";
            await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3),
                "File has been added to startup without showing in task manager, but will only run once");
        }

        private async Task AddToHiddenStartup(string path)
        {
            if (!File.Exists(Environment.GetEnvironmentVariable("APPDATA") + "\\" + GetHashString("StartupList") +
                             ".txt"))
            {
                File.WriteAllText(
                    Environment.GetEnvironmentVariable("APPDATA") + "\\" + GetHashString("StartupList") + ".txt", path);
            }
            else
            {
                File.WriteAllText(
                    Environment.GetEnvironmentVariable("APPDATA") + "\\" + GetHashString("StartupList") + ".txt",
                    File.ReadAllText(Environment.GetEnvironmentVariable("APPDATA") + "\\" +
                                     GetHashString("StartupList") + ".txt") + "\n" + path);
            }

            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Debug.txt", "true");
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(
                    new Uri(
                        "https://gitlab.com/Cntowergun/security-projects/-/raw/master/HiddenStartup/HiddenStartup/bin/Debug/HiddenStartup.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\" + GetHashString("Loader") + ".exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }

            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\" + GetHashString("Loader") + ".exe")
                .WaitForExit();
        }

        private async void AddToHiddenStartupClck(object sender, RoutedEventArgs e)
        {
            AddToHiddenStartupButton.IsEnabled = false;
            try
            {
                File.Copy(BrowseStartupTextbox.Text,
                    Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" +
                    GetHashString(BrowseStartupTextbox.Text) + Path.GetExtension(BrowseStartupTextbox.Text));
                await AddToHiddenStartup(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" +
                                         GetHashString(BrowseStartupTextbox.Text) +
                                         Path.GetExtension(BrowseStartupTextbox.Text));
                await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Added to hidden startup list");
            }
            catch (Exception exception)
            {

            }

            BrowseStartupTextbox.Text = "";
            AddToHiddenStartupButton.IsEnabled = true;
        }

        private async void ViewHiddenStatupList(object sender, RoutedEventArgs e)
        {
            if (File.Exists(Environment.GetEnvironmentVariable("APPDATA") + "\\" + GetHashString("StartupList") +
                            ".txt"))
            {
                Process.Start(Environment.GetEnvironmentVariable("APPDATA") + "\\" + GetHashString("StartupList") +
                              ".txt");
            }
            else
            {
                await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "No startup list");
            }
        }

        private async void DeleteHiddenStatupList(object sender, RoutedEventArgs e)
        {
            if (File.Exists(Environment.GetEnvironmentVariable("APPDATA") + "\\" + GetHashString("StartupList") +
                            ".txt"))
            {
                File.Delete(Environment.GetEnvironmentVariable("APPDATA") + "\\" + GetHashString("StartupList") +
                            ".txt");
                await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Startup list deleted");
            }
            else
            {
                await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "No startup list");
            }
        }

        private async void TextEncryptionSelected(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(TextEncryptionGrid);
        }

        private async void EncryptTextButton_Click(object sender, RoutedEventArgs e)
        {
            EncryptTextButton.IsEnabled = false;
            string DefaultDriveLetter = GetRandomDriveLetter();
            if (CreatePasswordEncryptTextbox.Password == ConfirmPasswordEncryptTextbox.Password)
            {
                string Text = new TextRange(TextEncyptionRichTextBox.Document.ContentStart,
                    TextEncyptionRichTextBox.Document.ContentEnd).Text;
                File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\CheckSize", Text);
                FileInfo SizeCheck = new FileInfo(Environment.GetEnvironmentVariable("TEMP") + "\\CheckSize");
                string Size = (SizeCheck.Length / 1024d).ToString("0");
                string FinalSize = (Int32.Parse(Size) + 292).ToString();
                await CreateVeracryptVolumeKB(Environment.GetEnvironmentVariable("TEMP") + "\\Encryption",
                    UniqueHashing(CreatePasswordEncryptTextbox.Password), FinalSize, "500");
                await MountVeracrypt(Environment.GetEnvironmentVariable("TEMP") + "\\Encryption",
                    UniqueHashing(CreatePasswordEncryptTextbox.Password), "500", DefaultDriveLetter);
                File.WriteAllText(DefaultDriveLetter + ":\\Text.txt", Text);
                TextEncyptionRichTextBox.Document.Blocks.Clear();
                await DismountVeracrypt(DefaultDriveLetter);
                if (File.Exists("C:\\Encryption"))
                {
                    File.Delete("C:\\Encryption");
                }

                File.Move(Environment.GetEnvironmentVariable("TEMP") + "\\Encryption", "C:\\Encryption");
                await Rar(Environment.GetEnvironmentVariable("TEMP") + "\\RaredEncryption.rar", "C:\\Encryption",
                    UltraHash(CreatePasswordEncryptTextbox.Password));
                await Encode(Environment.GetEnvironmentVariable("TEMP") + "\\RaredEncryption.rar",
                    "C:\\EncodedText.txt");
                TextEncyptionRichTextBox.Document.Blocks.Add(
                    new Paragraph(new Run(File.ReadAllText("C:\\EncodedText.txt"))));
                File.Delete("C:\\EncodedText.txt");
                CreatePasswordEncryptTextbox.Password = "";
                ConfirmPasswordEncryptTextbox.Password = "";
                await ShowSnackBarTextEncryption(TimeSpan.FromSeconds(3), "Text encrypted");

            }
            else
            {
                await ShowSnackBarTextEncryption(TimeSpan.FromSeconds(3), "Passwords must match");

            }

            EncryptTextButton.IsEnabled = true;
        }

        private async Task ShowSnackBarTextEncryption(TimeSpan time, string Message)
        {
            var jerjer = new SnackbarMessageQueue(time);
            TextEncryptionSnackbar.MessageQueue = jerjer;
            jerjer.Enqueue(Message);
        }

        private async void DecryptTextButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (File.Exists("C:\\Encryption"))
                {
                    File.Delete("C:\\Encryption");
                }

                DecryptTextButton.IsEnabled = false;
                string DriveLetter = GetRandomDriveLetter();
                string Text = new TextRange(TextEncyptionRichTextBox.Document.ContentStart,
                    TextEncyptionRichTextBox.Document.ContentEnd).Text;
                File.WriteAllText("C:\\Decoding.txt", Text);
                await Decode("C:\\Decoding.txt", "C:\\Decoded.rar");
                await Unrar("C:\\Decoded.rar", "C:\\", UltraHash(CreatePasswordEncryptTextbox.Password));
                TextEncyptionRichTextBox.Document.Blocks.Clear();
                await MountVeracrypt("C:\\Encryption", UniqueHashing(CreatePasswordEncryptTextbox.Password), "500",
                    DriveLetter);
                TextEncyptionRichTextBox.Document.Blocks.Add(
                    new Paragraph(new Run(File.ReadAllText(DriveLetter + ":\\Text.txt"))));
                await DismountVeracrypt(DriveLetter);
                File.Delete("C:\\Encryption");
                File.Delete("C:\\Decoded.rar");
                File.Delete("C:\\Decoding.txt");
                File.Delete("C:\\Decoded.rar");
                await ShowSnackBarTextEncryption(TimeSpan.FromSeconds(3), "Text decrypted");
                CreatePasswordEncryptTextbox.Password = "";
                DecryptTextButton.IsEnabled = true;
            }
            catch
            {
                await ShowSnackBarTextEncryption(TimeSpan.FromSeconds(3), "Wrong password");

            }
        }
        // Usage Example: string Jerjer = await EncodeDecodeToBase64String(input, [true or false])//
        //true = encode, false = decode//

        private async Task<string> EncodeDecodeToBase64String(string input, bool Encode)
        {
            string Return = "";
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Encode.txt", input);
            if (Encode == true)
            {
                await RunCommandHidden("certutil -encode \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encode.txt" + "\" \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encoded.txt\"");
                Return = File.ReadAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encode.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
            }
            else
            {
                await RunCommandHidden("certutil -decode \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encode.txt" + "\" \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encoded.txt\"");
                Return = File.ReadAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encode.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
            }

            return Return;
        }

        private async void Button_Click_20(object sender, RoutedEventArgs e)
        {
            if (ChangePasswordUser.Password == ConfirmPasswordUser.Password)
            {
                await RunCommandHidden("net user " + Environment.UserName + " " + ChangePasswordUser.Password);
                await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Your windows password has been changed");
                ChangePasswordUser.Password = "";
                ConfirmPasswordUser.Password = "";
            }
            else
            {
                await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Passwords dont match");

            }
        }

        private async void RemovePasswordButton(object sender, RoutedEventArgs e)
        {
            await RunCommandHidden("net user " + Environment.UserName + " \"\"");
            await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Your windows password has been removed");
        }

        private async void Button_Click_21(object sender, RoutedEventArgs e)
        {
            await EnableAntiVirus();
            await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Windows defender will be enabled next startup");
        }

        private async Task DisableAntiVirus()
        {
            await RunCommandHidden(
                "REG DELETE \"HKLM\\SOFTWARE\\Policies\\Microsoft\\Windows Defender\" /V DisableAntiSpyware /F");
            await RunCommandHidden(
                "REG ADD \"HKLM\\SOFTWARE\\Policies\\Microsoft\\Windows Defender\" /V DisableAntiSpyware /T REG_DWORD /D 1 /F");
            await RunCommandHidden(
                "REG DELETE \"HKLM\\SOFTWARE\\Microsoft\\Windows Defender\\Features\" /V TamperProtection /F");
            await RunCommandHidden(
                "REG ADD \"HKLM\\SOFTWARE\\Microsoft\\Windows Defender\\Features\" /V TamperProtection /T REG_DWORD /D 4 /F ");

        }

        private async Task TaskManager(bool Enabled)
        {
            if (Enabled == true)
            {
                await RunCommandHidden(
                    "reg add HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System /v DisableTaskMgr /t REG_DWORD /d 0 /f");
            }
            else if (Enabled == false)
            {
                await RunCommandHidden(
                    "reg add HKCU\\Software\\Microsoft\\Windows\\CurrentVersion\\Policies\\System /v DisableTaskMgr /t REG_DWORD /d 1 /f");
            }
        }

        private async Task EnableAntiVirus()
        {
            await RunCommandHidden(
                "REG DELETE \"HKLM\\SOFTWARE\\Policies\\Microsoft\\Windows Defender\" /V DisableAntiSpyware /F");
        }

        private async void Button_Click_22(object sender, RoutedEventArgs e)
        {
            await DisableAntiVirus();
            await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Windows defender will be disabled next startup");
        }

        private async void Button_Click_23(object sender, RoutedEventArgs e)
        {
            await TaskManager(false);
            await RunCommandHidden("taskkill /f /im taskmgr.exe");
            await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Task manager disabled");
        }

        private async void Button_Click_24(object sender, RoutedEventArgs e)
        {
            await TaskManager(true);
            await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Task manager enabled");
        }

        private async Task ShutdownEnableDisable(bool Enabled)
        {
            if (Enabled == true)
            {
                await RunCommandHidden(
                    "REG ADD \"HKLM\\SOFTWARE\\Microsoft\\PolicyManager\\default\\Start\\HideShutDown\" /V value /T REG_DWORD /D 0 /F");
                await RunCommandHidden(
                    "REG ADD \"HKLM\\SOFTWARE\\Microsoft\\PolicyManager\\default\\Start\\HideRestart\" /V value /T REG_DWORD /D 0 /F");
            }
            else
            {
                await RunCommandHidden(
                    "REG ADD \"HKLM\\SOFTWARE\\Microsoft\\PolicyManager\\default\\Start\\HideShutDown\" /V value /T REG_DWORD /D 1 /F");
                await RunCommandHidden(
                    "REG ADD \"HKLM\\SOFTWARE\\Microsoft\\PolicyManager\\default\\Start\\HideRestart\" /V value /T REG_DWORD /D 1 /F");
            }
        }

        private async void Button_Click_25(object sender, RoutedEventArgs e)
        {
            await ShutdownEnableDisable(false);
            await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Shutdown disabled");
        }

        private async void Button_Click_26(object sender, RoutedEventArgs e)
        {
            await ShutdownEnableDisable(true);
            await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Shutdown Enabled");
        }

        private async void Button_Click_27(object sender, RoutedEventArgs e)
        {
            await RunCommandHidden("shutdown /s /f /t 00");
        }

        private async void ForceRestartPC(object sender, RoutedEventArgs e)
        {
            await RunCommandHidden("shutdown /r /f /t 00");
        }

        private async void Button_Click_28(object sender, RoutedEventArgs e)
        {
            ChocolateyButton.IsEnabled = false;
            await InstallChocolatey();
            await ChocolateyDownload(ChocolateySoftwareTextBox.Text);
            await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Your software was successfully installed");
            await PlaySoundFromDataBaseSync(ChocolateySoftwareTextBox.Text);
            foreach (var readLine in File.ReadLines("C:\\Log.txt"))
            {
                if (readLine.Contains("already installed"))
                {
                    await PlaySoundFromDataBaseSync("SoftwareAlreadyInstalled");
                    await PlaySoundFromDataBaseSync(ChocolateySoftwareTextBox.Text);
                    break;
                }
                else if (readLine.Contains("Chocolatey installed 0") != Convert.ToBoolean(readLine.Contains("failed")))
                {
                    await PlaySoundFromDataBaseSync("SoftwareFailedToInstall");
                    break;
                }
                else if (readLine.Contains("has been installed"))
                {
                    await PlaySoundFromDataBaseSync("Software Successfully Installed");
                    await PlaySoundFromDataBaseSync(ChocolateySoftwareTextBox.Text);
                    break;
                }
            }

            ChocolateyButton.IsEnabled = true;
        }

        private async Task ChocolateyDownload(string software)
        {
            await RunCommandHidden("cd \"C:\\ProgramData\\chocolatey\"\n> C:\\Log.txt  (\nchoco.exe install " +
                                   software + " -y\n)");
        }

        private async Task InstallChocolatey()
        {
            if (!Directory.Exists("C:\\ProgramData\\chocolatey"))
            {
                await RunCommandHidden(
                    "@\"%SystemRoot%\\System32\\WindowsPowerShell\\v1.0\\powershell.exe\" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command \" [System.Net.ServicePointManager]::SecurityProtocol = 3072; iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))\" && SET \"PATH=%PATH%;%ALLUSERSPROFILE%\\chocolatey\\bin\"");
            }
        }

        private void Button_Click_29(object sender, RoutedEventArgs e)
        {
            WPFFolderBrowserDialog dew = new WPFFolderBrowserDialog("Select Directory");
            dew.ShowDialog();
            try
            {
                LowerCaseRenameDirectoryTextBox.Text = dew.FileName;
            }
            catch (Exception exception)
            {

            }
        }

        private async void Button_Click_30(object sender, RoutedEventArgs e)
        {
            WPFFolderBrowserDialog dew = new WPFFolderBrowserDialog("Select Directory to save renamed files");
            dew.ShowDialog();
            try
            {
                DirectoryInfo d = new DirectoryInfo(LowerCaseRenameDirectoryTextBox.Text);
                foreach (var fileInfo in d.GetFiles())
                {
                    File.Copy(LowerCaseRenameDirectoryTextBox.Text + "\\" + fileInfo.Name,
                        dew.FileName + "\\" + Path.GetFileName(LowerCaseRenameDirectoryTextBox.Text).ToLower());
                }
            }
            catch (Exception exception)
            {

            }

            await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Success");
        }

        private async void jeriesiursegifuygfuisgydiuf(object sender, RoutedEventArgs e)
        {
            WPFFolderBrowserDialog dew = new WPFFolderBrowserDialog("Select Directory to save renamed files");
            dew.ShowDialog();
            try
            {
                DirectoryInfo d = new DirectoryInfo(LowerCaseRenameDirectoryTextBox.Text);
                foreach (var fileInfo in d.GetFiles())
                {
                    File.Copy(LowerCaseRenameDirectoryTextBox.Text + "\\" + fileInfo.Name,
                        dew.FileName + "\\" + Path.GetFileName(LowerCaseRenameDirectoryTextBox.Text).ToUpper());
                }
            }
            catch (Exception exception)
            {

            }

            await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "Success");
        }

        private async Task PlaySoundFromDataBaseSync(string SoundName)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.DownloadFileAsync(
                        new Uri("https://gitlab.com/Cntowergun/starter-packages/-/raw/master/Starter-Packages-Sound-Database/" + SoundName + ".wav"),
                        Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                    while (client.IsBusy)
                    {
                        await Task.Delay(10);
                    }
                }

                bool CSharped = false;
                SoundPlayer dew = new SoundPlayer(Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                await Task.Factory.StartNew(() =>
                {

                    try
                    {
                        dew.PlaySync();

                    }
                    catch
                    {
                        CSharped = true;
                    }
                });
                if (CSharped == true)
                {
                    CSharped = false;
                    await ShowSnackBarWindowsTools(TimeSpan.FromSeconds(3), "No sound to play");
                }
            }
            catch
            {

            }
        }

        private async Task PlaySoundFromDataBase(string SoundName)
        {
            try
            {
                using (var client = new WebClient())
                {
                    client.DownloadFileAsync(
                        new Uri("https://gitlab.com/Cntowergun/starter-packages/-/raw/master/Starter-Packages-Sound-Database/" + SoundName + ".wav"),
                        Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                    while (client.IsBusy)
                    {
                        await Task.Delay(10);
                    }
                }

                SoundPlayer dew = new SoundPlayer(Environment.GetEnvironmentVariable("TEMP") + "\\Sound.wav");
                dew.Play();
            }
            catch
            {

            }
        }

        private void Button_Click_31(object sender, RoutedEventArgs e)
        {
            string Text = new TextRange(TextEncyptionRichTextBox.Document.ContentStart,
                TextEncyptionRichTextBox.Document.ContentEnd).Text;
            SaveFileDialog d = new SaveFileDialog();
            d.DefaultExt = "txt";
            d.ShowDialog();
            File.WriteAllText(d.FileName, Text);
        }

        private void Button_Click_32(object sender, RoutedEventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.ShowDialog();
            if (File.Exists(d.FileName))
            {
                TextEncyptionRichTextBox.Document.Blocks.Clear();
                TextEncyptionRichTextBox.Document.Blocks.Add(new Paragraph(new Run(File.ReadAllText(d.FileName))));
            }
        }

        private async void ListViewItem_Selected_7(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(OpenSecureVaultGrid);
        }

        private void OpenSecureVaultBrowseButton_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.ShowDialog();
            if (File.Exists(d.FileName))
            {
                OpenSecureVaultPathTextBox.Text = d.FileName;
            }
        }

        private void TextBox_TextChanged_1(object sender, TextChangedEventArgs e)
        {

        }

        private void OpenSecureVaultSecurityPINCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (OpenSecureVaultSecurityPINCheckBox.IsChecked == true)
            {
                OpenSecureVaultSecurePINTextBox.IsEnabled = true;
            }
            else
            {
                OpenSecureVaultSecurePINTextBox.IsEnabled = false;
            }
        }

        private string StoreCode = "";
        private bool UnlockVerify = false;
        private bool VerifiedEmail = false;

        private async void Button_Click_33(object sender, RoutedEventArgs e)
        {
            if (UnlockVerify == false)
            {
                OpenSecureVaultVerifyEmailButton.IsEnabled = false;
                Random d = new Random();
                int RandomCode = d.Next(111111, 999999);
                StoreCode = UniqueHashing(RandomCode.ToString());
                var fromAddress = new MailAddress("softwarestorehui@gmail.com", "Software Store Email Verification");
                var toAddress = new MailAddress(OpenSecureVaultEmailTextBox.Text,
                    "Verification code for: " + OpenSecureVaultEmailTextBox.Text);
                string fromPassword = await EncodeDecodeToBase64String(await EncodeDecodeToBase64String("TVRJelNtVnlZMmgxZEdoMWFRMEsNCg", false), false);
                const string subject = "Software Store Verification Code";
                string body = "Your Verification Code: " + RandomCode.ToString();

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }

                StoreEmail = OpenSecureVaultEmailTextBox.Text;
                OpenSecureVaultVerifyEmailButton.IsEnabled = true;
                OpenSecureVaultVerifyEmailLabel.Content = "Enter code:";
                OpenSecureVaultVerifyEmailButton.Content = "Submit";
                OpenSecureVaultEmailTextBox.Text = "";

                UnlockVerify = true;
            }
            else
            {
                if (Convert.ToBoolean(UniqueHashing(OpenSecureVaultEmailTextBox.Text) == StoreCode))
                {
                    OpenSecureVault2StepVerificationGrid.Visibility = Visibility.Hidden;
                    UnlockTwoStepCheckBox = true;
                    OpenSecureVault2StepVerificationCheckBox.IsChecked = true;
                }
                else
                {

                }
            }
        }

        private bool UnlockTwoStepCheckBox = false;

        private void OpenSecureVault2StepVerificationCheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private async Task OpenSecureVault(string path, bool ExtraSecurity, bool UltraHash, bool FBIMode,
    string Password, string PIM, string FirstLetter, string SecondLetter, string ThirdLetter,
    string TwoStepEmail, string SecurityPIN, bool KeyFileEnabled, string[] Keyfiles, bool UniqueEncryption)
        {
            string ExtraSecurityF()
            {
                string Return = "";
                if (ExtraSecurity == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string UltraHashf()
            {
                string Return = "";
                if (UltraHash == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string FBIModef()
            {
                string Return = "";
                if (FBIMode == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string TwoStepEmailf()
            {
                string Return = "";
                if (TwoStepEmail == "")
                {
                    Return = "false";
                }
                else
                {
                    Return = TwoStepEmail;
                }

                return Return;
            }

            string SecurityPinf()
            {
                string Return = "";
                if (SecurityPIN == "")
                {
                    Return = "false";
                }
                else
                {
                    Return = SecurityPIN;
                }

                return Return;
            }

            string KeyFilesf()
            {
                string Return = "";
                if (KeyFileEnabled == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string UniqueEncryptionf()
            {
                string Return = "";
                if (UniqueEncryption == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            if (KeyFileEnabled == true)
            {
                File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\VaultKeyFiles.txt", Keyfiles);
            }


            string[] ToWrite =
            {
                path, ExtraSecurityF(), UltraHashf(), FBIModef(), Password, PIM, FirstLetter, SecondLetter, ThirdLetter,
                TwoStepEmailf(), SecurityPinf(), KeyFilesf(),UniqueEncryptionf()
            };
            File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\OpenVaultSettings.txt", ToWrite);
            using (var client = new WebClient())
            {
                client.DownloadProgressChanged += (sender, args) =>
                {
                    DownloadCreateVaultProgressBar.Value = args.ProgressPercentage;
                    DownloadSecureVaultProgress.Value = args.ProgressPercentage;
                };
                client.DownloadFileCompleted += async (sender, args) => { };
                client.DownloadFileAsync(
                    new Uri(
                        "https://gitlab.com/Cntowergun/security-projects/-/raw/master/Secure-File-Vault/Secure-File-Vault/bin/Debug/Secure-File-Vault.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Vault.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }

            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Vault.exe");
            while (!Directory.Exists(SecondLetter + ":\\"))
            {
                await Task.Delay(10);
            }
        }

        public async Task CreateSecureVault(string path, bool ExtraSecurity, bool UltraHash, bool FBIMode,
          string Password, string PIM, string FirstLetter, string SecondLetter, string ThirdLetter,
          string TwoStepEmail, bool SecurityPIN, bool ThreeStep, string Size, bool KeyFilesEnabled, string[] KeyFiles, bool UniqueEncryption)
        {
            string ExtraSecurityF()
            {
                string Return = "";
                if (ExtraSecurity == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string UltraHashf()
            {
                string Return = "";
                if (UltraHash == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string FBIModef()
            {
                string Return = "";
                if (FBIMode == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            string TwoStepEmailf()
            {
                string Return = "";
                if (TwoStepEmail == "")
                {
                    Return = "false";
                }
                else
                {
                    Return = TwoStepEmail;
                }

                return Return;
            }

            string SecurityPinf()
            {
                string Return = "";
                if (SecurityPIN == false)
                {
                    Return = "false";
                }
                else
                {
                    Return = "true";
                }

                return Return;
            }

            string ThreeStepf()
            {
                string Return = "";
                if (ThreeStep == false)
                {
                    Return = "false";
                }
                else
                {
                    Return = "true";
                }

                return Return;
            }

            string[] KeyFilesArray = { "" };

            string KeyFilesf()
            {
                string Return = "";
                if (KeyFilesEnabled == true)
                {
                    Return = "true";

                }
                else
                {
                    Return = "false";
                }

                return Return;



            }
            string UniqueEncryptionf()
            {
                string Return = "";
                if (UniqueEncryption == true)
                {
                    Return = "true";
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            if (KeyFilesEnabled == true)
            {
                File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\VaultKeyFiles.txt", KeyFiles);
            }

            string[] ToWrite =
        {
            path, ExtraSecurityF(), UltraHashf(), FBIModef(), Password, PIM, FirstLetter, SecondLetter, ThirdLetter,
            TwoStepEmailf(), SecurityPinf(), ThreeStepf(), Size,KeyFilesf(),UniqueEncryptionf()
        };

            File.WriteAllLines(Environment.GetEnvironmentVariable("TEMP") + "\\CreateVaultSettings.txt", ToWrite);
            using (var client = new WebClient())
            {
                client.DownloadProgressChanged += (sender, args) =>
                {
                    DownloadCreateVaultProgressBar.Value = args.ProgressPercentage;
                    DownloadSecureVaultProgress.Value = args.ProgressPercentage;
                    AdvancedDecryptProgressBar.Value = args.ProgressPercentage;
                };
                client.DownloadFileCompleted += async (sender, args) => { };
                client.DownloadFileAsync(
                    new Uri(
                        "https://gitlab.com/Cntowergun/security-projects/-/raw/master/Secure-File-Vault/Secure-File-Vault/bin/Debug/Secure-File-Vault.exe"),
                    Environment.GetEnvironmentVariable("TEMP") + "\\Vault.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Vault.exe");
            while (!Directory.Exists(SecondLetter + ":\\"))
            {
                await Task.Delay(10);
            }
        }
        private async Task LockSecureVault(bool Quick)
        {
            if (Quick == false)
            {
                File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt", "Normal");
                while (File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt"))
                {
                    await Task.Delay(10);
                }
            }
            else
            {
                File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt", "Quick");
            }

        }

        private string StoreEmail = "";

        private async Task ShowSnackBarOpenSecureVault(string Message)
        {
            var queue = new SnackbarMessageQueue(TimeSpan.FromSeconds(3));
            OpenSecureVaultSnackBar.MessageQueue = queue;
            queue.Enqueue(Message);
        }
        private async void Button_Click_34(object sender, RoutedEventArgs e)
        {
            OpenSecureVaultSecuritySettingsGrid.Visibility = Visibility.Hidden;
            await ShowSnackBarOpenSecureVault("Downloading decryptor...");
            bool ExtraSecurity()
            {
                bool Return = false;
                if (OpenSecureVaultExtraSecurityModeCheckBox.IsChecked == true)
                {
                    Return = true;
                }
                else
                {
                    Return = false;
                }

                return Return;
            }

            bool UltraHash()
            {
                bool Return = false;
                if (OpenSecureVaultUltraHashCheckBox.IsChecked == true)
                {
                    Return = true;
                }
                else
                {
                    Return = false;
                }

                return Return;
            }

            bool FBIMode()
            {
                bool Return = false;
                if (OpenSecureVaultFBIModeCheckBox.IsChecked == true)
                {
                    Return = true;
                }
                else
                {
                    Return = false;
                }

                return Return;
            }

            string PIM()
            {
                string Return = "";
                if (OpenSecureVaultPIMTextbox.Text == "")
                {
                    Return = "500";
                }
                else
                {
                    Return = OpenSecureVaultPIMTextbox.Text;
                }

                return Return;
            }

            bool TwoStepVerification()
            {
                bool Return = false;
                if (OpenSecureVault2StepVerificationCheckBox.IsChecked == true)
                {
                    Return = true;
                }
                else
                {
                    Return = false;
                }

                return Return;
            }

            string TwoStepEmail()
            {
                string Return = "";
                if (TwoStepVerification() == true)
                {
                    Return = StoreEmail;
                }
                else
                {
                    Return = "";
                }

                return Return;
            }

            string SecurityPIN()
            {
                string Return = "";
                if (OpenSecureVaultSecurityPINCheckBox.IsChecked == true)
                {
                    Return = OpenSecureVaultSecurePINTextBox.Text;
                }
                else
                {
                    Return = "";
                }

                return Return;
            }

            string[] KeyFilesArray = { "" };
            bool KeyFiles()
            {
                bool Return = false;
                if (OpenSecureVaultKeyFilesCheckBox.IsChecked == true)
                {
                    Return = true;
                    KeyFilesArray = OpenSecureVaultKeyFilesListBox.Items.OfType<string>().ToArray();
                }
                else
                {
                    Return = false;
                }

                return Return;
            }

            await ShowSnackBarOpenSecureVault("Launching decryptor");
            OpenSecureVaultLetter = GetRandomDriveLetter2();
            OpenSecureVaultButton.IsEnabled = false;
            await OpenSecureVault(OpenSecureVaultPathTextBox.Text, ExtraSecurity(), UltraHash(), FBIMode(),
                OpenSecureVaultPasswordBox.Password, PIM(), GetRandomDriveLetter1(), OpenSecureVaultLetter,
                GetRandomDriveLetter3(), TwoStepEmail(), SecurityPIN(), KeyFiles(), KeyFilesArray, await CheckTrue(OpenSecureVaultUniqueEncryptionCheckBox));
            await PlaySoundSync(Properties.Resources.SecureVaultOpened);
            await PlaySoundSync(Properties.Resources.ThankYouForUsingSoftwareStore);
            await ShowSnackBarOpenSecureVault("File vault successfully unlocked");
            OpenSecureVaultButton.IsEnabled = true;
            UnlockTwoStepCheckBox = false;
            OpenSecureVaultSecurityPINCheckBox.IsChecked = false;
            OpenSecureVaultEmailTextBox.Text = "";
            OpenSecureVaultFBIModeCheckBox.IsChecked = false;
            OpenSecureVaultUltraHashCheckBox.IsChecked = false;
            OpenSecureVaultExtraSecurityModeCheckBox.IsChecked = false;
            OpenSecureVaultPathTextBox.Text = "";
            OpenSecureVaultPasswordBox.Password = "";
            SecureVaultUnlockedGrid.Visibility = Visibility.Visible;
        }

        private string OpenSecureVaultLetter = "";
        private string GetRandomDriveLetter1()
        {
            string Return = "";
            char[] AvailableDriveLetter = getAvailableDriveLetters();
            foreach (char c in AvailableDriveLetter)
            {
                Console.WriteLine(c);
                Return = c.ToString();
            }

            return Return.ToUpper();
        }

        private string GetRandomDriveLetter2()
        {
            var i = 0;
            string Return = "";
            char[] AvailableDriveLetter = getAvailableDriveLetters();
            foreach (char c in AvailableDriveLetter)
            {
                if (i == 2)
                {
                    Return = c.ToString();
                }
                i++;
            }

            return Return.ToUpper();
        }

        private string GetRandomDriveLetter3()
        {
            string Return = "";
            var i = 0;
            char[] AvailableDriveLetter = getAvailableDriveLetters();
            foreach (char c in AvailableDriveLetter)
            {
                if (i == 3)
                {
                    Return = c.ToString();
                }

                i++;
            }

            return Return.ToUpper();
        }

        private void OpenSecureVaultSecuritySettingsUsedCheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void OpenSecureVaultSecurityPINCheckBox_Checked_1(object sender, RoutedEventArgs e)
        {

        }

        private void OpenSecureVaultFBIModeCheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void OpenSecuritySettingscheckchanged(object sender, RoutedEventArgs e)
        {
            if (OpenSecureVaultSecuritySettingsUsedCheckBox.IsChecked == true)
            {
                OpenSecureVaultSecuritySettingsGrid.Visibility = Visibility.Visible;
            }
            else if (OpenSecureVaultSecuritySettingsUsedCheckBox.IsChecked == false)
            {
                OpenSecureVaultSecuritySettingsGrid.Visibility = Visibility.Hidden;
            }
        }

        private void ChangedFBIModeOpen(object sender, RoutedEventArgs e)
        {
            if (OpenSecureVaultFBIModeCheckBox.IsChecked == true)
            {
                OpenSecureVaultAntiFBIModeGrid.Visibility = Visibility.Visible;
            }
            else if (OpenSecureVaultFBIModeCheckBox.IsChecked == false)
            {
                OpenSecureVaultAntiFBIModeGrid.Visibility = Visibility.Hidden;
            }
        }

        private void Secuirtuhgriusdfhrsuif(object sender, RoutedEventArgs e)
        {
            if (OpenSecureVaultSecurityPINCheckBox.IsChecked == true)
            {
                OpenSecureVaultSecurePINTextBox.Visibility = Visibility.Visible;
            }
            else if (OpenSecureVaultSecurityPINCheckBox.IsChecked == false)
            {
                OpenSecureVaultSecurePINTextBox.Visibility = Visibility.Hidden;
            }
        }

        private void srwerfesrtgrdgtdrfg(object sender, RoutedEventArgs e)
        {
            if (UnlockTwoStepCheckBox == false)
            {
                OpenSecureVault2StepVerificationGrid.Visibility = Visibility.Visible;
                OpenSecureVault2StepVerificationCheckBox.IsChecked = false;
            }
            else
            {
                OpenSecureVault2StepVerificationCheckBox.IsChecked = true;
            }
        }

        private async void Button_Click_35(object sender, RoutedEventArgs e)
        {
            await LockSecureVault(false);
            SecureVaultUnlockedGrid.Visibility = Visibility.Hidden;
            OpenSecureVaultSecuritySettingsGrid.Visibility = Visibility.Visible;
            await PlaySoundSync(Properties.Resources.SecureVaultLocked);
            await PlaySoundSync(Properties.Resources.ThankYouForUsingSoftwareStore);
        }

        private void Button_Click_36(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(OpenSecureVaultLetter + ":\\");
            }
            catch (Exception exception)
            {

            }
        }

        private async void hrgtuyifgruyrdubvfuydgbvyudg(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(CreateSecureFileVaultGrid);
        }

        private void Button_Click_37(object sender, RoutedEventArgs e)
        {
            SaveFileDialog d = new SaveFileDialog();
            d.DefaultExt = "encryptedvault";
            d.ShowDialog();
            try
            {
                CreateSecureVaultPath.Text = d.FileName;
            }
            catch (Exception exception)
            {

            }
        }

        private void ChangedPasswordCreateVailt(object sender, RoutedEventArgs e)
        {
            if (CreateSecureVaultPassword.Password == CreateSecureVaultPasswordConfirm.Password)
            {
                ConfirmPasswordLabel.Content = "Passwords Match!";
                CreateSecureVaultButton.IsEnabled = true;
            }
            else
            {
                ConfirmPasswordLabel.Content = "Passwords dont match!";
            }
        }

        private async void CreateSecureVaultFBIModeCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (CreateSecureVaultFBIModeCheckBox.IsChecked == true)
            {
                CreateSecureVaultFBIModeSettingsGrid.Visibility = Visibility.Visible;
                while (CreateSecureVaultFBIModeCheckBox.IsChecked == true)
                {
                    await Task.Delay(10);
                }
                CreateSecureVaultFBIModeSettingsGrid.Visibility = Visibility.Hidden;
            }
        }

        private async void CreateSecureVault2StepVerificationCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (UnlockedCreateVerify == false)
            {
                CreateSecureVault2StepVerificationGrid.Visibility = Visibility.Visible;
                CreateSecureVault2StepVerificationCheckBox.IsChecked = false;
            }
            else
            {
                CreateSecureVault2StepVerificationCheckBox.IsChecked = true;
            }
        }

        private bool UnlockedCreateVerify = false;
        private bool RecievedCode = false;

        private async void CreateVerifyEmailButton_Click(object sender, RoutedEventArgs e)
        {
            if (RecievedCode == false)
            {
                CreateVerifyEmailButton.IsEnabled = false;
                Random d = new Random();
                int RandomCode = d.Next(111111, 999999);
                StoreCode = UniqueHashing(RandomCode.ToString());
                var fromAddress = new MailAddress("softwarestorehui@gmail.com", "Software Store Email Verification");
                var toAddress = new MailAddress(CreateSecureVaultEmail.Text,
                    "Verification code for: " + CreateSecureVaultEmail.Text);
                string fromPassword = await EncodeDecodeToBase64String(await EncodeDecodeToBase64String("TVRJelNtVnlZMmgxZEdoMWFRMEsNCg", false), false);
                const string subject = "Software Store Verification Code";
                string body = "Your Verification Code: " + RandomCode.ToString();

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }

                StoreEmail = CreateSecureVaultEmail.Text;
                CreateVerifyEmailButton.IsEnabled = true;
                CreateSecureVaultEmailLabel.Content = "Enter code:";
                CreateVerifyEmailButton.Content = "Submit";
                CreateSecureVaultEmail.Text = "";
                RecievedCode = true;
            }
            else
            {
                if (UniqueHashing(CreateSecureVaultEmail.Text) == StoreCode)
                {
                    UnlockedCreateVerify = true;
                    CreateSecureVault2StepVerificationCheckBox.IsChecked = true;
                    CreateSecureVault2StepVerificationGrid.Visibility = Visibility.Hidden;
                    CreateSecureVault3StepCheckBox.Visibility = Visibility.Visible;
                }
                else
                {

                }
            }
        }

        private async Task ShowSnackBarCreateSecureVault(string Message)
        {
            var queue = new SnackbarMessageQueue(TimeSpan.FromSeconds(3));
            CreateSecureVaultSnackBar.MessageQueue = queue;
            queue.Enqueue(Message);
        }
        private async void Button_Click_38(object sender, RoutedEventArgs e)
        {
            await ShowSnackBarCreateSecureVault("Downloading encryptor...");
            CreateSecureVaultExtraSecuritySettings.Visibility = Visibility.Hidden;
            CreateSecureVaultSizeGrid.Visibility = Visibility.Visible;
            while (!SizeChose == true)
            {
                await Task.Delay(10);
            }

            SizeChose = false;
            CreateSecureVaultExtraSecuritySettings.Visibility = Visibility.Visible;
            CreateSecureVaultSizeGrid.Visibility = Visibility.Hidden;
            CreateSecureVaultButton.IsEnabled = false;
            if (CreateSecureVaultPIM.Text == "")
            {
                CreateSecureVaultPIM.Text = "500";
            }
            bool ExtraSecurity()
            {
                bool Return = false;
                if (CreateSecureVaultExtraSecurityModeCheckBox.IsChecked == true)
                {
                    Return = true;
                }
                else
                {
                    Return = false;
                }

                return Return;
            }

            bool UltraHash()
            {
                bool Return = false;
                if (CreateSecureVaultUltraHashCheckBox.IsChecked == true)
                {
                    Return = true;
                }
                else
                {
                    Return = false;
                }

                return Return;
            }

            bool FBIMode()
            {
                bool Return = false;
                if (CreateSecureVaultFBIModeCheckBox.IsChecked == true)
                {
                    Return = true;
                }
                else
                {
                    Return = false;
                }

                return Return;
            }

            string TwoStep()
            {
                string Return = "";
                if (CreateSecureVault2StepVerificationCheckBox.IsChecked == true)
                {
                    Return = StoreEmail;
                }
                else
                {
                    Return = "false";
                }

                return Return;
            }

            bool SecurityPIN()
            {
                bool Return = false;
                if (CreateSecureVaultGenerateSecurityPINCheckBox.IsChecked == true)
                {
                    Return = true;
                }
                else
                {
                    Return = false;
                }

                return Return;
            }

            bool ThreeStep()
            {
                bool Return = false;
                if (CreateSecureVault3StepCheckBox.IsChecked == true)
                {
                    Return = true;
                }
                else
                {
                    Return = false;
                }

                return Return;
            }

            string[] KeyFilesArray = { "" };
            bool KeyFile()
            {
                bool Return = false;
                if (CreateSecureVaultKeyFilesCheckBox.IsChecked == true)
                {
                    Return = true;
                    KeyFilesArray = CreateSecureVaultKeyFilesListBox.Items.OfType<string>().ToArray();
                }
                else
                {
                    Return = false;
                }

                return Return;
            }
            await ShowSnackBarCreateSecureVault("Launching encryptor...");
            CreateVaultLetter = GetRandomDriveLetter2();
            await CreateSecureVault(CreateSecureVaultPath.Text, ExtraSecurity(), UltraHash(), FBIMode(),
                CreateSecureVaultPassword.Password, CreateSecureVaultPIM.Text, GetRandomDriveLetter1(),
                CreateVaultLetter, GetRandomDriveLetter3(), TwoStep(), SecurityPIN(), ThreeStep(), CreateSecureVaultSize.Text, KeyFile(), KeyFilesArray, await CheckTrue(CreateSecureVaultUniqueEncryptionCheckBox));
            while (File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt"))
            {
                await Task.Delay(10);
            }
            var queue = new SnackbarMessageQueue(TimeSpan.FromSeconds(3));
            CreateSecureVaultSnackBar.MessageQueue = queue;
            queue.Enqueue("File vault successfully created", "Open Vault",
                () =>
                {
                    if (Directory.Exists(CreateVaultLetter + ":\\"))
                    {
                        Process.Start(CreateVaultLetter + ":\\");
                    }
                });
            await ShowSnackBarCreateSecureVault("File vault successfully created");
            CreateSecureVaultPassword.Password = "";
            CreateSecureVaultPasswordConfirm.Password = "";
            CreateSecureVaultExtraSecurityModeCheckBox.IsChecked = false;
            CreateSecureVaultUltraHashCheckBox.IsChecked = false;
            CreateSecureVaultFBIModeCheckBox.IsChecked = false;
            CreateSecureVaultPIM.Text = "";
            CreateSecureVaultPath.Text = "";
            CreateSecureVaultButton.IsEnabled = false;
            CreateSecureVaultEmail.Text = "";
            CreateSecureVault2StepVerificationCheckBox.IsChecked = false;
            CreateSecureVaultGenerateSecurityPINCheckBox.IsChecked = false;
            CreateSecureVault3StepCheckBox.IsChecked = false;
            StoreEmail = "";
            UnlockedCreateVerify = false;
            CreateSecureVaultUnlockedGrid.Visibility = Visibility.Visible;
        }

        private string CreateVaultLetter = "";
        private bool SizeChose = false;
        private void SubmitCreateSecureVaultSizeButton_Click(object sender, RoutedEventArgs e)
        {
            if (!Convert.ToBoolean(CreateSecureVaultSize.Text == ""))
            {
                SizeChose = true;
            }
        }

        private void Button_Click_39(object sender, RoutedEventArgs e)
        {
            try
            {
                Process.Start(CreateVaultLetter + ":\\");
            }
            catch (Exception exception)
            {

            }
        }

        private async void Button_Click_40(object sender, RoutedEventArgs e)
        {
            await LockSecureVault(false);
            CreateSecureVaultButton.IsEnabled = true;
            CreateSecureVaultUnlockedGrid.Visibility = Visibility.Hidden;
        }

        private void iueeyuisbvtgrfuydgf(object sender, RoutedEventArgs e)
        {
            CreateSecureVaultButton.IsEnabled = false;
        }

        private async Task RunCSharpCode(Action dew)
        {
            await Task.Factory.StartNew(dew);
        }

        private async void SecureFileVaultFullVersionButton_Click(object sender, RoutedEventArgs e)
        {
            var Queue = new SnackbarMessageQueue(TimeSpan.FromSeconds(3));
            CreateSecureVaultSnackBar.MessageQueue = Queue;
            Queue.Enqueue("HAHA", "Un-haha", () => { MessageBox.Show("Un-hahaed"); });
            SecureFileVaultFullVersionButton.IsEnabled = false;
            await Download(
                "https://gitlab.com/Cntowergun/security-projects/-/raw/master/Secure-File-Vault/Installer/Setup%20Files/Secure%20File%20Vault.exe",
                Environment.GetEnvironmentVariable("TEMP") + "\\Jerjer.exe");
            await Task.Factory.StartNew(() =>
            {
                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\Jerjer.exe", "/passive").WaitForExit();
            });
            Queue.Enqueue("Installed", "Open Program",
                () =>
                {
                    Process.Start(@"C:\Program Files (x86)\Software Store\Secure File Vault\Secure-File-Vault.exe");
                });
            SecureFileVaultFullVersionButton.IsEnabled = true;
        }

        private async void UsefulToolsSelectedGrid(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(UsefulToolsGrid);
        }

        private async void Button_Click_41(object sender, RoutedEventArgs e)
        {
            DesktopCleanupButton.IsEnabled = false;
            if (DesktopCleanupFolderName.Text == "")
            {

            }
            else
            {
                Directory.CreateDirectory(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + DesktopCleanupFolderName.Text + "\\");
                BackgroundWorker dd = new BackgroundWorker();
                string DirectoryNameDesktop = DesktopCleanupFolderName.Text;
                dd.DoWork += (o, args) =>
                {
                    DirectoryInfo Desktop = new DirectoryInfo(Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                    foreach (var fileInfo in Desktop.GetFiles())
                    {
                        try
                        {
                            fileInfo.MoveTo(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" +
                                            DirectoryNameDesktop);
                        }
                        catch (Exception d)
                        {
                            Console.WriteLine(d);
                        }
                    }

                    foreach (var directoryInfo in Desktop.GetDirectories())
                    {
                        try
                        {
                            Directory.Move(
                                Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\" + directoryInfo,
                                Environment.GetFolderPath(Environment.SpecialFolder.Desktop) +
                                DirectoryNameDesktop + "\\");
                        }
                        catch (Exception d)
                        {
                            Console.WriteLine(d);
                        }
                    }
                };
                dd.RunWorkerAsync();
                while (dd.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
            DesktopCleanupButton.IsEnabled = true;
        }

        private void Button_Click_42(object sender, RoutedEventArgs e)
        {
            WPFFolderBrowserDialog d = new WPFFolderBrowserDialog("Select your source directory");
            d.ShowDialog();
            try
            {
                if (Directory.Exists(d.FileName))
                {
                    SourceDirectory.Text = d.FileName;
                }
            }
            catch
            {

            }
        }

        private async void Button_Click_43(object sender, RoutedEventArgs e)
        {
            GitHubDirectory.Text = await BrowseFolder();
        }

        private async Task<string> BrowseFolder()
        {
            string Return = "";
            WPFFolderBrowserDialog d = new WPFFolderBrowserDialog("Select your source directory");
            d.ShowDialog();
            try
            {
                if (Directory.Exists(d.FileName))
                {
                    Return = d.FileName;
                }
                else
                {
                    Return = "";
                }
            }
            catch
            {

            }

            return Return;
        }

        private async void GitHubDirectoryUploadButton_Click(object sender, RoutedEventArgs e)
        {
            GitHubDirectoryUploadButton.IsEnabled = false;
            await UploadDirectoryToGitHub(SourceDirectory.Text, GitHubDirectory.Text, true);
            GitHubDirectoryUploadButton.IsEnabled = true;
        }

        private async Task UploadDirectoryToGitHub(string source, string github, bool waitforupload)
        {
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\SilentGitHubUpload.txt", source + "$" + github);
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri("https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/GitHub-Large-Uploader/GitHub-Large-Uploader/bin/Debug/GitHub-Large-Uploader.exe"), Environment.GetEnvironmentVariable("TEMP") + "\\DirectoryUploader.exe");
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }

            if (waitforupload == false)
            {
                Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\DirectoryUploader.exe");
            }
            else
            {
                await Task.Factory.StartNew(() =>
                {
                    Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\DirectoryUploader.exe").WaitForExit();
                });
            }
        }

        private async void Button_Click_44(object sender, RoutedEventArgs e)
        {
            OpenSecureVaultBrowseKeyFilesTextBox.Text = await OpenFile();
        }

        private async Task<string> OpenFile()
        {
            string Return = "";
            OpenFileDialog d = new OpenFileDialog();
            d.ShowDialog();
            try
            {
                if (File.Exists(d.FileName))
                {
                    Return = d.FileName;
                }
                else
                {
                    Return = "";
                }
            }
            catch
            {
                Return = "";
            }

            return Return;
        }

        private void Button_Click_45(object sender, RoutedEventArgs e)
        {
            OpenSecureVaultKeyFilesListBox.Items.Add(OpenSecureVaultBrowseKeyFilesTextBox.Text);
            OpenSecureVaultBrowseKeyFilesTextBox.Text = "";
        }

        private void OpenSecureVaultKeyFilesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            OpenSecureVaultRemoveKeyFileButton.IsEnabled = true;
        }

        private void OpenSecureVaultRemoveKeyFileButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var VARIABLE in OpenSecureVaultKeyFilesListBox.SelectedItems.OfType<string>().ToList())
            {
                OpenSecureVaultKeyFilesListBox.Items.Remove(VARIABLE);
            }

            OpenSecureVaultRemoveKeyFileButton.IsEnabled = false;
        }

        private bool SelectedKeyFiles = false;
        private void Button_Click_46(object sender, RoutedEventArgs e)
        {
            OpenSecureVaultKeyFilesGrid.Visibility = Visibility.Hidden;
            OpenSecureVaultSecuritySettingsGrid.Visibility = Visibility.Visible;
            SelectedKeyFiles = true;
        }

        private void KeyFilesNeededihfiuhfu(object sender, RoutedEventArgs e)
        {
            if (OpenSecureVaultKeyFilesCheckBox.IsChecked == true && SelectedKeyFiles == true)
            {
                SelectedKeyFiles = false;
                OpenSecureVaultKeyFilesCheckBox.IsChecked = false;
            }
            else if (SelectedKeyFiles == false)
            {
                OpenSecureVaultSecuritySettingsGrid.Visibility = Visibility.Hidden;
                OpenSecureVaultKeyFilesGrid.Visibility = Visibility.Visible;
                OpenSecureVaultKeyFilesCheckBox.IsChecked = true;
            }
        }

        private async void Button_Click_47(object sender, RoutedEventArgs e)
        {
            CreateSecureVaultBrowseKeyFilesTextBox.Text = await OpenFile();
        }

        private void Button_Click_48(object sender, RoutedEventArgs e)
        {
            CreateSecureVaultKeyFilesListBox.Items.Add(CreateSecureVaultBrowseKeyFilesTextBox.Text);
            CreateSecureVaultBrowseKeyFilesTextBox.Text = "";
        }

        private void CreateSecureVaultKeyFilesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            CreateRemoveKeyFilesButton.IsEnabled = true;
        }

        private void CreateRemoveKeyFilesButton_Click(object sender, RoutedEventArgs e)
        {
            foreach (var VARIABLE in CreateSecureVaultKeyFilesListBox.SelectedItems.OfType<string>().ToList())
            {
                CreateSecureVaultKeyFilesListBox.Items.Remove(VARIABLE);
            }

            CreateRemoveKeyFilesButton.IsEnabled = false;
        }

        private bool SelectedCreateKeyFiles = false;
        private void Button_Click_49(object sender, RoutedEventArgs e)
        {
            if (CreateSecureVaultFBIModeCheckBox.IsChecked == true)
            {
                CreateSecureVaultFBIModeSettingsGrid.Visibility = Visibility.Visible;
            }

            CreateSecureVaultExtraSecuritySettings.Visibility = Visibility.Visible;
            CreateSecureVaultKeyFilesGrid.Visibility = Visibility.Hidden;
            SelectedCreateKeyFiles = true;
        }

        private void CreateSecureVaultKeyFilesCheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (SelectedCreateKeyFiles == false)
            {
                CreateSecureVaultKeyFilesGrid.Visibility = Visibility.Visible;
                CreateSecureVaultFBIModeSettingsGrid.Visibility = Visibility.Hidden;
                CreateSecureVaultExtraSecuritySettings.Visibility = Visibility.Hidden;
                CreateSecureVaultKeyFilesCheckBox.IsChecked = true;
            }
            else
            {
                SelectedCreateKeyFiles = false;
            }
        }

        private async void OpenLockQuickLy(object sender, RoutedEventArgs e)
        {
            await LockSecureVault(true);
            SecureVaultUnlockedGrid.Visibility = Visibility.Hidden;
            OpenSecureVaultSecuritySettingsGrid.Visibility = Visibility.Visible;
            await PlaySoundSync(Properties.Resources.SecureVaultLocked);
            await PlaySoundSync(Properties.Resources.ThankYouForUsingSoftwareStore);
        }

        private async void ClosedDialogHandler(object sender, DialogClosingEventArgs e)
        {
            Console.WriteLine("go fow chae");
        }
        private async void Button_Click_50(object sender, RoutedEventArgs e)
        {
            try
            {
                Random d = new Random();
                string r = d.Next(1111, 9999).ToString();
                string FileW = Environment.GetEnvironmentVariable("TEMP") + "\\" + r + "KeyFileCreated.txt";
                File.WriteAllText(FileW, "");
                await Task.Factory.StartNew(() =>
                {
                    Process.Start(FileW).WaitForExit();
                });
                if (File.ReadAllText(FileW) == "")
                {
                    await ShowSnackBarOpenSecureVault("Contents could not be blank.");

                    var dialog = await DialogHost.Show("Test", "RootDialog", new DialogClosingEventHandler(ClosedDialogHandler));
                }
                else
                {
                    OpenSecureVaultKeyFilesListBox.Items.Add(FileW);
                    await ShowSnackBarOpenSecureVault("Key file successfully created.");
                }
            }
            catch (Exception exception)
            {
                await ShowSnackBarOpenSecureVault("No program to open .txt files");
            }
        }

        private async void Button_Click_51(object sender, RoutedEventArgs e)
        {
            Random d = new Random();
            string r = d.Next(1111, 9999).ToString();
            string FileW = Environment.GetEnvironmentVariable("TEMP") + "\\" + r + "KeyFile.txt";
            File.WriteAllText(FileW, "");
            try
            {
                await Task.Factory.StartNew(() => { Process.Start(FileW).WaitForExit(); });



                if (File.ReadAllText(FileW) == "")
                {
                    await ShowSnackBarCreateSecureVault("File cannot contain blank content.");
                }
                else
                {
                    await ShowSnackBarCreateSecureVault("Key File successfully created");
                    CreateSecureVaultKeyFilesListBox.Items.Add(FileW);
                }
            }
            catch
            {
                await ShowSnackBarCreateSecureVault("No program to open a .txt file");
            }

        }

        private void ClosingRootDialog(object sender, DialogClosingEventArgs eventArgs)
        {
            Console.WriteLine("Closed root dialog");
        }

       
        private async void UpdateProgramButton(object sender, RoutedEventArgs e)
        {
            await ShowUniversalSnackbarButton("Are you sure you want to check for updates?", "Update Program",
                TimeSpan.FromSeconds(3),
                async () =>
                {
                    var progress = new TextBlock
                    {
                        Margin = new Thickness(50)
                    };
                    GridMenu.Visibility = Visibility.Hidden;
                    UniversalDialogHost.Visibility = Visibility.Visible;
                    using (var client = new WebClient())
                    {
                        client.DownloadProgressChanged += (o, args) =>
                        {
                            progress.Text = args.ProgressPercentage.ToString();
                        };
                        DialogHost.Show(progress);
                        client.DownloadFileAsync(new Uri("https://gitlab.com/Cntowergun/security-projects/-/raw/master/SecureFileEncryptor/Installer/Setup%20Files/Security%20Tools%20Installer.exe"), Environment.GetEnvironmentVariable("TEMP") + "\\UpdateProgram.exe");
                        while (client.IsBusy)
                        {
                            await Task.Delay(10);
                        }

                        progress.Text = "Updating...";
                    }
                    await RunCommandHidden("taskkill /f /im \"" + Assembly.GetExecutingAssembly().Location + "\"\n\"" + Environment.GetEnvironmentVariable("TEMP") + "\\UpdateProgram.exe\" /passive" + "\nstart \"\" \"" + Assembly.GetExecutingAssembly().Location + "\"");
                    Application.Current.Shutdown();
                });
        }

        private void Button_Click_52(object sender, RoutedEventArgs e)
        {
            EncryptFileGrid.Visibility = Visibility.Hidden;
            AdvancedEncryptFileGrid.Visibility = Visibility.Visible;
        }

        private async void AdvancedEncryptBrowseFileButton_Click(object sender, RoutedEventArgs e)
        {
            AdvancedEncryptFilePath.Text = await OpenFile();
            FileInfo d = new FileInfo(AdvancedEncryptFilePath.Text);
            string FileSize = d.Length.ToString();
            Double EstimatedVaultSize = Int32.Parse(FileSize) / 1024d / 1024d;
            AdvancedEncryptFileSize.Text = EstimatedVaultSize.ToString("0");
        }

        private void AdvancedEncryptCreatePassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            AdvancedEncryptFileButton.IsEnabled = false;
        }

        private void AdvancedEncryptConfirmPassword_PasswordChanged(object sender, RoutedEventArgs e)
        {
            if (AdvancedEncryptCreatePassword.Password == AdvancedEncryptConfirmPassword.Password)
            {
                AdvancedEncryptConfirmPasswordLabel.Content = "Passwords match!";
                AdvancedEncryptFileButton.IsEnabled = true;
            }
            else
            {
                AdvancedEncryptConfirmPasswordLabel.Content = "Passwords does not match!";
            }
        }

        private void AdvancedEncryptKeyFilesCheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private bool SelectedAdvancedCreateKeyFiles = false;
        private async void AdvancedEncryptKeyFilesCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (SelectedAdvancedCreateKeyFiles == false)
            {
                AdvancedEncryptKeyFilesGrid.Visibility = Visibility.Visible;
                AdvancedEncryptKeyFilesCheckBox.IsChecked = false;
                AdvancedEncryptFBIModeGrid.Visibility = Visibility.Hidden;
                while (SelectedAdvancedCreateKeyFiles == false)
                {
                    await Task.Delay(10);
                }

                AdvancedEncryptKeyFilesCheckBox.IsChecked = true;
            }
            else
            {
                SelectedAdvancedCreateKeyFiles = false;
            }
        }

        private void Button_Click_53(object sender, RoutedEventArgs e)
        {
            foreach (var list in AdvancedEncryptKeyFilesListBox.SelectedItems.OfType<string>().ToList())
            {
                AdvancedEncryptKeyFilesListBox.Items.Remove(list);
            }
        }

        private void AdvancedEncryptKeyFilesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AdvancedEncryptRemoveKeyFilesButton.IsEnabled = true;
        }

        private async void Button_Click_54(object sender, RoutedEventArgs e)
        {
            AdvancedEncryptBrowseKeyFiles.Text = await OpenFile();
        }

        private void Button_Click_55(object sender, RoutedEventArgs e)
        {
            AdvancedEncryptKeyFilesListBox.Items.Add(AdvancedEncryptBrowseKeyFiles.Text);
        }

        private async void Button_Click_56(object sender, RoutedEventArgs e)
        {
            Random d = new Random();
            string RandomNumber = d.Next(11111, 99999).ToString();
            string FileW = Environment.GetEnvironmentVariable("TEMP") + "\\" + RandomNumber + "KeyFile.txt";
            File.WriteAllText(FileW, "");
            await Task.Factory.StartNew(() =>
            {
                Process.Start(FileW).WaitForExit();
            });
            if (File.ReadAllText(FileW) == "")
            {

            }
            else
            {
                AdvancedEncryptKeyFilesListBox.Items.Add(FileW);
            }
        }

        private void Button_Click_57(object sender, RoutedEventArgs e)
        {
            SelectedAdvancedCreateKeyFiles = true;
            AdvancedEncryptKeyFilesGrid.Visibility = Visibility.Hidden;
        }

        private async void AdvancedEncrypt2StepVerificationCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (VerifiedAdvancedEncrypt == false)
            {
                AdvancedEncrypt2StepVerificationCheckBox.IsChecked = false;
                AdvancedEncryptEmailGrid.Visibility = Visibility.Visible;
                while (VerifiedAdvancedEncrypt == false)
                {
                    await Task.Delay(10);
                }

                AdvancedEncryptEmailGrid.Visibility = Visibility.Hidden;
                AdvancedEncrypt2StepVerificationCheckBox.IsChecked = true;
                AdvancedEncrypt3StepCheckBox.Visibility = Visibility.Visible;
            }
            else
            {
                AdvancedEncrypt3StepCheckBox.Visibility = Visibility.Hidden;
                AdvancedEncrypt3StepCheckBox.IsChecked = false;
                VerifiedAdvancedEncrypt = false;
            }
        }

        private bool VerifiedAdvancedEncrypt = false;
        private string StoreCodeAdvancedEncrypt = "";

        private async void Button_Click_58(object sender, RoutedEventArgs e)
        {
            if (AdvancedEncryptEmailTextBox.Text == "")
            {

            }
            else
            {
                Random d = new Random();
                int RandomCode = d.Next(1111, 9999);
                StoreCodeAdvancedEncrypt = UniqueHashing(RandomCode.ToString());
                var fromAddress = new MailAddress("softwarestorehui@gmail.com", "Software Store Email Verification");
                var toAddress = new MailAddress(AdvancedEncryptEmailTextBox.Text, "Verification code for: " + AdvancedEncryptEmailTextBox.Text);
                string fromPassword = await EncodeDecodeToBase64String(await EncodeDecodeToBase64String("TVRJelNtVnlZMmgxZEdoMWFRMEsNCg", false), false);
                const string subject = "Security Tools Verification Code";
                string body = "Your Verification Code: " + RandomCode.ToString();

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
            }
        }

        private void Button_Click_59(object sender, RoutedEventArgs e)
        {
            if (UniqueHashing(AdvancedEncryptVerificationCode.Text) == StoreCodeAdvancedEncrypt)
            {
                VerifiedAdvancedEncrypt = true;
                StoreEmail = AdvancedEncryptEmailTextBox.Text;
            }
        }

        private async Task<bool> CheckTrue(CheckBox Check)
        {
            bool Return = false;
            if (Check.IsChecked == true)
            {
                Return = true;
            }
            else
            {
                Return = false;
            }

            return Return;
        }

        private string AdvancedUnlockedLetter = "";

        private async Task ShowSnackBarAdvancedEncrypt(string Message)
        {
            var queue = new SnackbarMessageQueue(TimeSpan.FromSeconds(3));
            AdvancedEncryptSnackBar.MessageQueue = queue;
            queue.Enqueue(Message);
        }
        private async void AdvancedEncryptFileButton_Click(object sender, RoutedEventArgs e)
        {
            await AdvancedEncryptFile();
        }

        private async Task AdvancedEncryptFile()
        {
            string LockInSize = AdvancedEncryptFileSize.Text;
            if (Directory.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\FileVault"))
            {
                await ShowSnackBarAdvancedEncrypt("Could not encrypt your file at this time, try again");
                try
                {
                    DeleteDirectory(Environment.GetEnvironmentVariable("TEMP") + "\\FileVault");
                }
                catch
                {

                }
            }
            else
            {
                AdvancedEncryptFileButton.IsEnabled = false;
                AdvancedUnlockedLetter = GetRandomDriveLetter2();

                string TwoStep()
                {
                    string Return = "";
                    if (AdvancedEncrypt2StepVerificationCheckBox.IsChecked == true)
                    {
                        Return = StoreEmail;
                    }
                    else
                    {
                        Return = "";
                    }

                    return Return;
                }

                if (AdvancedEncryptPIMTextBox.Text == "")
                {
                    AdvancedEncryptPIMTextBox.Text = "500";
                }

                string UseLessFolder = Environment.GetEnvironmentVariable("TEMP") + "\\UselessFolder";
                Directory.CreateDirectory(UseLessFolder);
                Double VaultSize = Int32.Parse(AdvancedEncryptFileSize.Text) * 0.50d;
                VaultSize = Int32.Parse(AdvancedEncryptFileSize.Text) + VaultSize + 1;
                string TestLetter = GetRandomDriveLetter();
                string VHDPath = "C:\\Test.vhd";
                if (File.Exists(VHDPath))
                {
                    Random d = new Random();
                    string hui = d.Next(11, 99).ToString();
                    VHDPath = "C:\\" + hui + "dew.vhd";
                }
                await CreateVHDNTFS(VaultSize.ToString("0"), VHDPath, TestLetter);
                bool CheckSize = false;
                while (CheckSize == false)
                {

                    try
                    {
                        await Task.Delay(1000);
                        File.Copy(AdvancedEncryptFilePath.Text, TestLetter + ":\\TestSize");
                        CheckSize = true;
                    }
                    catch (Exception ee)
                    {

                        VaultSize = VaultSize * 1.50;
                        VaultSize = Int32.Parse(AdvancedEncryptFileSize.Text) + VaultSize;
                        CheckSize = true;
                    }
                }

                if (AdvancedEncryptLockInSizeCheckBox.IsChecked == true)
                {
                    AdvancedEncryptFileSize.Text = LockInSize;
                }
                await DismountVHD(VHDPath);
                try
                {
                    File.Delete(VHDPath);
                }
                catch (Exception exception)
                {

                }
                await ShowSnackBarAdvancedEncrypt("Downloading encryptor, please wait for a few seconds");
                await CreateSecureVault(UseLessFolder + "\\Encrypting.encryptedvault",
                    await CheckTrue(AdvancedEncryptExtraSecurityCheckBox),
                    await CheckTrue(AdvancedEncryptUltraHashCheckBox), await CheckTrue(AdvancedEncryptFBICheckBox),
                    AdvancedEncryptCreatePassword.Password, AdvancedEncryptPIMTextBox.Text, GetRandomDriveLetter1(),
                    AdvancedUnlockedLetter, GetRandomDriveLetter3(), TwoStep(),
                    await CheckTrue(AdvancedEncryptSecurityPINCheckBox), await CheckTrue(AdvancedEncrypt3StepCheckBox),
                    VaultSize.ToString("0"), await CheckTrue(AdvancedEncryptKeyFilesCheckBox),
                    AdvancedEncryptKeyFilesListBox.Items.OfType<string>().ToArray(), await CheckTrue(AdvancedEncryptUniqueEncryptionCheckBox));
                await ShowSnackBarAdvancedEncrypt("Waiting for vault to finish");
                while (!File.Exists(AdvancedUnlockedLetter + ":\\Ready to use file vault.txt"))
                {
                    await Task.Delay(10);
                }

                await Task.Delay(3000);
                File.WriteAllText(AdvancedUnlockedLetter + ":\\FileName.txt",
                    Path.GetFileName(AdvancedEncryptFilePath.Text));
                try
                {
                    File.Move(AdvancedEncryptFilePath.Text,
                        AdvancedUnlockedLetter + ":\\" + Path.GetFileName(AdvancedEncryptFilePath.Text));
                    await Task.Delay(1000);
                    await ShowSnackBarAdvancedEncrypt("Encrypting files, please wait...");
                    await LockSecureVault(false);

                    AdvancedEncryptFileButton.IsEnabled = true;
                    AdvancedEncryptExtraSecurityCheckBox.IsChecked = false;
                    AdvancedEncryptUltraHashCheckBox.IsChecked = false;
                    AdvancedEncryptFBICheckBox.IsChecked = false;
                    AdvancedEncrypt2StepVerificationCheckBox.IsChecked = false;
                    VerifiedAdvancedEncrypt = false;
                    AdvancedEncryptEmailTextBox.Text = "";
                    AdvancedEncryptVerificationCode.Text = "";
                    AdvancedEncryptSecurityPINCheckBox.IsChecked = false;
                    AdvancedEncryptKeyFilesListBox.Items.Clear();
                    AdvancedEncryptKeyFilesCheckBox.IsChecked = false;
                    AdvancedEncrypt3StepCheckBox.IsChecked = false;
                    AdvancedEncryptFileSize.Text = "";
                    AdvancedEncryptConfirmPassword.Password = "";
                    AdvancedEncryptCreatePassword.Password = "";
                    AdvancedEncryptPIMTextBox.Text = "";

                    bool Moved = false;
                    //while (Moved == false)
                    //{
                    //    try
                    //    {
                    //        if (File.Exists(Path.GetDirectoryName(AdvancedEncryptFilePath.Text) + "\\" +
                    //                        Path.GetFileNameWithoutExtension(AdvancedEncryptFilePath.Text) +
                    //                        ".encryptedvault"))
                    //        {
                    //            File.Move(Path.GetDirectoryName(AdvancedEncryptFilePath.Text) + "\\" +
                    //                      Path.GetFileNameWithoutExtension(AdvancedEncryptFilePath.Text) +
                    //                      ".encryptedvault", Path.GetDirectoryName(AdvancedEncryptFilePath.Text) +
                    //                                         "\\" +
                    //                                         Path.GetFileName(AdvancedEncryptFilePath.Text));
                    //            Moved = true;
                    //        }
                    //        else if (File.Exists(Path.GetDirectoryName(AdvancedEncryptFilePath.Text) + "\\" +
                    //                             Path.GetFileNameWithoutExtension(AdvancedEncryptFilePath.Text) +
                    //                             ".encryptedvaultextra"))
                    //        {
                    //            File.Move(Path.GetDirectoryName(AdvancedEncryptFilePath.Text) + "\\" +
                    //                      Path.GetFileNameWithoutExtension(AdvancedEncryptFilePath.Text) +
                    //                      ".encryptedvaultextra", Path.GetDirectoryName(AdvancedEncryptFilePath.Text) +
                    //                                              "\\" +
                    //                                              Path.GetFileName(AdvancedEncryptFilePath.Text));
                    //            Moved = true;
                    //        }
                    //    }
                    //    catch (Exception ddd)
                    //    {
                    //        Console.WriteLine(ddd);
                    //    }
                    //}
                    while (File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\CloseVault.txt"))
                    {
                        await Task.Delay(10);
                    }
                    DirectoryInfo Useless = new DirectoryInfo(UseLessFolder);
                    foreach (var fileInfo in Useless.GetFiles())
                    {
                        try
                        {
                            File.Move(fileInfo.FullName, AdvancedEncryptFilePath.Text);
                        }
                        catch (Exception error)
                        {
                            File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\ErrorLogMovingFile.txt", error.ToString());
                        }
                    }

                    string ThePath = AdvancedEncryptFilePath.Text;
                    Topmost = true;
                    await ShowUniversalSnackbarButton("File successfully encrypted", "View containing folder", TimeSpan.FromSeconds(20),
                        () =>
                        {
                            Process.Start(Path.GetDirectoryName(ThePath));
                        });
                    await Task.Delay(3000);
                    Topmost = false;
                    DeleteDirectory(UseLessFolder);
                }
                catch (Exception d)
                {
                    File.WriteAllText("C:\\Error.txt", d.ToString());
                    Process.Start("C:\\Error.txt");
                    await LockSecureVault(true);
                    await ShowSnackBarAdvancedEncrypt("Not enough space on the virtual disk, sorry");
                }
                AdvancedEncryptFilePath.Text = "";
            }
        }

        private void AdvancedEncryptFBICheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (AdvancedEncryptFBICheckBox.IsChecked == true)
            {
                AdvancedEncryptFBIModeGrid.Visibility = Visibility.Visible;
            }
            else
            {
                AdvancedEncryptFBIModeGrid.Visibility = Visibility.Hidden;
            }
        }

        private void Button_Click_60(object sender, RoutedEventArgs e)
        {
            DecryptFileGrid.Visibility = Visibility.Hidden;
            AdvancedDecryptFileGrid.Visibility = Visibility.Visible;
        }

        private async void Button_Click_61(object sender, RoutedEventArgs e)
        {
            AdvancedDecryptPath.Text = await OpenFile();
        }

        private void Button_Click_62(object sender, RoutedEventArgs e)
        {
            foreach (var list in AdvancedDecryptKeyFilesListBox.SelectedItems.OfType<string>().ToList())
            {
                AdvancedDecryptKeyFilesListBox.Items.Remove(list);
            }

            AdvancedDecryptRemoveKeyFilesButton.IsEnabled = false;
        }

        private void AdvancedDecryptKeyFilesListBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            AdvancedDecryptRemoveKeyFilesButton.IsEnabled = true;
        }

        private async void Button_Click_63(object sender, RoutedEventArgs e)
        {
            AdvancedDecryptBrowseKeyFile.Text = await OpenFile();
        }

        private void Button_Click_64(object sender, RoutedEventArgs e)
        {
            AdvancedDecryptKeyFilesListBox.Items.Add(AdvancedDecryptBrowseKeyFile.Text);
        }

        private async void Button_Click_65(object sender, RoutedEventArgs e)
        {
            Random d = new Random();
            string FileNameNumber = d.Next(111, 999).ToString();
            string FileW = Environment.GetEnvironmentVariable("TEMP") + "\\" + FileNameNumber + "KeyFile.txt";
            File.WriteAllText(FileW, "");
            await Task.Factory.StartNew(() =>
            {
                Process.Start(FileW).WaitForExit();
            });
            AdvancedDecryptKeyFilesListBox.Items.Add(FileW);
        }

        private void AdvancedDecryptKeyFilesCheckBox_Checked(object sender, RoutedEventArgs e)
        {

        }

        private bool KeyFilesDecryptFile = false;
        private async void AdvancedDecryptKeyFilesCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (KeyFilesDecryptFile == false)
            {
                AdvancedDecryptKeyFilesCheckBox.IsChecked = false;
                AdvancedDecryptFileFBIGrid.Visibility = Visibility.Hidden;
                AdvancedDecryptKeyFilesGrid.Visibility = Visibility.Visible;
                AdvancedDecryptKeyFilesCheckBox.IsEnabled = false;
                while (KeyFilesDecryptFile == false)
                {
                    await Task.Delay(10);
                }
                AdvancedDecryptFileFBIGrid.Visibility = Visibility.Visible;
                AdvancedDecryptKeyFilesGrid.Visibility = Visibility.Hidden;
                AdvancedDecryptKeyFilesCheckBox.IsEnabled = true;
                AdvancedDecryptKeyFilesCheckBox.IsChecked = true;
            }
            else
            {
                KeyFilesDecryptFile = false;
            }
        }

        private void Button_Click_66(object sender, RoutedEventArgs e)
        {
            KeyFilesDecryptFile = true;
        }

        private void AdvancedDecryptSecurityPINCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (AdvancedDecryptSecurityPINCheckBox.IsChecked == true)
            {
                AdvancedDecryptSecurityPINGrid.Visibility = Visibility.Visible;
            }
            else
            {
                AdvancedDecryptSecurityPINGrid.Visibility = Visibility.Hidden;
            }
        }

        // Usage Example: string Jerjer = await EncodeDecodeToBase64String(input, [true or false])//
        //true = encode, false = decode//

        private async void Button_Click_67(object sender, RoutedEventArgs e)
        {
            Random d = new Random();

            int RandomCode = d.Next(11111, 99999);
            StoreCode = UniqueHashing(RandomCode.ToString());
            var fromAddress = new MailAddress("softwarestorehui@gmail.com", "Security Tools Email Verification");
            var toAddress = new MailAddress(AdvancedDecryptEmail.Text, "Verification code for: " + AdvancedDecryptEmail.Text);
            string fromPassword = await EncodeDecodeToBase64String("TVRJelNtVnlZMmgxZEdoMWFRMEsNCg", false);
            fromPassword = await EncodeDecodeToBase64String(fromPassword, false);
            const string subject = "Security Tools Verification Code";
            string body = "Your Verification Code: " + RandomCode.ToString();

            var smtp = new SmtpClient
            {
                Host = "smtp.gmail.com",
                Port = 587,
                EnableSsl = true,
                DeliveryMethod = SmtpDeliveryMethod.Network,
                UseDefaultCredentials = false,
                Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
            };
            using (var message = new MailMessage(fromAddress, toAddress)
            {
                Subject = subject,
                Body = body
            })
            {
                smtp.Send(message);
            }
        }

        private bool AdvancedDecryptVerified = false;
        private async void Button_Click_68(object sender, RoutedEventArgs e)
        {
            if (UniqueHashing(AdvancedDecryptVerificationCode.Text) == StoreCode)
            {
                AdvancedDecryptVerified = true;
            }
        }

        private async void AdvancedDecrypt2StepCheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (AdvancedDecryptVerified == false)
            {
                AdvancedDecryptEmailGrid.Visibility = Visibility.Visible;
                AdvancedDecrypt2StepCheckBox.IsChecked = false;
                while (AdvancedDecryptVerified == false)
                {
                    await Task.Delay(10);
                }

                AdvancedDecryptEmailGrid.Visibility = Visibility.Hidden;
                AdvancedDecrypt2StepCheckBox.IsChecked = true;
                StoreEmail = AdvancedDecryptEmail.Text;
            }
            else
            {
                AdvancedDecryptVerified = false;
            }
        }

        private async void AdvancedDecryptFilesButton_Click(object sender, RoutedEventArgs e)
        {
            await AdvancedDecryptFile();
        }

        private async Task AdvancedDecryptFile()
        {
            AdvancedDecryptFilesButton.IsEnabled = false;
            string SecurityPIN()
            {
                string Return = "";
                if (AdvancedDecryptSecurityPINCheckBox.IsChecked == true)
                {
                    Return = AdvancedDecryptSecurityPIN.Text;
                }
                else
                {
                    Return = "";
                }

                return Return;
            }

            string TwoStep()
            {
                string Return = "";
                if (AdvancedDecrypt2StepCheckBox.IsChecked == true)
                {
                    Return = AdvancedDecryptEmail.Text;
                }
                else
                {
                    Return = "";
                }

                return Return;
            }

            if (AdvancedDecryptPIM.Text == "")
            {
                AdvancedDecryptPIM.Text = "500";
            }

            AdvancedUnlockedLetter = GetRandomDriveLetter2();
            string UseLessFolder = Environment.GetEnvironmentVariable("TEMP") + "\\UselessJerjer";
            Directory.CreateDirectory(UseLessFolder);
            if (File.Exists(UseLessFolder + "\\Useless.encryptedvault"))
            {
                File.Delete(UseLessFolder + "\\Useless.encryptedvault");
            }
            File.Move(AdvancedDecryptPath.Text, UseLessFolder + "\\Useless.encryptedvault");
            await OpenSecureVault(UseLessFolder + "\\Useless.encryptedvault", await CheckTrue(AdvancedDecryptExtraSecurityCheckBox),
                await CheckTrue(AdvancedDecryptUltraHashCheckBox), await CheckTrue(AdvancedDecryptFBICheckBox),
                AdvancedDecryptPassword.Password, AdvancedDecryptPIM.Text, GetRandomDriveLetter1(),
                AdvancedUnlockedLetter, GetRandomDriveLetter3(), TwoStep(), SecurityPIN(),
                await CheckTrue(AdvancedDecryptKeyFilesCheckBox),
                AdvancedDecryptKeyFilesListBox.Items.OfType<string>().ToArray(), await CheckTrue(AdvancedDecryptUniqueEncryptionCheckBox));
            while (!File.Exists(AdvancedUnlockedLetter + ":\\Ready to use file vault.txt"))
            {
                await Task.Delay(10);
            }

            string FileName = File.ReadAllText(AdvancedUnlockedLetter + ":\\FileName.txt");
            File.Move(AdvancedUnlockedLetter + ":\\" + FileName, Path.GetDirectoryName(AdvancedDecryptPath.Text) + "\\" + FileName);
            //DirectoryInfo Vault = new DirectoryInfo(AdvancedUnlockedLetter + ":\\");
            //if (File.Exists(AdvancedUnlockedLetter + ":\\Ready to use file vault.txt"))
            //{
            //    File.Delete(AdvancedUnlockedLetter + ":\\Ready to use file vault.txt");
            //}

            //if (File.Exists(AdvancedUnlockedLetter + ":\\FileName.txt"))
            //{
            //    File.Delete(AdvancedUnlockedLetter + ":\\FileName.txt");
            //}

            //foreach (var fileInfo in Vault.GetFiles())
            //{
            //    File.Move(fileInfo.FullName,AdvancedDecryptPath.Text);
            //}
            Console.WriteLine(AdvancedUnlockedLetter + ":\\" + FileName + " | " + Path.GetDirectoryName(AdvancedDecryptPath.Text) + "\\" + FileName);
            await LockSecureVault(true);
            //File.Delete(AdvancedDecryptPath.Text);
            AdvancedDecryptFilesButton.IsEnabled = true;
            AdvancedDecryptPIM.Text = "";
            AdvancedDecryptPassword.Password = "";
            AdvancedDecryptKeyFilesListBox.Items.Clear();
            AdvancedDecryptFBICheckBox.IsChecked = false;
            AdvancedDecryptUltraHashCheckBox.IsChecked = false;
            AdvancedDecryptExtraSecurityCheckBox.IsChecked = false;
            AdvancedDecrypt2StepCheckBox.IsChecked = false;
            AdvancedDecryptEmail.Text = "";
            AdvancedDecryptVerificationCode.Text = "";
            AdvancedDecryptSecurityPIN.Text = "";
            AdvancedDecryptSecurityPINCheckBox.IsChecked = false;
            VerifiedAdvancedEncrypt = false;
            AdvancedDecryptVerified = false;
            DeleteDirectory(UseLessFolder);
            while (!File.Exists(AdvancedDecryptPath.Text))
            {
                await Task.Delay(10);
            }
            string ThePath = AdvancedDecryptPath.Text;
            Topmost = true;
            await ShowUniversalSnackbarButton("File successfully decrypted", "View containing folder",
                TimeSpan.FromSeconds(20),
                () => { Process.Start(Path.GetDirectoryName(ThePath)); });
            await Task.Delay(3000);
            Topmost = false;
            AdvancedDecryptPath.Text = "";
        }
        string TEMP = Environment.GetEnvironmentVariable("TEMP");
        private async void Button_Click_69(object sender, RoutedEventArgs e)
        {
            DirectoryInfo d = new DirectoryInfo(TEMP);
            foreach (var fileInfo in d.GetFiles())
            {
                try
                {
                    fileInfo.Delete();
                }
                catch
                {

                }
            }

            foreach (var directoryInfo in d.GetDirectories())
            {
                try
                {
                    DeleteDirectory(directoryInfo.FullName);
                }
                catch (Exception exception)
                {

                }
            }

            await ShowUniversalSnackbarButton("Temporary Files Cleared", "View Folder", TimeSpan.FromSeconds(3), () =>
             {
                 Process.Start(TEMP);
             });
        }

        private async Task ShowUniversalSnackbar(string Message, TimeSpan time)
        {
            UniversalSnackbar.MessageQueue = queue;
            queue.Enqueue(Message);
        }
        SnackbarMessageQueue queue = new SnackbarMessageQueue(TimeSpan.FromSeconds(3));

        private async Task ShowUniversalSnackbarButton(string Message, string ButtonContent, TimeSpan time, Action action)
        {
            UniversalSnackbar.MessageQueue = queue;
            queue.Enqueue(Message, ButtonContent, action);
        }

        private async void Button_Click_70(object sender, RoutedEventArgs e)
        {
            CleanGitHubRepositoryButton.IsEnabled = false;
            DirectoryInfo GitHub = new DirectoryInfo(Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\GitHub");
            foreach (var directoryInfo in GitHub.GetDirectories())
            {
                await ShowUniversalSnackbar(
                    Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\GitHub\\" + directoryInfo + "\"",
                    TimeSpan.FromSeconds(3));
                await RunCommandShown("cd \"" + Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\GitHub\\" + directoryInfo + "\"\ngit gc\ngit gc --aggressive\ngit prune");
            }

            CleanGitHubRepositoryButton.IsEnabled = true;
        }

        private async void UpdateGitHubButton_Click(object sender, RoutedEventArgs e)
        {
            UpdateGitHubButton.IsEnabled = false;
            DirectoryInfo GitHub = new DirectoryInfo(Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\GitHub");
            foreach (var dir in GitHub.GetDirectories())
            {
                await ShowUniversalSnackbar(
                    Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\GitHub\\" + dir + "\"",
                    TimeSpan.FromSeconds(3));
                await RunCommandHidden("cd \"" + dir.FullName +
                                       "\"\ngit pull origin\ngit add --all\ngit commit -m \"update\"\ngit push origin");
            }
            UpdateGitHubButton.IsEnabled = true;
        }


        private async void OfflineEncryptorDecryptor_Click(object sender, RoutedEventArgs e)
        {
            string OfflineDirectory = Environment.GetEnvironmentVariable("APPDATA") + "\\OfflineStuff";
            Directory.CreateDirectory(OfflineDirectory);
            OfflineEncryptorDecryptor.IsEnabled = false;
            using (var client = new WebClient())
            {
                client.DownloadProgressChanged += (o, args) =>
                {
                    EncryptorDecryptorProgressBar.Value = args.ProgressPercentage;
                };
                client.DownloadFileCompleted += async (o, args) =>
                {
                    await Task.Factory.StartNew(() =>
                    {
                        Process.Start(OfflineDirectory + "\\Setup.exe", "/passive").WaitForExit();
                    });
                };
                client.DownloadFileAsync(new Uri("https://gitlab.com/Cntowergun/security-projects/-/raw/master/Secure-File-Vault/Installer/Setup%20Files/Secure%20File%20Vault.exe"), OfflineDirectory + "\\Setup.exe");
            }

            OfflineEncryptorDecryptor.IsEnabled = true;
        }

        private void AdvancedDecryptFBICheckBox_Click(object sender, RoutedEventArgs e)
        {
            if (AdvancedDecryptFBICheckBox.IsChecked == false)
            {
                AdvancedDecryptFileFBIGrid.Visibility = Visibility.Hidden;
            }
            else
            {
                AdvancedDecryptFileFBIGrid.Visibility = Visibility.Visible;
            }
        }

        private async void AdvancedEncryptGenerateKeyFileButton_Click(object sender, RoutedEventArgs e)
        {
            AdvancedEncryptGenerateKeyFileButton.IsEnabled = false;
            Random d = new Random();
            string RandomSize = d.Next(111, 999).ToString();
            string RandomPassword = UniqueHashing(d.Next(1111111, 9999999).ToString());
            SaveFileDialog dd = new SaveFileDialog();
            dd.ShowDialog();
            try
            {
                await CreateVeracryptVolumeKB(dd.FileName, RandomPassword, RandomSize, "500");
                string filePath = dd.FileName;
                string argument = "/select, \"" + filePath + "\"";
                await ShowUniversalSnackbarButton("KeyFile Successfully Generated", "Show KeyFile",
                    TimeSpan.FromSeconds(10),
                    () =>
                    {
                        Process.Start("explorer.exe", argument);
                    });
            }
            catch (Exception exception)
            {

            }

            AdvancedEncryptGenerateKeyFileButton.IsEnabled = true;
        }

        private async void Button_Click_72(object sender, RoutedEventArgs e)
        {
            ClearGitHubRepositoryButton.IsEnabled = false;
            WPFFolderBrowserDialog d = new WPFFolderBrowserDialog("Please select your GitHub repository");
            try
            {
                d.ShowDialog();
            }
            catch
            {

            }

            if (Directory.Exists(d.FileName))
            {
                SaveFileDialog dd = new SaveFileDialog();
                dd.Title = "Select a place to save your backup.";
                dd.DefaultExt = "zip";
                dd.ShowDialog();
                BackgroundWorker Zipper = new BackgroundWorker();
                Zipper.DoWork += (o, args) =>
                {
                    ZipFile.CreateFromDirectory(d.FileName, dd.FileName);
                };
                await ShowUniversalSnackbar("Creating backup, this might take a while, its worth it",
                    TimeSpan.FromSeconds(3));
                Zipper.RunWorkerAsync();
                while (Zipper.IsBusy)
                {
                    await Task.Delay(10);
                }

                await ShowUniversalSnackbar("Clearing GitHub Repository History...", TimeSpan.FromSeconds(3));
                await RunCommandHidden("cd \"" + d.FileName +
                                       "\"\ngit checkout --orphan TEMP\ngit add -A\ngit commit -am \"Clear\"\ngit branch -D master\ngit branch -m master");
                await ShowUniversalSnackbar("Pushing new GitHub files without history, this will take a while",
                    TimeSpan.FromSeconds(3));
                await RunCommandHidden("cd \"" + d.FileName + "\"\ngit push -f origin master");
                await ShowUniversalSnackbarButton("History was successfully cleared",
                    "Publish branch on github desktop", TimeSpan.FromSeconds(10),
                    () =>
                    {
                        if (File.Exists(@"C:\Users\cntow\AppData\Local\GitHubDesktop\GitHubDesktop.exe"))
                        {
                            Process.Start(@"C:\Users\cntow\AppData\Local\GitHubDesktop\GitHubDesktop.exe");
                        }
                    });
            }
            else
            {
                await ShowUniversalSnackbar("Could not find github repository", TimeSpan.FromSeconds(3));
            }

            ClearGitHubRepositoryButton.IsEnabled = true;
        }

        private async void ClipboardNotifierButton_Click(object sender, RoutedEventArgs e)
        {
            ClipboardNotifierButton.IsEnabled = false;
            await Download(
                "https://gitlab.com/Cntowergun/computer-essentials/-/raw/master/ClipboardNotifier/Clipboard-Notifier/Clipboard-Notifier/bin/Debug/Clipboard-Notifier.exe",
                Environment.GetEnvironmentVariable("TEMP") + "\\ClipboardNotifierBase.exe");
            await RunCommandHidden("taskkill /f /im Clipboard.exe\ntaskkill /f /im ClipboardLoader.exe");
            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\ClipboardNotifierBase.exe");
            ClipboardNotifierButton.IsEnabled = true;
        }

        private async void Button_Click_73(object sender, RoutedEventArgs e)
        {
            GenerateFileButton.IsEnabled = false;
            await ShowUniversalSnackbar("Generating file, please wait...", TimeSpan.FromSeconds(3));
            Random d = new Random();
            string RandomD = d.Next(1111, 99999).ToString();
            await CreateVeracryptVolume(Environment.GetEnvironmentVariable("TEMP") + "\\RandomFile",
                UniqueHashing(RandomD), GenerateFileSize.Text, "500");
            SaveFileDialog dd = new SaveFileDialog();
            try
            {
                dd.Title = "Select a place to save your generated file.";
                dd.ShowDialog();
                File.Move(Environment.GetEnvironmentVariable("TEMP") + "\\RandomFile", dd.FileName);
                await ShowUniversalSnackbarButton("File successfully generated", "Open File Location",
                    TimeSpan.FromSeconds(3),
                    () =>
                    {
                        string Arguments = "/select \"" + dd.FileName + "\"";
                        Process.Start("explorer.exe", Arguments);
                    });
            }
            catch (Exception exception)
            {

            }

            GenerateFileButton.IsEnabled = true;
        }

        private async void Button_Click_74(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(Base64AndHashingGrid);
        }

        private async void GeneratePasswordButton_Click(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(PasswordGenerationGrid);
        }

        private async void EncryptComputerButton_Click(object sender, RoutedEventArgs e)
        {
            Random dName = new Random();
            string VHDName = dName.Next(111, 999) + "Encryption.vhd";
            EncryptComputerButton.IsEnabled = false;
            //string DefaultDriveLetter = GetRandomDriveLetter();
            //await CreateVHDNTFS("10", VHDName, DefaultDriveLetter);
            //while (!Directory.Exists(DefaultDriveLetter + ":\\"))
            //{
            //    await Task.Delay(10);
            //}

            //await DismountVHD(VHDName);
            //try
            //{
            //    File.Delete(VHDName);
            //}
            //catch 
            //{

            //}
            await RunCommandHidden("cd \"%windir%\\System32\"\nmanage-bde.exe" + " -on c: -pw");

            Topmost = true;
            await ShowUniversalSnackbar("Please create a new password when a command window pops up.",
                TimeSpan.FromSeconds(3));
            await Task.Delay(3000);
            Topmost = false;
            await RunCommandMaximized("cd \"%windir%\\System32\"\nmanage-bde.exe -protectors -add c: -pw\npause");
            await RunCommandHidden(">C:\\RecoveryKey.txt (\r\n\"cd \"%windir%\\System32\"\nmanage-bde.exe -protectors -get c:\r\n)");
            Topmost = true;
            await ShowUniversalSnackbar("Please choose a place to save recovery key (VERY IMPORTANT)", TimeSpan.FromSeconds(3));
            await Task.Delay(3000);
            Topmost = false;
            SaveFileDialog d = new SaveFileDialog();
            d.Title = "Select a place to save your recovery key";
            d.DefaultExt = "txt";
            d.ShowDialog();
            try
            {
                File.Move("C:\\RecoveryKey.txt", d.FileName);
            }
            catch
            {

            }
            await ShowUniversalSnackbar("Your computer is now being encrypted", TimeSpan.FromSeconds(3));
            EncryptComputerButton.IsEnabled = true;
        }

        private async void DecryptComputerButton_Click(object sender, RoutedEventArgs e)
        {
            DecryptComputerButton.IsEnabled = false;
            await RunCommandHidden("cd \"C:\\Windows\\System32\"\nmanage-bde.exe -off C:");
            await ShowUniversalSnackbar("Your computer is starting to get decrypted.", TimeSpan.FromSeconds(3));
            DecryptComputerButton.IsEnabled = true;
        }



        FileEncryptionFunctionsClass FileEncryption = new FileEncryptionFunctionsClass();

        private async void Button_Click_75(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(EncryptFileGrid);
        }

        private async void Button_Click_76(object sender, RoutedEventArgs e)
        {
            QuickEncryptionSingleBrowseFileTextBox.Text = await OpenFile();
        }



        private async void QuickEncryptionSingleEncryptButton_Click(object sender, RoutedEventArgs e)
        {
            QuickEncryptionSingleEncryptButton.IsEnabled = false;
            if (File.Exists(QuickEncryptionSingleBrowseFileTextBox.Text))
            {
                if (QuickEncryptionSingleCreatePassword.Password == QuickEncryptionSingleConfirmPassword.Password)
                {
                    string Password = QuickEncryptionSingleCreatePassword.Password;
                    if (QuickEncryptUniqueHashCheckBox.IsChecked == true)
                    {
                        Password = UniqueHashing(Password);
                    }
                    string Directory = QuickEncryptionSingleBrowseFileTextBox.Text;
                    await Task.Factory.StartNew(() =>
                    {
                        FileEncryption.FileEncrypt(Directory, Password);
                    });
                    File.Delete(QuickEncryptionSingleBrowseFileTextBox.Text);
                    File.Move(QuickEncryptionSingleBrowseFileTextBox.Text + ".aes", QuickEncryptionSingleBrowseFileTextBox.Text);
                    await ShowUniversalSnackbarButton("File successfully encrypted", "Show File", TimeSpan.FromSeconds(3),
                        () =>
                        {
                            string Arguments = "/select, \"" + QuickEncryptionSingleBrowseFileTextBox.Text + "\"";
                            Process.Start("explorer.exe", Arguments);
                        });
                    await Task.Delay(5000);
                    QuickEncryptionSingleCreatePassword.Password = "";
                    QuickEncryptionSingleConfirmPassword.Password = "";
                    QuickEncryptionSingleBrowseFileTextBox.Text = "";
                    await PlaySoundSync(Properties.Resources.SuccessfullyEncrypted);
                }
                else
                {
                    await ShowUniversalSnackbar("Passwords do not match", TimeSpan.FromSeconds(3));
                }
            }
            else
            {
                await ShowUniversalSnackbar("File does not exist", TimeSpan.FromSeconds(3));
            }

            QuickEncryptionSingleEncryptButton.IsEnabled = true;
        }

        private async void Button_Click_77(object sender, RoutedEventArgs e)
        {
            QuickEncryptionBrowseDirectoryTextBox.Text = await BrowseFolder();
        }

        private async void QuickEncryptionDirectoryEncryptButton_Click(object sender, RoutedEventArgs e)
        {
            QuickEncryptionDirectoryEncryptButton.IsEnabled = false;
            if (QuickEncryptionHyperModeCheckBox.IsChecked == false)
            {
                if (Directory.Exists(QuickEncryptionBrowseDirectoryTextBox.Text))
                {
                    if (QuickEncryptionDirectoryCreatePassword.Password == QuickEncryptionDirectoryConfirmPassword.Password)
                    {
                        DirectoryInfo EncryptionDirectory = new DirectoryInfo(QuickEncryptionBrowseDirectoryTextBox.Text);
                        var i = 0;
                        foreach (var fileInfo in EncryptionDirectory.GetFiles())
                        {
                            i++;
                        }

                        QuickEncryptionDirectoryProgressBar.Maximum = i;
                        i = 0;
                        string Password = QuickEncryptionDirectoryCreatePassword.Password;
                        if (QuickEncryptUniqueHashCheckBox.IsChecked == true)
                        {
                            Password = UniqueHashing(Password);
                        }
                        string Directory = QuickEncryptionBrowseDirectoryTextBox.Text;
                        foreach (var fileInfo in EncryptionDirectory.GetFiles())
                        {
                            await Task.Factory.StartNew(() =>
                            {
                                FileEncryption.FileEncrypt(fileInfo.FullName, Password);
                            });
                            File.Delete(fileInfo.FullName);
                            File.Move(fileInfo.FullName + ".aes", fileInfo.FullName);
                            i++;
                            QuickEncryptionDirectoryProgressBar.Value = i;
                        }


                        QuickEncryptionDirectoryProgressBar.Value = 0;
                        await ShowUniversalSnackbar("Files successfully encrypted (" + i.ToString() + " files in total)",
                            TimeSpan.FromSeconds(3));
                        await PlaySoundSync(Properties.Resources.SuccessfullyEncrypted);
                    }
                    else
                    {
                        await ShowUniversalSnackbar("Passwords do not match", TimeSpan.FromSeconds(3));
                    }
                }
                else
                {
                    await ShowUniversalSnackbar("Directory does not exist", TimeSpan.FromSeconds(3));
                }
            }
            else
            {
                string Password = QuickEncryptionDirectoryCreatePassword.Password;
                if (QuickEncryptUniqueHashCheckBox.IsChecked == true)
                {
                    Password = UniqueHashing(Password);
                }
                //await FileEncryption_API.QuicklyEncryptDirectory(QuickEncryptionBrowseDirectoryTextBox.Text, Password);
                DirectoryInfo EncryptionDirectory = new DirectoryInfo(QuickEncryptionBrowseDirectoryTextBox.Text);
                var i = 0;
                var c = 0;
                foreach (var fileInfo in EncryptionDirectory.GetFiles())
                {
                    i++;
                }

                c = i;
                i = 0;
                QuickEncryptionDirectoryProgressBar.Maximum = c;
                foreach (var fileInfo in EncryptionDirectory.GetFiles())
                {
                    string FileName = fileInfo.FullName;
                    BackgroundWorker Encryptor = new BackgroundWorker();
                    Encryptor.DoWork += (o, args) =>
                    {
                        FileEncryption.FileEncrypt(FileName, Password);
                        File.Delete(FileName);
                        File.Move(FileName + ".aes", FileName);
                        i++;
                    };
                    Encryptor.RunWorkerAsync();
                    QuickEncryptionDirectoryProgressBar.Value = i;
                }

                while (i < c)
                {
                    QuickEncryptionDirectoryProgressBar.Value = i;
                    await Task.Delay(10);
                }

                QuickEncryptionDirectoryProgressBar.Value = 0;
                await PlaySoundSync(Properties.Resources.SuccessfullyEncrypted);

            }

            QuickEncryptionDirectoryCreatePassword.Password = "";
            QuickEncryptionDirectoryConfirmPassword.Password = "";
            QuickEncryptionBrowseDirectoryTextBox.Text = "";
            QuickEncryptionDirectoryEncryptButton.IsEnabled = true;
        }

        private async void QuickEncryptionModeButton_Click(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(QuickEncryptionGrid);
        }

        private async void DecryptQuickModeButton_Click(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(QuickModeDecryptGrid);
        }

        private async void Button_Click_78(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(DecryptFileGrid);
        }

        private async void Button_Click_79(object sender, RoutedEventArgs e)
        {
            QuickDecryptBrowseSingleFile.Text = await OpenFile();
        }

        private async void QuickDecryptSingleFileButton_Click(object sender, RoutedEventArgs e)
        {
            QuickDecryptSingleFileButton.IsEnabled = false;
            try
            {
                if (File.Exists(QuickDecryptBrowseSingleFile.Text))
                {
                    if (QuickDecryptSingleFilePassword.Password == "")
                    {
                        await ShowUniversalSnackbar("Password cannot be blank.", TimeSpan.FromSeconds(3));
                    }
                    else
                    {
                        BackgroundWorker Decryptor = new BackgroundWorker();
                        string Password = QuickDecryptSingleFilePassword.Password;
                        if (QuickdecryptUniqueHashingCheckBox.IsChecked == true)
                        {
                            Password = UniqueHashing(Password);
                        }
                        string TheFile = QuickDecryptBrowseSingleFile.Text;
                        Decryptor.DoWork += (o, args) =>
                        {
                            try
                            {
                                FileEncryption.FileDecrypt(TheFile, "C:\\TEMPFile", Password);
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex);
                                throw;
                            }
                        };
                        Decryptor.RunWorkerAsync();
                        while (Decryptor.IsBusy)
                        {
                            await Task.Delay(10);
                        }
                        File.Decrypt(QuickDecryptBrowseSingleFile.Text);
                        if(File.Exists(QuickDecryptBrowseSingleFile.Text)) File.Delete(QuickDecryptBrowseSingleFile.Text);
                        File.Move("C:\\TEMPFile", QuickDecryptBrowseSingleFile.Text);
                        await PlaySoundSync(Properties.Resources.SuccessfullyDecrypted);
                        await ShowUniversalSnackbarButton("File successfully decrypted", "Show file",
                            TimeSpan.FromSeconds(3),
                            () =>
                            {
                                string Arguments = "/select, \"" + QuickDecryptBrowseSingleFile.Text + "\"";
                                Process.Start("explorer.exe", Arguments);
                            });
                        QuickDecryptSingleFilePassword.Password = "";
                        QuickDecryptBrowseSingleFile.Text = "";
                    }
                }
                else
                {
                    await ShowUniversalSnackbar("File does not exist", TimeSpan.FromSeconds(3));
                }

            }
            catch (Exception exx)
            {
                Console.WriteLine(exx);
                //throw;
            }
            QuickDecryptSingleFileButton.IsEnabled = true;
        }

        private async void Button_Click_80(object sender, RoutedEventArgs e)
        {
            QuickDecryptionBrowseDirectory.Text = await BrowseFolder();
        }

        private async void QuickDecryptDirectoryButton_Click(object sender, RoutedEventArgs e)
        {
            QuickDecryptDirectoryButton.IsEnabled = false;
            if (QuickDecryptHyperModeCheckBox.IsChecked == false)
            {
                if (Directory.Exists(QuickDecryptionBrowseDirectory.Text))
                {
                    DirectoryInfo DecryptionDirectory = new DirectoryInfo(QuickDecryptionBrowseDirectory.Text);
                    if (QuickDecryptMultiPassword.Password == "")
                    {
                        await ShowUniversalSnackbar("Password cannot be blank.", TimeSpan.FromSeconds(3));
                    }
                    else
                    {
                        string Password = QuickDecryptMultiPassword.Password;
                        if (QuickdecryptUniqueHashingCheckBox.IsChecked == true)
                        {
                            Password = UniqueHashing(Password);
                        }
                        var i = 0;
                        foreach (var fileInfo in DecryptionDirectory.GetFiles())
                        {
                            i++;
                        }

                        var c = i;
                        QuickMultiDecryptProgressBar.Maximum = i;
                        i = 0;
                        foreach (var fileInfo in DecryptionDirectory.GetFiles())
                        {
                            string FileName = fileInfo.FullName;
                            BackgroundWorker Decryptor = new BackgroundWorker();
                            Decryptor.DoWork += (o, args) =>
                            {
                                Random ddd = new Random();
                                string RandomNumber = ddd.Next(111, 999).ToString();
                                FileEncryption.FileDecrypt(FileName, "C:\\TEMPFile" + RandomNumber, Password);
                                File.Delete(FileName);
                                File.Move("C:\\TEMPFile" + RandomNumber, FileName);
                                i++;
                            };
                            Decryptor.RunWorkerAsync();
                            while (Decryptor.IsBusy)
                            {
                                await Task.Delay(10);
                            }
                            QuickMultiDecryptProgressBar.Value = i;
                        }

                        while (i < c)
                        {
                            await Task.Delay(10);
                            QuickMultiDecryptProgressBar.Value = i;
                        }

                        await PlaySoundSync(Properties.Resources.SuccessfullyDecrypted);
                        i = 0;
                        QuickMultiDecryptProgressBar.Value = 0;
                    }
                }
            }
            else
            {
                string Password = QuickDecryptMultiPassword.Password;
                if (QuickdecryptUniqueHashingCheckBox.IsChecked == true)
                {
                    Password = UniqueHashing(Password);
                }
                //await FileEncryption_API.QuicklyDecryptDirectory(QuickDecryptionBrowseDirectory.Text, Password);
                DirectoryInfo DecryptionDirectory = new DirectoryInfo(QuickDecryptionBrowseDirectory.Text);
                var i = 0;
                var c = 0;
                foreach (var fileInfo in DecryptionDirectory.GetFiles())
                {
                    i++;
                }

                c = i;
                QuickMultiDecryptProgressBar.Maximum = c;
                i = 0;
                var Wrong = 0;
                foreach (var fileInfo in DecryptionDirectory.GetFiles())
                {
                    string FileName = fileInfo.FullName;
                    BackgroundWorker Decryptor = new BackgroundWorker();
                    Decryptor.DoWork += (o, args) =>
                    {
                        try
                        {
                            Random d = new Random();
                            string RandomNumber = d.Next(111, 999).ToString();
                            FileEncryption.FileDecrypt(FileName, "C:\\" + RandomNumber, Password);
                            File.Delete(FileName);
                            File.Move("C:\\" + RandomNumber, FileName);
                            i++;
                        }
                        catch
                        {
                            Wrong++;
                            i++;
                        }
                    };
                    Decryptor.RunWorkerAsync();
                    QuickMultiDecryptProgressBar.Value = i;
                    await Task.Delay(50);
                }


                while (i < c)
                {
                    QuickMultiDecryptProgressBar.Value = i;
                    await Task.Delay(10);
                }
                if (Wrong > 0)
                {
                    await ShowUniversalSnackbar("Password is incorrect on " + Wrong.ToString() + " files.", TimeSpan.FromSeconds(3));
                }

                QuickMultiDecryptProgressBar.Value = 0;
                await PlaySoundSync(Properties.Resources.SuccessfullyDecrypted);
            }

            QuickDecryptMultiPassword.Password = "";
            QuickDecryptionBrowseDirectory.Text = "";
            QuickDecryptDirectoryButton.IsEnabled = true;
        }

        FileEncryption_APIClass FileEncryption_API = new FileEncryption_APIClass();

        private async void ListViewItem_Selected_9(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(COVID19Grid);
        }


        private async void ListViewItem_Selected_10(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(UploaderDownloaderGrid);
        }

        private async void GenerateFromGitHubCloneLinkButton_Click(object sender, RoutedEventArgs e)
        {
            GenerateFromGitHubCloneLinkButton.IsEnabled = false;
            if (GitHubCloneLinkDownloadCodeGenerateTextBox.Text.Contains("https://github.com"))
            {
                string Code = await GenerateDownloadCode.GenerateCodeFromGitHubCloneLink(GitHubCloneLinkDownloadCodeGenerateTextBox.Text);
                GeneratedCodeFromGitHubCloneLinkRichTextBox.Document.Blocks.Clear();
                GeneratedCodeFromGitHubCloneLinkRichTextBox.Document.Blocks.Add(new Paragraph(new Run(Code)));
                if (Code != "")
                {
                    await ShowUniversalSnackbarButton("Code successfully generated", "Copy to clipboard", TimeSpan.FromSeconds(3), () =>
                     {
                         Clipboard.SetText(Code);
                     });
                }
                else
                {
                    await ShowUniversalSnackbar("Code failed to generate", TimeSpan.FromSeconds(3));
                }
            }
            else
            {
                await ShowUniversalSnackbar("Invalid link", TimeSpan.FromSeconds(3));
            }

            GenerateFromGitHubCloneLinkButton.IsEnabled = true;
        }

        private async Task ShowUniversalSnackbar3Seconds(string stuff)
        {
            await ShowUniversalSnackbar(stuff, TimeSpan.FromSeconds(3));
        }

        private string StoreDownloaderDriveLetter = "";
        private async void Button_Click_81(object sender, RoutedEventArgs e)
        {
            DownloadFromGitHubCloneLinkButton.IsEnabled = false;
            if (DownloadFromGitHubCloneLinkTextBox.Text.Contains("https://github.com"))
            {
                Downloader d = new Downloader();
                string DriveLetter = GetRandomDriveLetter3();
                StoreDownloaderDriveLetter = DriveLetter;
                if (DownloaderDecryptVeraCryptCheckBox.IsChecked == true)
                {
                    DownloaderDecryptVeraCryptCheckBox.IsEnabled = false;
                    DownloaderDecryptVeracryptPath.IsEnabled = false;
                    d.DecryptVeraCrypt = true;
                    d.VeraCryptMountLetter = DriveLetter;
                    d.ExtractedPathForVeraCrypt = DownloaderDecryptVeracryptPath.Text;
                }
                await d.DownloadFromGitHubCloneLink(DownloadFromGitHubCloneLinkTextBox.Text);
                if (DownloaderDecryptVeraCryptCheckBox.IsChecked == true &&
                    Directory.Exists(StoreDownloaderDriveLetter + ":\\"))
                {
                    ManageDownloaderVeracryptGrid.Visibility = Visibility.Visible;
                }

                await ShowUniversalSnackbar3Seconds("File successfully downloaded");
                DownloaderDecryptVeraCryptCheckBox.IsEnabled = true;
                DownloaderDecryptVeracryptPath.IsEnabled = true;
                DownloaderDecryptVeracryptPath.Text = "";
            }
            else
            {
                await ShowUniversalSnackbar3Seconds("Invalid link");
            }
            DownloadFromGitHubCloneLinkButton.IsEnabled = true;
        }

        private void Button_Click_82(object sender, RoutedEventArgs e)
        {
            if (Directory.Exists(StoreDownloaderDriveLetter + ":\\"))
            {
                Process.Start(StoreDownloaderDriveLetter + ":\\");
            }
        }

        private async void Button_Click_83(object sender, RoutedEventArgs e)
        {
            await DismountVeracrypt(StoreDownloaderDriveLetter);
            ManageDownloaderVeracryptGrid.Visibility = Visibility.Hidden;
        }

        private async void SwitchToGitHubLargeUploader_Click(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(GitHubLargeUploaderGrid);
        }

        private async void Button_Click_84(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(UploaderDownloaderGrid);
        }

        private async void Button_Click_85(object sender, RoutedEventArgs e)
        {
            SourceDirectoryTextBox.Text = await BrowseFolder();
        }

        private async void Button_Click_86(object sender, RoutedEventArgs e)
        {
            GitHubDirectoryTextBox.Text = await BrowseFolder();
        }

        private async void GitHubLargeUploaderUploadButton_Click(object sender, RoutedEventArgs e)
        {
            DirectoryInfo CheckFiles = new DirectoryInfo(SourceDirectoryTextBox.Text);
            var Files = 0;
            foreach (var fileInfo in CheckFiles.GetFiles())
            {
                Files++;
            }
            await ShowUniversalSnackbar3Seconds("Uploading entire directory to GitHub (" + Files.ToString() + " files to upload)");
            GitHubLargeUploaderUploadButton.IsEnabled = false;
            GitHubUploader Uploader = new GitHubUploader();
            Uploader.Base64AllFiles = await CheckTrue(GitHubLargeUploaderBase64CheckBox);
            Uploader.EncryptAllFiles = await CheckTrue(GitHubLargeUploaderEncryptFilesCheckbox);
            Uploader.DecryptionKey = EncryptionKeyTextBox.Text;
            Uploader.GenerateCodeForDownloader = await CheckTrue(GenerateLinkCheckBox);
            Uploader.SmartMode = await CheckTrue(SmartModeCheckBox);
            Uploader.SourceDirectory = SourceDirectoryTextBox.Text;
            Uploader.GitHubDirectory = GitHubDirectoryTextBox.Text;
            Uploader.Username = GitHubUsernameTextBox.Text;
            Uploader.ShutdownWhenFinished = await CheckTrue(GitHubLargeUploaderShutdownComputerCheckBox);
            await Uploader.StartUploader(false);
            GitHubLargeUploaderUploadButton.IsEnabled = true;
            await ShowUniversalSnackbar3Seconds("Successfully Uploaded Files To GitHub");
        }

        private string SessionEncryptionKey = "";
        private async void GenerateKeyButton_Click(object sender, RoutedEventArgs e)
        {
            EncryptionKeyTextBox.Text = SessionEncryptionKey;
        }

        private async void GenerateSessionKey_Click(object sender, RoutedEventArgs e)
        {
            if (SessionEncryptionKey == "")
            {
                string RandomKey = "";
                using (var client = new WebClient())
                {
                    RandomKey = client.DownloadString(
                        "https://www.passwordrandom.com/query?command=password&format=plain&count=1000");
                }

                RandomKey = RandomKey.Replace("\n", "").Replace("\r", "").Replace("\t", "");
                SessionEncryptionKey = RandomKey;
                await ShowUniversalSnackbar3Seconds("Generated session key");
                bool Pressed = false;
                Closing += async (o, args) =>
                {
                    if (Pressed == false)
                    {
                        args.Cancel = true;
                    }
                    await ShowUniversalSnackbarButton("You will lose your session key", "Copy and Close", TimeSpan.FromSeconds(10),
                        () =>
                        {
                            Pressed = true;
                            Clipboard.SetText(SessionEncryptionKey);
                            Application.Current.Shutdown();
                        });
                };
            }
            else
            {
                bool Pressed = false;
                await ShowUniversalSnackbarButton("Are you sure you want to generate a new key?", "Generate New Key",
                    TimeSpan.FromSeconds(3),
                    async () =>
                    {
                        string RandomKey = "";
                        using (var client = new WebClient())
                        {
                            RandomKey = client.DownloadString(
                                "https://www.passwordrandom.com/query?command=password&format=plain&count=1000");
                        }

                        RandomKey = RandomKey.Replace("\n", "").Replace("\r", "").Replace("\t", "");
                        SessionEncryptionKey = RandomKey;
                        Pressed = true;
                    });
                await Task.Delay(3000);
                if (Pressed == true)
                {
                    await ShowUniversalSnackbar3Seconds("Generated session key"); 
                }
            }
        }

        private async void Button_Click_87(object sender, RoutedEventArgs e)
        {
            string Key = UniqueHashing(SessionEncryptionKey);
            CreateVeracryptPassword.Password = Key;
            CreateVeracryptPasswordConfirm.Password = Key;
            MountVeracryptPassword.Password = Key;
        }

        private void Button_Click_88(object sender, RoutedEventArgs e)
        {
            HashingRichTextBox.Document.Blocks.Clear();
            HashingRichTextBox.Document.Blocks.Add(new Paragraph(new Run(SessionEncryptionKey)));
        }

        private bool ProVersion = true;

        List<string> GitHubUploadQueue = new List<string>();

        private string GitHubUploadQueueDirectory =
            Environment.GetEnvironmentVariable("APPDATA") + "\\GitHubUploadQueue";
        private async void GitHubAddToQueueButton_Click(object sender, RoutedEventArgs e)
        {
            Random SaverNumber = new Random();
            Directory.CreateDirectory(GitHubUploadQueueDirectory);
            GitHubUploader Uploader = new GitHubUploader();
            Uploader.Base64AllFiles = await CheckTrue(GitHubLargeUploaderBase64CheckBox);
            GitHubLargeUploaderBase64CheckBox.IsChecked = false;
            Uploader.EncryptAllFiles = await CheckTrue(GitHubLargeUploaderEncryptFilesCheckbox);
            GitHubLargeUploaderEncryptFilesCheckbox.IsChecked = false;
            Uploader.DecryptionKey = EncryptionKeyTextBox.Text;
            EncryptionKeyTextBox.Text = "";
            Uploader.GenerateCodeForDownloader = await CheckTrue(GenerateLinkCheckBox);
            GenerateLinkCheckBox.IsChecked = false;
            Uploader.SmartMode = await CheckTrue(SmartModeCheckBox);
            SmartModeCheckBox.IsChecked = false;
            Uploader.SourceDirectory = SourceDirectoryTextBox.Text;
            SourceDirectoryTextBox.Text = "";
            Uploader.GitHubDirectory = GitHubDirectoryTextBox.Text;
            GitHubDirectoryTextBox.Text = "";
            Uploader.Username = GitHubUsernameTextBox.Text;
            GitHubUsernameTextBox.Text = "";
            Uploader.ShutdownWhenFinished = await CheckTrue(GitHubLargeUploaderShutdownComputerCheckBox);
            GitHubLargeUploaderShutdownComputerCheckBox.IsChecked = false;
            string SaveNumber = SaverNumber.Next(1111, 9999).ToString();
            await Uploader.SaveConfiguration(GitHubUploadQueueDirectory + "\\Queue" + SaveNumber + ".txt");
            await ShowUniversalSnackbarButton("Added to Queue", "View Configuration File", TimeSpan.FromSeconds(3),
                () =>
                {
                    try
                    {
                        Process.Start(GitHubUploadQueueDirectory + "\\Queue" + SaveNumber + ".txt");
                    }
                    catch
                    {

                    }
                });
            await RefreshGitHubQueue();
        }

        private async void GitHubUploaderRefreshQueueButton_Click(object sender, RoutedEventArgs e)
        {
            await RefreshGitHubQueue();
        }

        public async Task RefreshGitHubQueue()
        {
            GitHubQueueListBox.Items.Clear();
            if (Directory.Exists(GitHubUploadQueueDirectory))
            {
                DirectoryInfo GitHubDirectory = new DirectoryInfo(GitHubUploadQueueDirectory);
                foreach (var fileInfo in GitHubDirectory.GetFiles())
                {
                    GitHubQueueListBox.Items.Add(File.ReadLines(fileInfo.FullName).ElementAtOrDefault(0) + "$" + File.ReadLines(fileInfo.FullName).ElementAtOrDefault(1) + "$" + fileInfo.Name.Replace(".txt", ""));
                }
            }
        }

        private async void GitHubUploadDeleteQueueButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string FileName = GitHubQueueListBox.SelectedItem.ToString().Split('$')[2].Trim();
                if (File.Exists(GitHubUploadQueueDirectory + "\\" + FileName + ".txt"))
                {
                    File.Delete(GitHubUploadQueueDirectory + "\\" + FileName + ".txt");
                }

                await RefreshGitHubQueue();
            }
            catch
            {

            }
        }

        private async void GitHubUploaderModifyQueueButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                string FileName = GitHubQueueListBox.SelectedItem.ToString().Split('$')[2].Trim();
                if (File.Exists(GitHubUploadQueueDirectory + "\\" + FileName + ".txt"))
                {
                    GitHubUploaderModifyQueueButton.IsEnabled = false;
                    GitHubUploadDeleteQueueButton.IsEnabled = false;
                    GitHubUploaderRefreshQueueButton.IsEnabled = false;
                    GitHubAddToQueueButton.IsEnabled = false;
                    GitHubUploaderProcessQueueButton.IsEnabled = false;
                    GitHubLargeUploaderUploadButton.IsEnabled = false;
                    GitHubUploadSaveQueueButton.IsEnabled = true;
                    string TheFile = GitHubUploadQueueDirectory + "\\" + FileName + ".txt";

                    string GetLine(int line)
                    {
                        return File.ReadLines(TheFile).ElementAtOrDefault(line);
                    }

                    string SourceDirectory = GetLine(0);
                    string GitHubDirectory = GetLine(1);
                    string SmartMode = GetLine(2);
                    string ShutdownWhenFinished = GetLine(3);
                    string Base64AllFiles = GetLine(4);
                    string EncryptAllFiles = GetLine(5);
                    string DecryptionKey = GetLine(6);
                    string GitHubUsername = GetLine(7);

                    void CheckTrueThenChangeCheckBox(string Check, CheckBox checkbox)
                    {
                        if (Check == "true")
                        {
                            checkbox.IsChecked = true;
                        }
                    }

                    SourceDirectoryTextBox.Text = SourceDirectory;
                    GitHubDirectoryTextBox.Text = GitHubDirectory;
                    CheckTrueThenChangeCheckBox(SmartMode, SmartModeCheckBox);
                    CheckTrueThenChangeCheckBox(ShutdownWhenFinished, GitHubLargeUploaderShutdownComputerCheckBox);
                    CheckTrueThenChangeCheckBox(Base64AllFiles, GitHubLargeUploaderBase64CheckBox);
                    CheckTrueThenChangeCheckBox(EncryptAllFiles, GitHubLargeUploaderEncryptFilesCheckbox);
                    EncryptionKeyTextBox.Text = DecryptionKey;
                    GitHubUsernameTextBox.Text = GitHubUsername;
                    QueueToModify = TheFile;
                }
            }
            catch
            {
                await ShowUniversalSnackbar3Seconds("No queue selected");
            }
        }

        private string QueueToModify = "";

        private async void GitHubUploadSaveQueueButton_Click(object sender, RoutedEventArgs e)
        {
            GitHubUploaderModifyQueueButton.IsEnabled = true;
            GitHubUploadDeleteQueueButton.IsEnabled = true;
            GitHubUploaderRefreshQueueButton.IsEnabled = true;
            GitHubAddToQueueButton.IsEnabled = true;
            GitHubUploaderProcessQueueButton.IsEnabled = true;
            GitHubLargeUploaderUploadButton.IsEnabled = true;
            GitHubUploadSaveQueueButton.IsEnabled = false;
            GitHubUploader Uploader = new GitHubUploader();
            Uploader.Base64AllFiles = await CheckTrue(GitHubLargeUploaderBase64CheckBox);
            GitHubLargeUploaderBase64CheckBox.IsChecked = false;
            Uploader.EncryptAllFiles = await CheckTrue(GitHubLargeUploaderEncryptFilesCheckbox);
            GitHubLargeUploaderEncryptFilesCheckbox.IsChecked = false;
            Uploader.DecryptionKey = EncryptionKeyTextBox.Text;
            EncryptionKeyTextBox.Text = "";
            Uploader.GenerateCodeForDownloader = await CheckTrue(GenerateLinkCheckBox);
            GenerateLinkCheckBox.IsChecked = false;
            Uploader.SmartMode = await CheckTrue(SmartModeCheckBox);
            SmartModeCheckBox.IsChecked = false;
            Uploader.SourceDirectory = SourceDirectoryTextBox.Text;
            SourceDirectoryTextBox.Text = "";
            Uploader.GitHubDirectory = GitHubDirectoryTextBox.Text;
            GitHubDirectoryTextBox.Text = "";
            Uploader.Username = GitHubUsernameTextBox.Text;
            GitHubUsernameTextBox.Text = "";
            Uploader.ShutdownWhenFinished = await CheckTrue(GitHubLargeUploaderShutdownComputerCheckBox);
            GitHubLargeUploaderShutdownComputerCheckBox.IsChecked = false;
            File.Delete(QueueToModify);
            await Uploader.SaveConfiguration(QueueToModify);
            await ShowUniversalSnackbar3Seconds("Saved configuration.");
        }

        private bool ProcessQueue = false;
        private async void GitHubUploaderProcessQueueButton_Click(object sender, RoutedEventArgs e)
        {
            GitHubUploaderStopQueueButton.IsEnabled = true;
            ProcessQueue = true;
            GitHubLargeUploaderUploadButton.IsEnabled = false;
            GitHubUploaderProcessQueueButton.IsEnabled = false;
            await ShowUniversalSnackbar3Seconds("Processing uploads...");
            while (ProcessQueue == true && DirectoryEmpty(GitHubUploadQueueDirectory) == false)
            {
                await Task.Delay(100);
                DirectoryInfo UploaderDirectory = new DirectoryInfo(GitHubUploadQueueDirectory);
                GitHubUploader Uploader = new GitHubUploader();
                foreach (var fileInfo in UploaderDirectory.GetFiles())
                {
                    await Uploader.LoadConfiguration(fileInfo.FullName, true);
                    await Uploader.StartUploader(true);
                    await RefreshGitHubQueue();
                    await Task.Delay(100);
                    if (ProcessQueue == false)
                    {
                        break;
                    }
                }
            }
            GitHubUploaderStopQueueButton.IsEnabled = false;
            GitHubLargeUploaderUploadButton.IsEnabled = true;
            GitHubUploaderProcessQueueButton.IsEnabled = true;
            ProcessQueue = false;
            await ShowUniversalSnackbar3Seconds("Uploads complete.");
        }

        public bool DirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }

        private void GitHubUploaderStopQueueButton_Click(object sender, RoutedEventArgs e)
        {
            GitHubUploaderStopQueueButton.IsEnabled = false;
            ProcessQueue = false;
        }

        private void GitHubUploaderViewFileButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (File.Exists(GitHubUploadQueueDirectory + "\\" + GitHubQueueListBox.SelectedItem.ToString().Split('$')[2].Trim() + ".txt"))
                {
                    Process.Start(GitHubUploadQueueDirectory + "\\" +
                                  GitHubQueueListBox.SelectedItem.ToString().Split('$')[2].Trim() + ".txt");
                }
            }
            catch
            {

            }
        }

        private async void GitLogoutButton_Click(object sender, RoutedEventArgs e)
        {
            await RunCommandHidden("cmdkey /delete:LegacyGeneric:target=git:https://github.com");
            await ShowUniversalSnackbar3Seconds("Logged out of Git");
        }

        private async void Button_Click_89(object sender, RoutedEventArgs e)
        {
            await ShowUniversalSnackbar3Seconds("Logging in to GitHub");
            await RunCommandHidden("cd \"%temp%\"\ngit clone https://github.com/EpicGamesGun/Login.git");
            Random HA = new Random();
            string HANumber = HA.Next(1111, 9999).ToString();
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Login\\Something.txt", HANumber);
            await RunCommandHidden("cd \"%temp%\\Login\"\ngit add --all\ngit commit -m \"Login\"\ngit push origin");
            await ShowUniversalSnackbar3Seconds("Success");
            Topmost = true;
            await Task.Delay(1000);
            Topmost = false;
        }

        private async  void Button_Click_90(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(GitHubLoginGrid);
        }

        private async void Button_Click_91(object sender, RoutedEventArgs e)
        {
            await LoginGitHub(GitHubLoginUsername.Text, PersonalAccessTokenTextBox.Text);
        }

        private string GitHubLoginDirectory = Environment.GetEnvironmentVariable("APPDATA") + "\\GitHubLogin";
        private async Task LoginGitHub(string Username, string PersonalAccessToken)
        {
            if (!File.Exists(GitHubLoginDirectory + "\\SavedAccount.txt"))
            {
                File.WriteAllText(GitHubLoginDirectory + "\\SavedAccount.txt",Username + "$" + PersonalAccessToken);
            }
            else
            {
                File.WriteAllText(GitHubLoginDirectory + "\\SavedAccount.txt", File.ReadAllText(GitHubLoginDirectory + "\\SavedAccount.txt") + "\n" + Username + "$" + PersonalAccessToken);
            }
            await RefreshGitHubLoginListBox();
        }

        private async Task RetrieveGitHub(string Username)
        {
            if (File.Exists(GitHubLoginDirectory + "\\SavedAccount.txt"))
            {
                foreach (var readLine in File.ReadLines(GitHubLoginDirectory + "\\SavedAccount.txt"))
                {
                    if (readLine.Contains(Username))
                    {
                        GitHubLoggedInAccount = readLine.Split('$')[0].Trim();
                        GitHubLoggedInLabel.Content = readLine.Split('$')[0].Trim();
                        File.WriteAllText(GitHubLoginDirectory + "\\CurrentAccount.txt", readLine.Split('$')[0].Trim());
                        File.WriteAllText(GitHubLoginDirectory + "\\CurrentToken.txt", readLine.Split('$')[1].Trim());
                        //await RunCommandHidden("gh auth logout -h github.com | echo y");
                        //await RunCommandHidden("gh auth login --with-token < \"" + GitHubLoginDirectory + "\\CurrentToken.txt\"");
                        //await RunCommandHidden("cmdkey /delete:LegacyGeneric:target=git:https://github.com");
                        GitHub git = new GitHub(File.ReadAllText(GitHubLoginDirectory + "\\CurrentToken.txt"));
                        await git.LoginToGit();
                        await git.LoginToGitHubCLI();
                        //await RunCommandHidden("cmdkey /generic:LegacyGeneric:target=git:https://github.com /user:PersonalAccessToken /pass:" + GitHubLoggedInAccount);
                        await ShowUniversalSnackbar3Seconds("Logged in to " + GitHubLoggedInAccount + ".");
                        break;
                    }
                }
            }
            else
            {
                await ShowUniversalSnackbar3Seconds("No account on list");
            }
            await RefreshGitHubLoginListBox();
        }

        private async void LoginGitHubAccountButton_Click(object sender, RoutedEventArgs e)
        {
            LoginGitHubAccountButton.IsEnabled = false;
            if (GitHubAccountsListBox.SelectedItem != null)
            {
                await RetrieveGitHub(GitHubAccountsListBox.SelectedItem.ToString());
            }

            LoginGitHubAccountButton.IsEnabled = true;
        }

        private async void RemoveGitHubAccountButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                foreach (var readLine in File.ReadLines(GitHubLoginDirectory + "\\SavedAccount.txt"))
                {
                    if (!readLine.Contains(GitHubAccountsListBox.SelectedItem.ToString()))
                    {
                        GitHubAccountRemovalListBox.Items.Add(readLine);
                    }
                }
                File.Delete(GitHubLoginDirectory + "\\SavedAccount.txt");
                File.WriteAllLines(GitHubLoginDirectory + "\\SavedAccount.txt", GitHubAccountRemovalListBox.Items.OfType<string>().ToArray());
                GitHubAccountRemovalListBox.Items.Clear();
                if (File.ReadLines(GitHubLoginDirectory + "\\SavedAccount.txt").ElementAtOrDefault(0) == "")
                {
                    File.Delete(GitHubLoginDirectory + "\\SavedAccount.txt");
                }
                await RefreshGitHubLoginListBox();
            }
            catch
            {

            }
        }

        private async void LogoutOfGitHubButton_Click(object sender, RoutedEventArgs e)
        {
            LogoutOfGitHubButton.IsEnabled = false;
            await RunCommandHidden("gh auth logout -h github.com | echo y");
            GitHubLoggedInAccount = "";
            GitHubLoggedInLabel.Content = "Not logged in";
            LogoutOfGitHubButton.IsEnabled = true;
        }

        private string NewRepositoryDirectory = Environment.GetEnvironmentVariable("APPDATA") + "\\GitHub";
        private async void Button_Click_92(object sender, RoutedEventArgs e)
        {
            CreateRepositoryButton.IsEnabled = false;
            if (GitHubLoggedInAccount != "")
            {
                Directory.CreateDirectory(NewRepositoryDirectory);
                //await RunCommandHidden("cd \"" + NewRepositoryDirectory + "\"\ngh repo create \"" + NewGitHubRepositoryNameTextBox.Text.Replace("=","").Replace(" ","") + "\" --public -y");
                GitHub git = new GitHub("null");
                await git.CreateGitHubRepository(NewGitHubRepositoryNameTextBox.Text,NewRepositoryDirectory);
                GeneratedDownloadLinkTextBox.Text = "https://github.com/" + GitHubLoggedInAccount + "/" + NewGitHubRepositoryNameTextBox.Text + "";
                GitHubDirectoryTextBox.Text = NewRepositoryDirectory + "\\" + NewGitHubRepositoryNameTextBox.Text.Replace("=", "").Replace(" ", "");
                string[] Stuff =
                {
                    "Created using security tools", "By software store technologies inc",
                    "2020 during COVID-19 pandemic"
                };
                File.WriteAllLines(GitHubDirectoryTextBox.Text + "\\Credits.txt",Stuff);
                await ShowUniversalSnackbar3Seconds("Initializing repository");
                await git.PushOrigin(NewRepositoryDirectory + "\\" + NewGitHubRepositoryNameTextBox.Text, "Update");
                await ShowUniversalSnackbar3Seconds("Successfully created repository.");
            }
            else
            {
                await ShowUniversalSnackbarButton("Not logged into github", "Login to github", TimeSpan.FromSeconds(3),
                    async () =>
                    {
                        await ChangeGrid(GitHubLoginGrid);
                    });
            }
            CreateRepositoryButton.IsEnabled = true;
            NewGitHubRepositoryNameTextBox.Text = "";
        }

        private async void Button_Click_93(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(UploaderDownloaderGrid);
        }

        private static void DirectoryCopy(
            string sourceDirName, string destDirName, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the source directory does not exist, throw an exception.
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory does not exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }


            // Get the file contents of the directory to copy.
            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                // Create the path to the new copy of the file.
                string temppath = Path.Combine(destDirName, file.Name);

                // Copy the file.
                file.CopyTo(temppath, false);
            }

            // If copySubDirs is true, copy the subdirectories.
            if (copySubDirs)
            {

                foreach (DirectoryInfo subdir in dirs)
                {
                    // Create the subdirectory.
                    string temppath = Path.Combine(destDirName, subdir.Name);

                    // Copy the subdirectories.
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }
        private string CreatedMountLetter = "";
        private async void CreateInsertDirectoryVeracryptButton_Click(object sender, RoutedEventArgs e)
        {
            CreateInsertDirectoryVeracryptButton.IsEnabled = false;
            try
            {
                WPFFolderBrowserDialog d = new WPFFolderBrowserDialog("Select your source directory");
                d.ShowDialog();
                await ShowUniversalSnackbar3Seconds("Creating veracrypt volume");
                string Password = CreateVeracryptPassword.Password;
                string CreateDirectory = BrowseCreateVeracrypt.Text;
                CreatedMountLetter = GetRandomDriveLetter1();
                await CreateVeracryptVolumeSECURETOOLS();
                await ShowUniversalSnackbar3Seconds("Volume has been created");
                await MountVeracrypt(CreateDirectory, Password, "500", CreatedMountLetter);
                while (!Directory.Exists(CreatedMountLetter + ":\\"))
                {
                    await Task.Delay(10);
                }

                await ShowUniversalSnackbar3Seconds("Moving directory into volume");
                BackgroundWorker DirectoryCopier = new BackgroundWorker();
                DirectoryCopier.DoWork += (o, args) =>
                {
                    DirectoryCopy(d.FileName, CreatedMountLetter + ":\\", true);
                };
                DirectoryCopier.RunWorkerAsync();
                while (DirectoryCopier.IsBusy)
                {
                    await Task.Delay(10);
                }

                await ShowUniversalSnackbar3Seconds("Dismounting veracrypt volume");

                await DismountVeracrypt(CreatedMountLetter);
                await ShowUniversalSnackbar3Seconds("FINISHED");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                await ShowUniversalSnackbarButton("Error has occured", "Show log", TimeSpan.FromSeconds(3), async () =>
                {
                    try
                    {
                        string TempFile = Path.GetTempFileName();
                        File.WriteAllText(TempFile, ex.ToString());
                        Process.Start(TempFile);
                    }
                    catch
                    {

                    }
                });
            }
            CreateInsertDirectoryVeracryptButton.IsEnabled = true;
        }
        public static long DirSize(DirectoryInfo d)
        {
            long size = 0;
            // Add file sizes.
            FileInfo[] fis = d.GetFiles();
            foreach (FileInfo fi in fis)
            {
                size += fi.Length;
            }
            // Add subdirectory sizes.
            DirectoryInfo[] dis = d.GetDirectories();
            foreach (DirectoryInfo di in dis)
            {
                size += DirSize(di);
            }
            return size;
        }
        private async void Button_Click_94(object sender, RoutedEventArgs e)
        {
            try
            {
                WPFFolderBrowserDialog d = new WPFFolderBrowserDialog("Select your folder to see size");
                d.ShowDialog();
                DirectoryInfo CheckSize = new DirectoryInfo(d.FileName);
                long FolderSize = DirSize(CheckSize);
                Console.WriteLine(FolderSize);
                double InKB = FolderSize / 1024d;
                double InMB = InKB / 1024d;
                double InGB = InMB / 1024d;
                if (InKB <= 1024)
                {
                    CreateVeracryptSize.Text = InKB.ToString("0");
                    SizeUnitCreateVeracrypt.Text = "KB";
                    CreateFAT.IsChecked = true;
                }
                else if (InKB >= 1024 && InMB <= 1024)
                {
                    CreateVeracryptSize.Text = InMB.ToString("0");
                    SizeUnitCreateVeracrypt.Text = "MB";
                    CreateFAT.IsChecked = true;
                }
                else if (InMB >= 1024)
                {
                    double OriginalGB = InGB;
                    if (Math.Round(InGB) < InGB)
                    {
                        InGB++;
                    }
                    CreateVeracryptSize.Text = InGB.ToString("0");
                    SizeUnitCreateVeracrypt.Text = "GB";
                    if (OriginalGB > 4)
                    {
                        double GB = InGB * 0.12;
                        GB = GB + InGB;
                        CreateVeracryptSize.Text = GB.ToString("0");
                        CreateNTFS.IsChecked = true;
                    }
                    else
                    {
                        CreateFAT.IsChecked = true;
                    }
                }
            }
            catch
            {

            }
        }

        private async void UploadVeracryptButton_Click(object sender, RoutedEventArgs e)
        {
            UploadVeracryptButton.IsEnabled = false;
            if (UploadVeraCryptBrowseFileTextBox.Text != "" && UploadVeraCryptSizePerPartTextBox.Text != "")
            {
                await RARFunctions.CreateFromVeraCrypt(UploadVeraCryptBrowseFileTextBox.Text,
                    Int32.Parse(UploadVeraCryptSizePerPartTextBox.Text),
                    await CheckTrue(UploadVeracryptDeleteOriginalButton),"C:\\",RARFunctions.SizeFormat.MegaBytes);
                SourceDirectoryTextBox.Text = Path.GetDirectoryName(UploadVeraCryptBrowseFileTextBox.Text);
                UploadVeraCryptBrowseFileTextBox.Text = "";
                UploadVeraCryptSizePerPartTextBox.Text = "";
                await ShowUniversalSnackbar3Seconds("Successfully created");
            }
            else
            {
                await ShowUniversalSnackbar3Seconds("Values cannot be blank.");
            }
            UploadVeracryptButton.IsEnabled = true;
        }

        private async void Button_Click_95(object sender, RoutedEventArgs e)
        {
            UploadVeraCryptBrowseFileTextBox.Text = await OpenFile();
        }

        private async void Button_Click_71(object sender, RoutedEventArgs e)
        {
            CleanGitHubRepositoryButtonSingle.IsEnabled = false;
            try
            {
                WPFFolderBrowserDialog d = new WPFFolderBrowserDialog("Select GitHub/GitLab Repository Directory");
                d.ShowDialog();
                await ShowUniversalSnackbar3Seconds("Clearing GitHub Repository...");
                await RunCommandShown("cd \"" + d.FileName + "\"\ngit gc");
                await ShowUniversalSnackbar3Seconds("Done!");
            }
            catch (Exception exception)
            {
                await ShowUniversalSnackbar3Seconds("Invalid Directory");
            }
            CleanGitHubRepositoryButtonSingle.IsEnabled = true;
        }

        private void AdvancedDecryptFBICheckBox_Checked(object sender, RoutedEventArgs e)
        {
            if (AdvancedDecryptFBICheckBox.IsChecked == true)
            {
                AdvancedDecryptFileFBIGrid.Visibility = Visibility.Visible;
            }
            else
            {
                AdvancedDecryptFileFBIGrid.Visibility = Visibility.Hidden;
            }
        }

        private void DeleteFile(string Path)
        {
            if(File.Exists(Path))
            {
                File.Delete(Path);
            }
        }
        private async void SettingsChangeSidePanelTrigger(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(SettingsPageOneGrid);
        }

        // Settings Loader
        private void MainWindow_Loaded2(object sender, RoutedEventArgs e)
        {
            string OntarioFile = @"C:\Users\BigHead\AppData\Local\Ontario.txt";
            string ServerComputerFile = @"C:\Users\BigHead\AppData\Local\ServerComputer.txt";
            OntarioAnnouncerEnabledCheckBox.IsChecked = File.Exists(OntarioFile) && File.Exists(ServerComputerFile);
            COVID19ClipboardNotifier.IsChecked = !File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DisableClipboard.txt");
            COVID19OntarioOnlyCheckBox.IsChecked = File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\OntarioOnly.txt");
            COVID19RunBigHeadDetector.IsChecked = !File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DisableWadwlyHead.txt");
            COVID19AnnounceAtStartup.IsChecked = !File.Exists(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\DisableAnnouncer.txt");
            EnabledCOVIDAnnouncer.Visibility = COVID19AnnounceAtStartup.IsChecked.Value ? EnabledCOVIDAnnouncer.Visibility = Visibility.Visible : EnabledCOVIDAnnouncer.Visibility = Visibility.Hidden;
            SpeechSynthesizer Synth = new SpeechSynthesizer();
            DefaultCOVIDVoice.Text = File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DefaultCOVIDVoice.txt") ? File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DefaultCOVIDVoice.txt") : "Random Voice";
            foreach (InstalledVoice installedVoice in Synth.GetInstalledVoices())
            {
                if(DefaultCOVIDVoice.Text != installedVoice.VoiceInfo.Name)
                {
                    DefaultCOVIDVoice.Items.Add(installedVoice.VoiceInfo.Name);
                }
            }
            if(DefaultCOVIDVoice.Text != "Random Voice")
            {
                DefaultCOVIDVoice.Items.Add("Random Voice");
            }
            RotationScheduleFaceToFace.IsChecked = File.Exists(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\FaceToFace.txt");
            RotationScheduleRecordingsPath.Text = File.Exists(Environment.GetEnvironmentVariable("USERPROFILE") + @"\AppData\Roaming\PeriodRotations\RecordingsSavings.txt") ? File.ReadAllText(Environment.GetEnvironmentVariable("USERPROFILE") + @"\AppData\Roaming\PeriodRotations\RecordingsSavings.txt") : "";
        }

        private async void Button_Click_96(object sender, RoutedEventArgs e)
        {
            string OntarioFile = Environment.GetEnvironmentVariable("USERPROFILE") + @"\AppData\Local\Ontario.txt";
            string ServerComputerFile = Environment.GetEnvironmentVariable("USERPROFILE") + @"\AppData\Local\ServerComputer.txt";
            if(OntarioAnnouncerEnabledCheckBox.IsChecked == true)
            {
                File.WriteAllText(OntarioFile,"s");
                File.WriteAllText(ServerComputerFile,"s");
            }
            else
            {
                DeleteFile(OntarioFile);
                DeleteFile(ServerComputerFile);
            }
            await ShowUniversalSnackbar3Seconds("Settings saved successfully.");
        }

        private async void Button_Click_97(object sender, RoutedEventArgs e)
        {
            if(COVID19AnnounceAtStartup.IsChecked == true)
            {
                DeleteFile(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\DisableAnnouncer.txt");
            }
            else
            {
                File.WriteAllText(Environment.GetEnvironmentVariable("LOCALAPPDATA") + "\\DisableAnnouncer.txt","");
            }

            if (COVID19RunBigHeadDetector.IsChecked == true)
            {
                DeleteFile(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DisableWadwlyHead.txt");
            }
            else
            {
                File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DisableWadwlyHead.txt","");
            }

            if(COVID19ClipboardNotifier.IsChecked == true)
            {
                DeleteFile(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DisableClipboard.txt");
            }
            else
            {
                File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DisableClipboard.txt","");
            }

            if(COVID19OntarioOnlyCheckBox.IsChecked == true)
            {
                File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\OntarioOnly.txt","");
            }
            else
            {
                DeleteFile(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\OntarioOnly.txt");
            }

            if (DefaultCOVIDVoice.Text == "Random Voice")
            {
                DeleteFile(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DefaultCOVIDVoice.txt");
            }
            else if (DefaultCOVIDVoice.Text == "")
            {

            }
            else
            {
                File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DefaultCOVIDVoice.txt",DefaultCOVIDVoice.Text);
            }
            await ShowUniversalSnackbar3Seconds("Default COVID-19 Announce voice will be " + File.ReadAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\DefaultCOVIDVoice.txt"));
            await ShowUniversalSnackbar3Seconds("Settings saved successfully.");
        }

        private async void COVID19AnnounceAtStartup_Checked(object sender, RoutedEventArgs e)
        {
            if(COVID19AnnounceAtStartup.IsChecked == true)
            {
                EnabledCOVIDAnnouncer.Visibility = Visibility.Visible;
            }
            else
            {
                EnabledCOVIDAnnouncer.Visibility = Visibility.Hidden;
            }
        }

        private async void Button_Click_98(object sender, RoutedEventArgs e)
        {
            if (RotationScheduleFaceToFace.IsChecked == true)
            {
                await ShowUniversalSnackbar3Seconds("Rotation Schedule will now follow Face-To-Face schedules");
                File.WriteAllText(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\FaceToFace.txt", "");
            }
            else
            {
                await ShowUniversalSnackbar3Seconds("Rotation Schedule will now follow virtual class schedules");
                DeleteFile(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData) + "\\FaceToFace.txt");
            }

            if (Directory.Exists(@"C:\Users\BigHead\AppData\Roaming\PeriodRotations"))
            {
                File.WriteAllText(@"C:\Users\BigHead\AppData\Roaming\PeriodRotations\RecordingsSavings.txt",RotationScheduleRecordingsPath.Text);
            }
            await ShowUniversalSnackbar3Seconds("Settings saved successfully.");
        }

        private async void Button_Click_99(object sender, RoutedEventArgs e)
        {
            RotationScheduleRecordingsPath.Text = await BrowseFolder();
        }

        private async void ListViewItem_Selected_8(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(TTSGrid);
        }

        private async void Button_Click_100(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(EncryptFileGrid);
        }

        private async void Button_Click_101(object sender, RoutedEventArgs e)
        {
            await ChangeGrid(DecryptFileGrid);
        }
    }
}
