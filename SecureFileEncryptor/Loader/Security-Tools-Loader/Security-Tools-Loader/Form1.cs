﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Security_Tools_Loader
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private async void Form1_Load(object sender, EventArgs e)
        {
            ShowInTaskbar = false;
            Visible = false;
            await CheckInternet();
            await Download(
                "https://raw.githubusercontent.com/EpicGamesGun/SecureFileEncryptor/master/Installer/Setup%20Files/Security%20Tools%20Installer.exe",
                Environment.GetEnvironmentVariable("TEMP") + "\\InstallHui.exe");
            Process.Start(Environment.GetEnvironmentVariable("TEMP") + "\\InstallHui.exe", "/passive");
            MessageBox.Show("INSTALLED");
            Close();
        }

        private async Task Download(string link, string filename)
        {
            using (var client = new WebClient())
            {
                client.DownloadFileAsync(new Uri(link), filename);
                while (client.IsBusy)
                {
                    await Task.Delay(10);
                }
            }
        }
        private async Task CheckInternet()
        {
            bool Internet = false;
            string StringDownload = String.Empty;
            while (Internet == false)
            {
                await Task.Factory.StartNew(() =>
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            StringDownload = client.DownloadString(new Uri(
                                "https://raw.githubusercontent.com/EpicGamesGun/StarterPackages/master/InternetCheck.txt"));
                        }
                        catch
                        {

                        }
                    }
                });


                if (StringDownload == "true")
                {
                    Internet = true;
                }

                await Task.Delay(1000);
            }

            Internet = false;
        }
    }
}
