﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Google.Authenticator;

namespace Secure_File_Vault
{
    public partial class OpenThreeStepVerification : Form
    {
        public OpenThreeStepVerification()
        {
            InitializeComponent();
            Closing += OpenThreeStepVerification_Closing;
        }

        private bool NeedToClose = false;
        private void OpenThreeStepVerification_Closing(object sender, CancelEventArgs e)
        {
            if (NeedToClose == false)
            {
                e.Cancel = true;
            }
        }

        private void OpenThreeStepVerification_Load(object sender, EventArgs e)
        {
            TopMost = true;
            FormBorderStyle = FormBorderStyle.FixedSingle;
        }
        private string MainDirectory = Environment.GetEnvironmentVariable("TEMP") + "\\FileVault";


        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
            bool Correct = tfa.ValidateTwoFactorPIN(File.ReadAllText(MainDirectory + "\\Code.txt"), textBox1.Text);
            if (Correct == true)
            {
                NeedToClose = true;
                File.WriteAllText(MainDirectory + "\\Correct.txt",GetHashString("Correct"));
                Close();
            }
            else
            {
                
            }
        }

        public static byte[] GetHash(string inputstring)
        {
            using (HashAlgorithm algorithm = SHA512.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputstring));
        }

        public static string GetHashString(string inputstring)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputstring))
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            NeedToClose = true;
            Close();
        }
    }
}
