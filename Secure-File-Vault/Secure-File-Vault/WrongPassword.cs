﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Secure_File_Vault.Properties;

namespace Secure_File_Vault
{


    public partial class WrongPassword : Form
    {
        [DllImport("user32.dll")]
        public static extern IntPtr CreateDesktop(string lpszDesktop, IntPtr lpszDevice,
            IntPtr pDevmode, int dwFlags, uint dwDesiredAccess, IntPtr lpsa);

        [DllImport("user32.dll")]
        private static extern bool SwitchDesktop(IntPtr hDesktop);

        [DllImport("user32.dll")]
        public static extern bool CloseDesktop(IntPtr handle);

        [DllImport("user32.dll")]
        public static extern bool SetThreadDesktop(IntPtr hDesktop);

        [DllImport("user32.dll")]
        public static extern IntPtr GetThreadDesktop(int dwThreadId);

        [DllImport("kernel32.dll")]
        public static extern int GetCurrentThreadId();


        public WrongPassword()
        {
            InitializeComponent();
        }
        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }

        enum DESKTOP_ACCESS : uint
        {
            DESKTOP_NONE = 0,
            DESKTOP_READOBJECTS = 0x0001,
            DESKTOP_CREATEWINDOW = 0x0002,
            DESKTOP_CREATEMENU = 0x0004,
            DESKTOP_HOOKCONTROL = 0x0008,
            DESKTOP_JOURNALRECORD = 0x0010,
            DESKTOP_JOURNALPLAYBACK = 0x0020,
            DESKTOP_ENUMERATE = 0x0040,
            DESKTOP_WRITEOBJECTS = 0x0080,
            DESKTOP_SWITCHDESKTOP = 0x0100,

            GENERIC_ALL = (DESKTOP_READOBJECTS | DESKTOP_CREATEWINDOW | DESKTOP_CREATEMENU |
                           DESKTOP_HOOKCONTROL | DESKTOP_JOURNALRECORD | DESKTOP_JOURNALPLAYBACK |
                           DESKTOP_ENUMERATE | DESKTOP_WRITEOBJECTS | DESKTOP_SWITCHDESKTOP),
        }
        private async void WrongPassword_Load(object sender, EventArgs e)
        {
                //IntPtr NEWDESKTOP = CreateDesktop("dew", IntPtr.Zero, IntPtr.Zero, 0, (uint) DESKTOP_ACCESS.GENERIC_ALL,IntPtr.Zero);
                //IntPtr OLDDESKTOP = GetThreadDesktop(GetCurrentThreadId());
                //SwitchDesktop(NEWDESKTOP);
                //SetThreadDesktop(NEWDESKTOP);

                //Thread.CurrentThread.TrySetApartmentState(ApartmentState.STA);
                ShowIcon = false;
                Text = "";
                TopMost = true;
                ControlBox = false;
                FormBorderStyle = FormBorderStyle.None;
                WindowState = FormWindowState.Maximized;
                button1.Enabled = false;
                MessageCycle();
                var i = 60;
                Task.Factory.StartNew(() =>
                {
                    SoundPlayer dewd = new SoundPlayer(Resources.WrongPassword);
                    dewd.PlaySync();
                });
                SoundPlayer astronomia = new SoundPlayer(Resources.music);
                astronomia.PlayLooping();
                while (i > 0)
                {
                    CountDownLabel.Text = "Please wait " + i +
                                 " seconds before you can dismiss to prevent \n DDOS, BRUTE FORCE ATTACKS";
                    i = i - 1;
                    await Task.Delay(1000);
                }
                //SetThreadDesktop(OLDDESKTOP);
                //CloseDesktop(NEWDESKTOP);
                //SwitchDesktop(OLDDESKTOP);
                CountDownLabel.Text = "We are not responsible if you lose your password and files";
                button1.Enabled = true;
        }
        

        private void button1_Click(object sender, EventArgs e)
        {
            if (File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\CreateVaultSettings.txt"))
            {
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\CreateVaultSettings.txt");
                Application.Exit();
            }

            if (File.Exists(Environment.GetEnvironmentVariable("TEMP") + "\\OpenVaultSettings.txt"))
            {
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\OpenVaultSettings.txt");
                Application.Exit();
            }
            Close();
        }

        private async Task PlayMusic()
        {
            SoundPlayer lol = new SoundPlayer(Resources.music);
            lol.Play();
        }

        private async Task MessageCycle()
        {
            while (true)
            {
                label2.Text = "We are not able to recover your \n files if you forget your password";
                await Task.Delay(3000);
                pictureBox1.Image = Resources.coffin_2;
                label2.Text = "We are not able to give you back \n your password because we do not store logs";
                await Task.Delay(3000);
                label2.Text = "Coffin Dance Meme";
                await Task.Delay(3000);
                label2.Text = "Your files may be lost or you \n forgot your password :(";
                await Task.Delay(3000);
                label2.Text = "See if you can remember your password";
                await Task.Delay(3000);
                label2.Text = "Check your lastpass, your notebook \n or chrome password manager";
                await Task.Delay(3000);
            }
        }
    }
}
