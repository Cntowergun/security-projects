﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Secure_File_Vault
{
    public partial class Verification : Form
    {
        public Verification()
        {
            InitializeComponent();
        }

        public static byte[] GetHash(string inputstring)
        {
            using (HashAlgorithm algorithm = SHA512.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputstring));
        }

        public static bool Verified = false;
        public static string GetHashString(string inputstring)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputstring))
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }

        private bool Exit = false;

        public async Task RunCommandHidden(string Command)
        {
            Random dew = new Random();
            int hui = dew.Next(0000, 9999);
            string[] CommandChut = { Command };
            File.WriteAllLines(
                System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui + ".bat",
                CommandChut);
            Process C = new Process();
            C.StartInfo.FileName = System.Environment.GetEnvironmentVariable("USERPROFILE") +
                                   "\\Documents\\RunCommand" + hui + ".bat";
            C.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            C.EnableRaisingEvents = true;
            C.Exited += C_Exited;
            C.Start();
            while (Exit == false)
            {
                await Task.Delay(10);
            }

            Exit = false;
            File.Delete(System.Environment.GetEnvironmentVariable("USERPROFILE") + "\\Documents\\RunCommand" + hui +
                        ".bat");
        }

        private void C_Exited(object sender, EventArgs e)
        {
            Exit = true;
        }
        private async Task<string> EncodeDecodeToBase64String(string input, bool Encode)
        {
            string Return = "";
            File.WriteAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Encode.txt", input);
            if (Encode == true)
            {
                await RunCommandHidden("certutil -encode \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encode.txt" + "\" \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encoded.txt\"");
                Return = File.ReadAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encode.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
            }
            else
            {
                await RunCommandHidden("certutil -decode \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encode.txt" + "\" \"" + Environment.GetEnvironmentVariable("TEMP") +
                                       "\\Encoded.txt\"");
                Return = File.ReadAllText(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encode.txt");
                File.Delete(Environment.GetEnvironmentVariable("TEMP") + "\\Encoded.txt");
            }

            return Return;
        }
        string StoreCode = "";

        private async void Verification_Load(object sender, EventArgs e)
        {
            try
            {
                Random Code = new Random();
                int RandomCode = Code.Next(11111, 999999);
                StoreCode = GetHashString(RandomCode.ToString());
                var fromAddress = new MailAddress("softwarestorehui@gmail.com", "SOFTWARE STORE");
                var toAddress = new MailAddress(Create.TwoStepEmailV, "Verification code");
                string fromPassword = await EncodeDecodeToBase64String(await EncodeDecodeToBase64String("TVRJelNtVnlZMmgxZEdoMWFRMEsNCg", false), false); ;
                const string subject = "Secure File Vault Code";
                string body = "Your Verification Code: " + RandomCode.ToString();

                var smtp = new SmtpClient
                {
                    Host = "smtp.gmail.com",
                    Port = 587,
                    EnableSsl = true,
                    DeliveryMethod = SmtpDeliveryMethod.Network,
                    UseDefaultCredentials = false,
                    Credentials = new NetworkCredential(fromAddress.Address, fromPassword)
                };
                using (var message = new MailMessage(fromAddress, toAddress)
                {
                    Subject = subject,
                    Body = body
                })
                {
                    smtp.Send(message);
                }
                await Task.Delay(-1);
            }
            catch
            {
                MessageBox.Show("No Internet connection available.");
                Verified = false;
            }
            Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (GetHashString(textBox1.Text) == StoreCode)
            {
                Verified = true;
                Close();
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (GetHashString(textBox1.Text) == StoreCode)
            {
                Verified = true;
                Close();
            }
        }
    }
}
